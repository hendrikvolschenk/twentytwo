<?php

    /**
    * This file server as a "Bootstrapper" for the Entity Framework.
    * All the classes, objects and variables that will be necessary will be loaded.
    * */

    //turn on error reporting for PHP
    error_reporting(0);

    //start the PHP session handler
    session_start();

    // in build of PHP < 5.3.0 the "lcfirst" method does not exist
    // check if it does
    if (false === function_exists('lcfirst')) {
        /**
         * Make a string's first character lowercase
         *
         * @param string $str
         * @return string the resulting string.
         */
        function lcfirst( $str ) {
            // make the first letter of the string uppercase
            $str[0] = strtolower($str[0]);
            // return the full string
            return (string)$str;
        }
    }

    //Load the Database, System, Entity and Factory classes
    require_once(dirname(__FILE__) . '/entity/classes/connection/interface.connection.php');
    require_once(dirname(__FILE__) . '/entity/classes/connection/class.mysql.php');
    require_once(dirname(__FILE__) . '/entity/classes/system/class.system.php');
    require_once(dirname(__FILE__) . '/entity/classes/entity/class.entity.php');
    require_once(dirname(__FILE__) . '/entity/classes/entity/class.entity.factory.php');
    require_once(dirname(__FILE__) . '/entity/classes/template/class.template.php');

    //set up the System object
    $system = new System;

    //load the system configuration settings
    $system->loadSettings();

    //load all the class files
    $system->loadClasses();

    //before loading the first entity, we need to connect to the database
    //set up the Database object
    $database = new MySql;

    // see if the system is installed
    if ($system->isInstalled() === true) {

    } else {

    }

    //connect to the database
    $database->connect($system->settings['HOST'], $system->settings['USERNAME'], $system->settings['PASSWORD'], $system->settings['DATABASE']);

    //get the module, action and url
    $system->loadModuleActionUrl($database);

    //get the url of the user that is logged in
    $userUrl = (isset($_SESSION['login']) && !empty($_SESSION['login'])) ? $_SESSION['login'] : 'visitor';

    //get the system user
    $system->user = EntityFactory::build('user');

    //load the user
    $system->user->load($userUrl, $database);

    //get the Entity
    $entity = EntityFactory::build($system->module);

    //check if the url is "all" or contains a comma (multiple)
    if ($system->url == 'all' || strpos($system->url, ',') !== false) {
        //load the Entity with an empty url
        $entity->load(' ', $database);
    } else {
        //load the Entity
        $entity->load($system->url, $database);
    }

    //if the url is 'all', there is no need to load an entity
    if ($system->url == 'all') {
        //get a list of all entities
        $entities = EntityFactory::buildList($database, $system->module);
        //set an empty name for this page's title
        $name = null;
    } elseif(strpos($system->url, ',') !== false) {
        //get all the urls in the url
        $entities = explode(',', $system->url);
        //there is no name for these objects
        $name = null;
    } elseif (!empty($system->url)) {
        //get the url column of the Entity
        $urlColumn = $entity->form->urlColumn;
        //get the name of this Entity
        $name = $entity->$urlColumn;
        //set entities to an array containing only this item
        $entities = array($system->url);
    } else {
        //set an empty name for this page's title
        $name = null;
        //set an empty list of entities for this page
        $entities = null;
    }

    //set up the Template object
    $template = new Template;
    //check that the template may be included (This is not a post page)
    if ($system->templateView != '!' && $system->templateView != '!post') {
        //override the template if we're in admin mode
        if ($system->admin === true && $system->user->children['user_group'][0] != 'visitors') {
            $system->settings['APPLICATION_TEMPLATE'] = 'twentytwo';
        }
        //get the page title
        $template->getTitle($system->module, $system->action, $system->url, $name, $system->settings['APPLICATION_NAME']);
        //load the Template's page content
        $template->loadContent($system, $database, $entity, $entities);
        //load the Template's javascript
        $template->getFiles($system, 'javascript');
        //load the template's styles
        $template->getFiles($system, 'styles');
        //load the template file
        $template->loadTemplate($database, $system, $entity);
    } else {
        //load the Template's page content
        $template->loadContent($system, $database, $entity, $entities, 'test2');
        //show the content
        echo $template->content;
    }
    /**
    * When logging, use http://www.php.net/manual/en/function.array-diff-assoc.php as a way to detrmine which fields were changed
    * When updating, use http://www.php.net/manual/en/function.array-diff-assoc.php as a way to determine which fields need to be updated.
    * */

?>