   $.extend(true, Entity, {
       // a form row object to handle the showing/hiding/changing of fields when a Form Row gets manipulated
       formRow : {
           // a list of field types and which rows they show and their initial values
           fieldTypes : {
               // this configuration is necessary for all fields
               all  : {
                   fields : [
                       ['label'              , ''],
                       ['description'        , ''],
                       ['type'               , ''],
                       ['null'               , ''],
/*                       [default_value      , ''],*/
                       ['disabled'           , ''],
                       ['readonly'           , ''],
                       ['functions_prepare'  , ''],
                       ['functions_validate' , '']
                   ]
               },
               // for color rows
               color : {
                   fields : [
                       ['length_min' , '0'],
                       ['length_max' , '7']
                   ]
               },
               // for date rows
               date : {
                   fields : [
                       ['placeholder' , ''],
                       ['value_min'   , ''],
                       ['value_max'   , '']
                   ]
               },
               // for datetime rows
               datetime : {
                   fields : [
                       ['placeholder' , ''],
                       ['value_min'   , ''],
                       ['value_max'   , '']
                   ]
               },
               // for email rows
               email : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , '6'],
                       ['length_max'  , '255']
                   ]
               },
                
               // for month rows
               month : {
                   fields : [
                       ['value_min'   , ''],
                       ['value_max'   , '']
                   ]
               },
               
               // for number rows
               number : {
                   fields : [
                       ['placeholder' , ''],
                       /*[signed      , '0'],*/
                       ['value_min'   , ''],
                       ['value_max'   , ''],
                       ['step'        , '1']
                   ]
               },
               
               // for range rows
               range : {
                   fields : [
                       ['placeholder' , ''],
                       /*[signed      , '0'],*/
                       ['value_min'   , ''],
                       ['value_max'   , ''],
                       ['step'        , '1']
                   ]
               },
               
               // for decimal rows
               decimal : {
                   fields : [
                       ['placeholder' , ''],
                       /*[signed      , '0'],*/
                       ['value_min'   , ''],
                       ['value_max'   , ''],
                       ['step'        , '0.1'],
                       ['length_min'  , ''],
                       ['length_max'  , '']
                   ]
               },
               
               // for tel (telephone) rows
               tel : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , '8'],
                       ['length_max'  , '20']
                   ]
               },
               
               // for time rows
               time : {
                   fields : [
                       ['value_min'   , ''],
                       ['value_max'   , '']
                   ]
               },
               
               // for url rows
               url : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , ''],
                       ['length_max'  , '']
                   ]
               },
               
               // for week rows
               week : {
                   fields : [
                       ['value_min' , ''],
                       ['value_max' , '']
                   ]
               },
               
               // for checkbox rows
               checkbox : {
                   fields : []
               },
               
               // for hidden rows
               hidden : {
                   fields : []
               },
               
               // for password rows
               password : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , ''],
                       ['length_max'  , '']
                   ]
               },
               
               // for radio rows
               radio : {
                   fields : []
               },
               
               // for text rows
               text : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , '0'],
                       ['length_max'  , '64']
                   ]
               },
               
               // for textarea rows
               textarea : {
                   fields : [
                       ['placeholder' , ''],
                       ['length_min'  , '0'],
                       ['length_max'  , '65535']
                   ]
               },
               
               // for dropdown rows
               dropdown : {
                   fields : [
                       ['allowed_values' , '']
                   ]
               },
               
               // for dropdown multiple rows
               multiple : {
                   fields : [
                       ['allowed_values' , '']
                   ]
               },
               
               // for html rows
               html : {
                   fields : [
                       ['length_min'  , '0'],
                       ['length_max'  , '65535'],
                       ['placeholder' , '']
                   ]
               }
               
           },
           
           // get the value of the "field" field
           // @param event e The blur event to trigger the change
           getFieldValue         : function (e) {
               //a variable to hold the new field name
               //convert the string to lowercase
               //replace spaces with underscores
               //replace special characters with nothing
               var fieldName = $(e.target).val().toLowerCase().replace(/\W/g, '_');
               //set the hidden field name input to the new value
               $('#field').val(fieldName);
           },
           
           // applies a certain row's configuration so only the correct fields are shown
           // @param string type The field type selected
           applyRowConfiguration : function (type) {
               // all the fields required for every type
               var allFields = Entity.formRow.fieldTypes.all.fields,
               // by default the type is text
               type = (type === undefined) ? 'text' : type,
               // all the fields required for this specific type
               typeFields = Entity.formRow.fieldTypes[type].fields;
               // see if any typeFields are present
               if (typeFields.length > 0) {
                   // add the specific fields to all the fields
                   allFields = $.merge($.merge([],allFields), typeFields);
               }
               // hide all fields on the form
               $('.form_row').hide();
               // go through all fields needing to be shown
               $(allFields).each(function (index, field) {
                   // the field' name (id)
                   var fieldName = ($(field[0]).attr('name') === undefined) ? field[0] : $(field[0]).attr('name'),
                   id = $(field[0]).attr('id'),
                   // the field's value
                   value = field[1];
                   
                   //show the field
                   $('#form_row_' + fieldName + '_label').show();
                   $('#form_row_' + fieldName + '_field').show();
                   $('#form_row_' + fieldName + '_description').show();
                   //see if a default value is set for the field
                   if (value !== '') {
                       $('#' + fieldName).val(value);
                   }
               });
           },
           
           // when the "type" field changes, some fields need to be adjusted
           // @param event e The event that triggered the cgange of this type
           changeFormRowType     : function (e) {
               //the value of the type field
               var type = $('#type').val();
               Entity.formRow.applyRowConfiguration(type);
           }
           
           
           
       }
   });
   
   
   // extend the Events onject
   $.extend(true, Events, {
       options : {
           // when the "type" field changes
           formRowTypeChange : {
               identifier : '#type',
               event      : 'change',
               action     : Entity.formRow.changeFormRowType
           },
           // when the "label" field gets blurred (loses focus)
           formRowLabelBlur : {
               identifier : '#label',
               event      : 'blur',
               action     : Entity.formRow.getFieldValue
           },
           // apply the default configuration when the page loads
           loadDefaultFormRowConfiguration : {
               identifier : 'document',
               event      : 'ready',
               action     : Entity.formRow.applyRowConfiguration
           }
       }
   });