<?php
    //create a new Form Validation object
    $formValidation = new FormValidation();
    //validate the POST data
    $validation = $formValidation->validate($entity->form, $_POST, $database);
    //see if the data contained any errors
    if ($validation['validated'] == 'true') {
        //create a duplicate of the entity
        $oldEntity = clone $entity;
        //this Entity can be updated
        $entity->update($formValidation->input, $entity, $database);
        //update the database row
        $entity->updateDatabaseRow($oldEntity, $entity, $database);
    }
    //set the redirect on success
    $validation['redirect'] = "/entity/{$system->module}/view/{$entity->url}";
    //print the validation results as a JSON string
    echo json_encode($validation);
?>