<div id="toolbar">
    <h1>Trash</h1>
</div>
<?php if (is_array($entities)): ?>
    <table class="data_table">
        <thead>
            <tr>
                <th>Type</th>
                <th>Identifier</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entities as $entityUrl): ?>
                <?php
                    //get the type of Entity that this is
                    $entityType = Entity::getType($database, $entityUrl);
                    //build the Entity
                    $entityObject = EntityFactory::build($entityType);
                    //load the Entity
                    $entityObject->load($entityUrl, $database);
                ?>
                <tr>
                    <td><?php echo $entityObject->form->name; ?></td>
                    <td>
                        <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $entityType, 'view', $system->view, $entityObject->url); ?>">
                            <?php echo $entityObject->{$entityObject->form->urlColumn}; ?>
                        </a>
                    </td>
                    <td class="actions">
                        <?php if (isset($entityObject->form->children['form_action']) && is_array($entityObject->form->children['form_action'])): ?>
                            <?php foreach ($entityObject->form->children['form_action'] as $formActionUrl): ?>
                                <?php
                                    //create a new Form Action object
                                    $formAction = EntityFactory::build('form_action');
                                    //load the Form Action
                                    $formAction->load($formActionUrl, $database);
                                ?>
                                <?php if ($formAction->displayEntity == '1'): ?>
                                    <?php if ((empty($formAction->disallowedStatusses) || !strstr($formAction->disallowedStatusses, $entityObject->status)) && (empty($formAction->allowedStatusses) || strstr($formAction->allowedStatusses, $entityObject->status))): ?>
                                        <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $entityObject->form->formModule, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, $entityObject->url); ?>" title="<?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle : $formAction->action; ?><?php echo ": {$entityObject->{$entityObject->form->urlColumn}}"; ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>">
                                            <?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle : $formAction->action; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="success">There is no Trash</div>
<?php endif; ?>