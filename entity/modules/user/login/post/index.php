<?php
    //see that both fields were filled in
    if (empty($_POST['email_address']) || empty($_POST['password'])) {
        $validation = array(
            'validated' => 'false',
            'groups' => array(
                'user_login' => array(
                    'validated' => 'false',
                    'rows' => array(
                        'email_address' => array(
                            'validated' => 'false',
                            'errorMessage' => 'This field is required'
                        ),
                        'password' => array(
                            'validated' => 'false',
                            'errorMessage' => 'This field is required'
                        )
                    )
                )
            )
        );
    } else {
        //escape the username and password
        $username = mysql_real_escape_string($_POST['email_address']);
        $password = mysql_real_escape_string($_POST['password']);
        //try and log the user in
        $validation = $entity->login($_POST['email_address'], $_POST['password'], $database);
        //see if the validation passed
        if ($validation['validated'] == 'true') {
            //set the login session for this user
            $_SESSION['login'] = $validation['userUrl'];
        }
    }
    
    echo json_encode($validation);
?>