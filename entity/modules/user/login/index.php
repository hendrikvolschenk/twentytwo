<div id="toolbar">
    <div class="heading-box">
        <h1>Login</h1>    
    </div>
</div>
<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, $system->action, $system->templateView); ?>" method="post">
    

    <div class="row">
        <div class="col-md-4 col-md-offset-1">


            <div class="form_group" id="form_group_user_login">
            
            <table>
                <tr class="form_row" id="form_row_label_email_address">
                    <td>
                        <label for="email_address">Username:</label>
                    </td>
                </tr>
                <tr class="form_row" id="form_row_email_address_field">
                    <td>
                        <input type="text" name="email_address" id="email_address" class="text" placeholder="Username" />
                    </td>
                </tr>
                <tr class="form_row" id="form_row_label_password">
                    <td>
                        <label for="password">Password:</label>
                    </td>
                </tr>
                <tr class="form_row" id="form_row_password_field">
                    <td>
                        <input type="password" name="password" id="password" class="text" placeholder="Password" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Login" class="button ajax" style="margin-top:20px;"/>
                    </td>
                </tr>
            </table>

            <a href="#" id="form_group_button_user_login" class="form_group_button hidden"></a>
            <br/>
            </div>


        </div>
    </div>

    
    
</form>