<?php
  // a url to view the user's profile
  $userProfileUrl = System::buildUrl(null, $system->admin, 'user', 'view', null, $system->user->url);
  // a url to log in
  $loginUrl = System::buildUrl(null, $system->admin, 'user', 'login');
  // a url to log out
  $logoutUrl = System::buildUrl('!post', $system->admin, 'user', 'logout');
?>
<ul>
  <li>
    <?php if ($system->user->children['user_group'][0] === 'visitors'): ?>
      <a href="<?php echo $loginUrl; ?>">Login</a>
    <?php else : ?>
      <a href="<?php echo $userProfileUrl; ?>"><?php echo $system->user->firstName . ' ' . $system->user->lastName; ?></a>
      <ul>
        <li>
          <a href="<?php echo $logoutUrl; ?>" class="logout">Logout</a>
        </li>
      </ul>
    <?php endif; ?>
  </li>
</ul>