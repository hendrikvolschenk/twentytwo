<?php
    //get the url column of the Entity
    $urlColumn = $entity->form->urlColumn;
?>
<div id="toolbar">

    <div class="heading-box">
        <div class="row">
            <div class="col-md-8">
                <h1><?php echo $entity->$urlColumn; ?></h1> 
            </div>
            <div class="col-md-4" style="padding-top: 18px;">  
                <?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
                        
                            <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
                                <?php
                                    //create a new Form Action object
                                    $formAction = EntityFactory::build('form_action');
                                    //load the Form Action
                                    $formAction->load($formActionUrl, $database);
                                ?>
                                <?php if ($formAction->displayEntity == '1' && $system->user->hasAccess($formAction) === true): ?>
                                    <?php if ((empty($formAction->disallowedStatusses) || !strstr($formAction->disallowedStatusses, $entity->status)) && (empty($formAction->allowedStatusses) || strstr($formAction->allowedStatusses, $entity->status))): ?>

                                    
                                     
                                            <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, $system->url); ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>" title="<?php echo $formAction->action; ?>">
                                                <input style="margin-right:20px;" type="button" value="<?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle: $formAction->action; ?>" class="button pull-right">
                                            </a>
                                     
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        
                    <?php endif; ?>
            </div>   
        </div>    
    </div>

   
    
</div>
<form action="<?php echo $entity->form->getFormAction($system->module, $system->action); ?>" method="post" id="form_<?php echo "{$system->module}_{$system->action}"; ?>">
    <?php if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])): ?>
        <?php $formGroupChildrenCounter = 0; ?>
        <?php echo $entity->display(650, 400); ?>
        <?php foreach ($entity->form->children['form_group'] as $formGroupUrl): ?>
            <?php
                //increase the counter
                $formGroupChildrenCounter++;
                //get the Form Group As an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl, $database);
            ?>
            <div class="form_group" id="form_group_<?php echo $formGroup->url; ?>">
                <table style="width:100%;">
                    <tbody>
                        <?php if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])): ?>
                            <?php foreach ($formGroup->children['form_row'] as $formRowUrl): ?>
                                <?php
                                    //get the Form Row as an object
                                    $formRow = EntityFactory::build('form_row');
                                    //load the Form Row
                                    $formRow->load($formRowUrl, $database);
                                ?>
                                <tr class="form_row" id="form_row_<?php echo $formRow->url; ?>" style="border-bottom: 1px solid #e3e3e3;">
                                    <td >
                                        <label style="font-size:15px; color:#414141;" class="form_label" id="form_label_<?php echo $formRow->url; ?>">
                                            <?php echo $formRow->label; ?>
                                        </label>
                                    </td>
                                    <td style="padding-top: 20px; padding-left: 20px;">
                                        <?php
                                            //get the field to be showing
                                            $field = lcfirst(functionsString::moduleToClassName($formRow->field));
                                            //set the input of the form row
                                            $formRow->input = $entity->$field;
                                            //show the value of the field
                                            echo $formRow->display();
                                        ?>
                                    </td>
                                </tr>
                                <!--
                                <tr class="form_row description" id="form_row_<?php echo $formRow->url; ?>_description">
                                    <td colspan="2">
                                        <p><?php echo $formRow->description; ?></p>
                                    </td>
                                </tr>
                                -->
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="error">There are no form rows linked to this form group.</div>
                        <?php endif; ?>
                    </tbody>
                </table>
                <table style="width:100%;">
                
                <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                    <?php
                        //get the Form Link as an object
                        $formLink = EntityFactory::build('form_link');
                        //load the Form Link
                        $formLink->load($formLinkUrl, $database);
                    ?>
                    <tr class="form_row link label" id="form_row_<?php echo $formLink->include; ?>_link_label">
                        <td><label><?php echo $formLink->title; ?> (<?php echo ucfirst($formLink->relationship); ?>)</label></td>
                    </tr>
                    <tr class="form_row link" id="form_row_<?php echo $formLink->include; ?>_field">
                        <td>
                            <?php echo $formLink->build($formLink->include, $database, $entity, $system); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                
            </table>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="error">There are no form groups linked to this form.</div>
    <?php endif; ?>
    
    

    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
        <div class="form_group" id="form_group_links">
            <h2 class="form_group_label" id="form_group_label_links">Links</h2>
            <p class="form_group_description" id="form_group_description_link">
                Form Links are created with the aim to be able to link one module to another. A Form Link can, for example, link a Product to a Product Category (as a child of the Product Category), or multiple Images to a Product (as the parent of the Image(s)).
            </p>
            
        </div>
    <?php endif; ?>
    
    
    <div class="form_controls<?php if (count($entity->form->children['form_group']) == 1 && (!isset($entity->form->children['form_link']) || !is_array($entity->form->children['form_link']))): ?> hidden<?php endif; ?>" id="form_controls_<?php echo "{$system->module}_{$system->action}"; ?>">
        
        <div class="clearer"></div>
    </div>
    
</form>