<div id="toolbar">
    <h1>All <?php echo FunctionsString::getPlural($entity->form->name); ?></h1>
</div>
<?php if (!isset($entities) || empty($entities)): ?>
    <div class="message warning">There are no <?php echo FunctionsString::getPlural($entity->form->name); ?>.</div>
<?php else: ?>
    <?php $urlColumn = $entity->form->urlColumn; ?>
    <form action="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, '{action}', null, null); ?>" method="post">
        <table class="data_table">
            <thead>
                <tr>
                    <th class="select">Select</th>
                    
                    <th class="select">Image</th>
                    
                    <th><?php echo ucwords(preg_replace('/([A-Z])/', ' $1', $entity->form->urlColumn)); ?></th>
                    
                    <th>Tag</th>
                    
                    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                        <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                            <?php
                                //get the Form Link as an object
                                $formLink = EntityFactory::build('form_link');
                                //load the Form Link
                                $formLink->load($formLinkUrl, $database);
                            ?>
                            <th><?php echo $formLink->title; ?></th>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($entities as $entityUrl): ?>
                    <?php
                        //create a new Entity object so the old one's info is wiped
                        $currentEntity = EntityFactory::build($system->module);
                        //load the new Entity
                        $currentEntity->load($entityUrl, $database);
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="entity[]" id="<?php echo $currentEntity->url; ?>" value="1" />
                        </td>
                        
                        <td>
                            <?php echo $currentEntity->display(16, 16); ?>
                        </td>
                        
                        <td>
                            <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, 'view', $system->view, $entityUrl); ?>">
                                <?php echo $currentEntity->$urlColumn; ?>
                            </a>
                        </td>
                        
                        <td><?php echo $currentEntity->tag; ?></td>
                        
                        <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                            <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                                <td>
                                    <?php
                                        //get the Form Link as an object
                                        $formLink = EntityFactory::build('form_link');
                                        //load the Form Link
                                        $formLink->load($formLinkUrl, $database);
                                        //get the relationship as a plural
                                        $pluralRelationship = ($formLink->relationship == 'child') ? 'children' : 'parents';
                                    ?>
                                    <?php if (isset($currentEntity->{$pluralRelationship}[$formLink->include]) && is_array($currentEntity->{$pluralRelationship}[$formLink->include])): ?>
                                        <?php
                                            //get the first included Entity as an object
                                            $includedEntity = EntityFactory::build($formLink->include);
                                            //load the Entity
                                            $includedEntity->load($currentEntity->{$pluralRelationship}[$formLink->include][0], $database);
                                        ?>
                                        <a href="<?php echo System::buildUrl('modal', $system->admin, $includedEntity->module, 'view', $system->view, $includedEntity->url); ?>" class="colorbox_iframe">
                                            <?php echo $includedEntity->{$includedEntity->form->urlColumn}; ?>
                                        </a>
                                        <?php if (count($currentEntity->{$pluralRelationship}[$formLink->include]) > 1): ?>
                                            <span class="more_results"> and <?php echo count($currentEntity->{$pluralRelationship}[$formLink->include]) - 1; ?> more</span>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <span class="no_results">None</span>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <td class="actions">
                            <?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
                                <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
                                    <?php
                                        //create a new Form Action object
                                        $formAction = EntityFactory::build('form_action');
                                        //load the Form Action
                                        $formAction->load($formActionUrl, $database);
                                    ?>
                                    <?php if ($formAction->displayEntity == '1'): ?>
                                        <?php if ((empty($formAction->disallowedStatusses) || !strstr($formAction->disallowedStatusses, $currentEntity->status)) && (empty($formAction->allowedStatusses) || strstr($formAction->allowedStatusses, $currentEntity->status))): ?>
                                            <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, $currentEntity->url); ?>" title="<?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle : $formAction->action; ?><?php echo ": {$currentEntity->{$currentEntity->form->urlColumn}}"; ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>">
                                                <?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle : $formAction->action; ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div id="edit_multiple" class="hidden actions">
            <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, 'delete', $system->view, 'multiple'); ?>" title="Delete" class="delete ajax_forward_multiple" id="delete_all_button"></a>
        </div>
    </form>
<?php endif; ?>