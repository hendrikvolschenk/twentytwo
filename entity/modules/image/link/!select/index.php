<?php
    //get the Form Link as an object
    $formLink = EntityFactory::build('form_link');
    //find the url of the Form Link
    $formLinkUrl = $formLink->getUrl($_GET['link_to'], $system->module, $database);
    //load the Form Link
    $formLink->load($formLinkUrl, $database);
    //get the include as an object
    $include = EntityFactory::build($formLink->include);
    //load the include object
    $include->load(' ', $database);
?>
<div id="toolbar">
    <h1>Select <?php echo functionsString::getPlural($include->form->name); ?></h1>
</div>
<p class="form_group_description">
    Select <?php echo ($formLink->multiple == 1) ? 'one or more ' . functionsString::getPlural($include->form->name) : 'only one ' . $formLink->title; ?> to link to the <?php echo $entity->form->name; ?>. 
    <?php echo ($formLink->required == 1) ? 'This Link is required.' : ''; ?>
</p>
<?php
    //get all Entities of type $system->module
    $list = EntityFactory::buildList($database, $system->module);
?>
<?php if ($list == false): ?>
    <div class="notice">The are no <?php echo functionsString::getPlural($formLink->title); ?> to link.</div>
<?php else: ?>
    <form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, $system->action, $system->view, $system->url); ?>" method="post">
        <table style="width:100%;" class="data_table">
            <thead>
                <tr>
                    <th class="select">Select</th>
                    <th><?php echo formRow::getLabel($formLink->include, $include->form->urlColumn, $database); ?></th>
                    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                        <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                            <?php
                                //build a Form Link Entity
                                $formLink = EntityFactory::build('form_link');
                                //load the Form Link
                                $formLink->load($formLinkUrl, $database);
                            ?>
                            <th><?php echo $formLink->title; ?></th>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list as $includeUrl): ?>
                    <?php
                        //get the include as an object
                        $includeObject = EntityFactory::build($entity->module);
                        //load the include object
                        $includeObject->load($includeUrl, $database);
                        //echo $includeObject;
                        //get the url column of the object
                        $urlColumn = $includeObject->form->urlColumn;
                    ?>
                    <tr>
                        <td style="width:15px;">
                            <input type="checkbox" value="1" id="<?php echo $includeUrl; ?>" name="<?php echo $includeUrl; ?>" class="selectable_form_link" />
                        </td>
                        <td>
                            <a href="/<?php echo $formLink->include; ?>/view/<?php echo $includeUrl; ?>" target="_blank">
                                <?php echo $includeObject->display(20, 20, 'center', 'center', null, null, true, true); ?>
                                <?php echo $includeObject->{$entity->form->urlColumn}; ?>
                            </a>
                        </td>
                        <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                            <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                                <?php
                                    //build a Form Link Entity
                                    $formLink = EntityFactory::build('form_link');
                                    //load the Form Link
                                    $formLink->load($formLinkUrl, $database);
                                    //get the relationship
                                    $relationship = ($formLink->relationship == 'child') ? 'children' : 'parents';
                                ?>
                                <td>
                                    <?php if (isset($includeObject->{$relationship}[$formLink->include]) && is_array($includeObject->{$relationship}[$formLink->include])): ?>
                                        <?php
                                            //build an Entity of the include type
                                            $linkedEntity = EntityFactory::build($formLink->include);
                                            //load the Entity
                                            $linkedEntity->load($includeObject->{$relationship}[$formLink->include][0], $database);
                                            $linkedEntityUrlColumn = $linkedEntity->form->urlColumn;
                                        ?>
                                        <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $linkedEntity->form->formModule, 'view', null, $linkedEntity->url); ?>">
                                            <?php echo $linkedEntity->{$linkedEntity->form->urlColumn}; ?>
                                        </a>
                                        <?php if (count($includeObject->{$relationship}[$formLink->include]) > 1): ?>
                                            <span class="more_results"> and <?php echo count($includeObject->{$relationship}[$formLink->include]) - 1; ?> more.</span>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <span class="no_results">None</span>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="clearer"></div>
        <br /><br />
        <input type="hidden" name="module" id="module" value="<?php echo $system->module; ?>" />
        <input type="submit" class="ajax_link button" value="Link" /> <input type="button" class="button" value="Cancel" onclick="parent.$.fn.colorbox.close();" />
        <div class="clearer"></div>
    </form>
<?php endif; ?>