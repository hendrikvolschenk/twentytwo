$(document).ready(function() {

    // extend the Entity object
    $.extend(true, Entity, {
        // The form object holds all the functions necessary to post/submit a form
        form       : {
            // uploads an image
            // @param event e The change event that fired the upload
            uploadImage : function (e) {
                // get the button that was pressed
                var button = $(e.target),
                // the html element that holds the percentage
                percent = $('.percent'),
                // the html element that represents the percentage
                bar = $('.bar'),
                // the actual html form
                form = button.parents('form').first();
                // show the progress bar
                Entity.form.toggleProgressBar();
                // submit the form
                form.submit();
            },
            // toogles showing and hiding of the progress bar
            toggleProgressBar : function () {
                // the progress bar element
                var progressBar = $('div.progress');
                // toggle the hidden class on the div
                progressBar.toggleClass('hidden');
            }
        },
        // extensions that need to be bound/added
        extensions : {
            // set a list of options
            options : {
                // an extension for the image upload form
                imageUpload : {
                    identifier : 'form',
                    extension  : 'ajaxForm',
                    options    : {
                        // extra data to go along with the request
                        data: {image_upload: '1'},
                        // the data-type the form is expecting back
                        dataType:  'json',
                        // a function to run before the upload begins
                        beforeSend: function() {
                            // the html element that represents the percentage
                            var bar = $('.bar'),
                            // the html element that holds the percentage
                            percent = $('.percent');
                            // set the percentage bar to 0%
                            bar.width('0%');
                            // set the ercentage to zero
                            percent.html('0%');
                        },
                        // a function to run as the upload progresses
                        // @param event event The javascript event
                        // @param integer position The position
                        // @param integer total The total length of the bar
                        // @param integer percentComplete How far the upload is
                        uploadProgress: function(event, position, total, percentComplete) {
                            // work out the percentage value
                            var pVel = percentComplete + '%',
                            // the html element that represents the percentage
                            bar = $('.bar'),
                            // the html element that holds the percentage
                            percent = $('.percent');
                            // set the new width of the percentage bar
                            bar.width(pVel);
                            // set the new value of the percentage text
                            percent.html(pVel + ((percentComplete == 100) ? '. Saving ...' : ' Uploaded.'));
                        },

                        // a function to run when the upload is complete
                        // @param json data The data returned by submitting the form
                        complete: function(data) {
                            // parse the data as json
                            var jsonResponse = JSON.parse(data.responseText),
                            // the html element that holds the percentage
                            percent = $('.percent'),
                            // the html element that represents the percentage
                            bar = $('.bar'),
                            // the full progress bar
                            progress = $('.progress');
                            // check if the upload was successful
                            if (jsonResponse.uploaded == 'true') {
                                // fill in the actual filename of the uploaded image
                                $('#filename').val(jsonResponse.fileName);
                                // fill in the new path of the uploaded image
                                $('#path').val(jsonResponse.path);
                                // fill in the physical file size of the file (in bytes)
                                $('#filesize').val(jsonResponse.fileSize);
                                // fill in the width of the uploaded image (in pixels)
                                $('#width').val(jsonResponse.width);
                                // fill in the height of the uploaded image (in pixels)
                                $('#height').val(jsonResponse.height);
                                //disable the button to prevent another upload
                                $('#image').attr('disabled', 'disabled');
                                // remove all form errors
                                $('#form_row_image_error').remove();
                                // show that the upload is complete
                                percent.html('100%. Done.');
                                // remove ajaxForm from the form
                                $('form').ajaxFormUnbind();
                            // the upload failed
                            } else {
                                // remove the error type from all fields
                                $('.validation_error').removeClass('validation_error');
                                //remove the error messages from the form (if any)
                                $('.form_row_error').remove();
                                // show the error message on screen
                                $('#form_row_image_field').after('<tr class="form_row_error" id="form_row_image_error"><td><p>' + jsonResponse.error + '</p></td></tr>');
                                // set the width of the percentage bar to 0
                                bar.width('0%');
                                // set the percentage text to 0%
                                percent.html('0%');
                                // hide the progress bar
                                progress.hide();
                            }
                        }
                    }
                }
            }
        }
    });
    
    // extend the events object
    $.extend(true, Events, {
        // set a list of options
        options : {
            // image uploads
            imageUpload : {
                identifier : '#image',
                event      : 'change',
                action     : Entity.form.uploadImage
            },
            // hiding the progress bar
            toggleProgressBar : {
              identifier : 'document',
              event      : 'ready',
              action     : Entity.form.toggleProgressBar  
            }
        }
    });

});