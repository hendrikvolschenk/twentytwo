<?php
    //see if the form was submitted to create an Image object or to upload an image
    if (isset($_POST['image_upload']) && $_POST['image_upload'] == '1') {
        //output json
        header('Content-type: application/json');
        //show the output of the Image upload operation
        echo json_encode(Image::upload($_FILES['image']));
    } else {
        //Create an Image object
        //include the usual post create page
        require_once(dirname(__FILE__) . '/../../../../entity/link/!create/post/index.php');
    }
?>