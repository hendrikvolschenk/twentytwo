<div id="toolbar">
    <div class="heading-box">
        <h1>Create a New <?php echo $entity->form->name; ?></h1>
    </div>
</div>
<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, $system->action, $system->view); ?>" method="post" id="form_<?php echo "{$system->module}_{$system->action}"; ?>">
    <?php if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])): ?>
        <?php $formGroupChildrenCounter = 0; ?>
        <?php foreach ($entity->form->children['form_group'] as $formGroupUrl): ?>
            <?php
                //increase the counter
                $formGroupChildrenCounter++;
                //get the Form Group As an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl, $database);
            ?>
            <div class="form_group" id="form_group_<?php echo $formGroup->url; ?>">
                <table style="width:100%;">
                    <tbody>
                        <?php if ($formGroupChildrenCounter == 1): ?>
                            <div class="status"></div>
                            <tr class="form_row label" id="form_row_image_label">
                                <td>
                                    <label for="image">Choose Image</label>
                                </td>
                            </tr>
                            <tr class="form_row field" id="form_row_image_field">
                                <td>
                                    <input id="image" type="file" name="image" />
                                    <div class="progress" style="position: relative; border: 1px solid #bbb; width: 304px; height: 20px; margin: 3px 0 0 0;">
                                        <div class="bar" style="background: #005daa; height: 20px; width: 0%; -webkit-transition: width .3s; -moz-transition: width .3s; transition: width .3s; position:absolute; top: 0; left: 0;"></div>
                                        <div class="percent" style="position:absolute; top: 0; left: 5px; color: #fff;">0%</div >
                                    </div>
                                </td>
                            </tr>
                            <tr class="form_row description" id="form_row_image_description">
                                <td>
                                    <p>Choose an Image to be uploaded</p>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?php if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])): ?>
                            <?php foreach ($formGroup->children['form_row'] as $formRowUrl): ?>
                                <?php
                                    //get the Form Row as an object
                                    $formRow = EntityFactory::build('form_row');
                                    //load the Form Row
                                    $formRow->load($formRowUrl, $database);
                                ?>
                                <?php if ($formRow->type != 'checkbox'): ?>
                                    <tr class="form_row label<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_label">
                                        <td>
                                            <label<?php if ($formRow->type != 'radio'): ?> for="<?php echo $formRow->field; ?>"<?php endif; ?> class="form_label" id="form_label_<?php echo $formRow->url; ?>">
                                                <?php echo $formRow->label; ?>
                                            </label>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr class="form_row field<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_field">
                                    <td>
                                        <?php echo $formRow->buildInput(); ?>
                                    </td>
                                </tr>
                                <tr class="form_row description<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_description">
                                    <td>
                                        <p><?php echo $formRow->description; ?></p>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="error">There are no form rows linked to this form group.</div>
                        <?php endif; ?>
                    </tbody>
                </table>
                <table style="width:100%;">
                    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                        <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                            <?php
                                //get the Form Link as an object
                                $formLink = EntityFactory::build('form_link');
                                //load the Form Link
                                $formLink->load($formLinkUrl, $database);
                            ?>
                            <tr class="form_row link label" id="form_row_<?php echo $formLink->include; ?>_link_label">
                                <td><label><?php echo $formLink->title; ?> (<?php echo ucfirst($formLink->relationship); ?>)</label></td>
                            </tr>
                            <tr class="form_row link" id="form_row_<?php echo $formLink->include; ?>_field">
                                <td>
                                    <?php echo $formLink->build($formLink->include, $database, $entity, $system); ?>
                                </td>
                            </tr>
                            <tr class="form_row description" id="form_row_<?php echo $formLink->include; ?>_description">
                                <td>
                                    <p><?php echo $formLink->description; ?></p>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="error">There are no form groups linked to this form.</div>
    <?php endif; ?>
    <?php if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])): ?>
        <div class="form_controls<?php if (count($entity->form->children['form_group']) == 1 && (!isset($entity->form->children['form_link']) || !is_array($entity->form->children['form_link']))): ?> hidden<?php endif; ?>" id="form_controls_<?php echo "{$system->module}_{$system->action}"; ?>">
            <ul>
                <?php
                    //count each time the loop has gone through
                    $counter = 0;
                    //the maximum amount
                    $max = count($entity->form->children['form_group']);
                ?>
                <?php foreach ($entity->form->children['form_group'] as $formGroupUrl): ?>
                    <?php
                        //increase the counter
                        $counter++;
                        //get the Form Row as an object
                        $formGroup = EntityFactory::build('form_group');
                        //load the Form Row
                        $formGroup->load($formGroupUrl, $database);
                    ?>
                    <li class="<?php echo ($counter == 1) ? ' first' : ''; ?><?php echo ((!isset($entity->form->children['form_link']) || !is_array($entity->form->children['form_link'])) && ($counter == $max)) ? ' last' : ''; ?>">
                        <a class="form_group_button" id="form_group_button_<?php echo $formGroup->url; ?>"><?php echo $counter; ?>. <?php echo $formGroup->title; ?></a>
                    </li>
                <?php endforeach; ?>
                <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                    <li class="last">
                        <a class="form_group_button" id="form_group_button_links"><?php echo $counter+1; ?>. Links</a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="clearer"></div>
        </div>
        <div class="form_buttons" id="form_buttons_<?php echo "{$system->module}_{$system->action}"; ?>">
            <div class="row bottom-row">
                <input type="submit" value="Create" class="<?php echo ($system->action == 'link') ? 'ajax_link_create' : 'ajax'; ?> button" />
                <input type="button" value="Cancel" class="button" onclick="<?php echo ($system->action == 'link') ? 'parent.jQuery.colorbox.close()' : 'history.go(-1)'; ?>" />
                <input type="hidden" name="module" id="module" value="<?php echo $system->module; ?>" />
            </div>
        </div>
    <?php endif; ?>
</form>