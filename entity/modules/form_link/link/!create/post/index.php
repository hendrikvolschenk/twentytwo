<?php
    //check if any forms were selected/created
    if (isset($_POST['form']) && is_array($_POST['form'])) {
        //create a new Form object
        $form = EntityFactory::build('form');
        //there will always only be one, get the first one as an object
        $form->load($_POST['form'][0], $database);
        //set the 'include' POST variable equal to the `form_module` of the Form
        $_POST['include'] = $form->formModule;
    }
    //include the default "create" page located at /entity/modules/entity/create/post/index.php
    require_once(dirname(__FILE__) . '/../../../../entity/link/!create/post/index.php');
?>