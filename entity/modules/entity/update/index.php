<div id="toolbar">
    <?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
        <ul>
            <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
                <?php
                    //create a new Form Action object
                    $formAction = EntityFactory::build('form_action');
                    //load the Form Action
                    $formAction->load($formActionUrl, $database);
                ?>
                <?php if ($formAction->moduleMenu == '1'): ?>
                    <li<?php echo ($system->action == strtolower(str_replace(' ', '_', $formAction->action))) ? ' class="selected"' : ''; ?>>
                        <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $entity->form->formModule, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, null); ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>" title="<?php echo $formAction->action; ?>">
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <h1><?php echo "{$system->formAction->action} {$entity->form->name} {$entity->{$entity->form->urlColumn}}"; ?></h1>
</div>
<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, $system->action, $system->view, $entity->url); ?>" method="post" id="form_<?php echo "{$system->module}_{$system->action}"; ?>">
    <?php if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])): ?>
        <?php $formGroupChildrenCounter = 0; ?>
        <?php foreach ($entity->form->children['form_group'] as $formGroupUrl): ?>
            <?php
                //increase the counter
                $formGroupChildrenCounter++;
                //get the Form Group As an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl, $database);
            ?>
            <div class="form_group" id="form_group_<?php echo $formGroup->url; ?>">
                <?php if (count($entity->form->children['form_group']) > 1 || (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link']))): ?>
                    <h2 class="form_group_label" id="form_group_label_<?php echo "{$system->module}_{$system->action}"; ?>"><?php echo $formGroupChildrenCounter; ?>. <?php echo $formGroup->title; ?></h2>
                <?php endif; ?>
                <p class="form_group_description" id="form_group_description_<?php echo $formGroup->url; ?>"><?php echo $formGroup->description; ?></p>
                <table>
                    <tbody>
                        <?php if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])): ?>
                            <?php foreach ($formGroup->children['form_row'] as $formRowUrl): ?>
                                <?php
                                    //get the Form Row as an object
                                    $formRow = EntityFactory::build('form_row');
                                    //load the Form Row
                                    $formRow->load($formRowUrl, $database);
                                ?>
                                <?php if ($formRow->type != 'checkbox'): ?>
                                    <tr class="form_row label<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_label">
                                        <td>
                                            <label<?php if ($formRow->type != 'radio'): ?> for="<?php echo $formRow->field; ?>"<?php endif; ?> class="form_label" id="form_label_<?php echo $formRow->url; ?>">
                                                <?php echo $formRow->label; ?>
                                            </label>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr class="form_row field<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_field">
                                    <td>
                                        <?php
                                            //get the field name
                                            $fieldName = lcfirst(functionsString::moduleToClassName($formRow->field));
                                            //build the input
                                            echo $formRow->buildInput($entity->$fieldName);
                                        ?>
                                    </td>
                                </tr>
                                <tr class="form_row description<?php echo ($formRow->type == 'hidden') ? ' hidden' : ''; ?>" id="form_row_<?php echo $formRow->field; ?>_description">
                                    <td>
                                        <p><?php echo $formRow->description; ?></p>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="error">There are no form rows linked to this form group.</div>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="error">There are no form groups linked to this form.</div>
    <?php endif; ?>
    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
        <div class="form_group" id="form_group_links">
            <h2 class="form_group_label" id="form_group_label_links"><?php echo $formGroupChildrenCounter + 1; ?>. Links</h2>
            <p class="form_group_description" id="form_group_description_link">
                Form Links are created with the aim to be able to link one module to another. A Form Link can, for example, link a Product to a Product Category (as a child of the Product Category), or multiple Images to a Product (as the parent of the Image(s)).
            </p>
            <table>
                <?php foreach ($entity->form->children['form_link'] as $formLinkUrl): ?>
                    <?php
                        //get the Form Link as an object
                        $formLink = EntityFactory::build('form_link');
                        //load the Form Link
                        $formLink->load($formLinkUrl, $database);
                        //create a new Form object
                        $linkedForm = EntityFactory::build('form');
                        //Load the form linked to the Form Link
                        $linkedForm->load($formLink->children['form'][0], $database);
                    ?>
                    <tr class="form_row link label" id="form_row_<?php echo $formLink->include; ?>_link_label">
                        <td><label><?php echo $formLink->title; ?> (<?php echo ucfirst($formLink->relationship); ?>)</label></td>
                    </tr>
                    <tr class="form_row link" id="form_row_<?php echo $formLink->include; ?>_field">
                        <td>
                            <?php echo $formLink->build($formLink->include, $database, $entity, $system); ?>
                        </td>
                    </tr>
                    <tr class="form_row description" id="form_row_<?php echo $formLink->include; ?>_description">
                        <td>
                            <p><?php echo $formLink->description; ?></p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    <?php endif; ?>
    <?php if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])): ?>
        <div class="form_controls<?php if (count($entity->form->children['form_group']) == 1 && (!isset($entity->form->children['form_link']) || !is_array($entity->form->children['form_link']))): ?> hidden<?php endif; ?>" id="form_controls_<?php echo "{$system->module}_{$system->action}"; ?>">
            <ul>
                <?php
                    //count each time the loop has gone through
                    $counter = 0;
                    //the maximum amount
                    $max = count($entity->form->children['form_group']);
                ?>
                <?php foreach ($entity->form->children['form_group'] as $formGroupUrl): ?>
                    <?php
                        //increase the counter
                        $counter++;
                        //get the Form Row as an object
                        $formGroup = EntityFactory::build('form_group');
                        //load the Form Row
                        $formGroup->load($formGroupUrl, $database);
                    ?>
                    <li class="<?php echo ($counter == 1) ? ' first' : ''; ?><?php echo ((!isset($entity->form->children['form_link']) || !is_array($entity->form->children['form_link'])) && ($counter == $max)) ? ' last' : ''; ?>">
                        <a class="form_group_button" id="form_group_button_<?php echo $formGroup->url; ?>"><?php echo $counter; ?>. <?php echo $formGroup->title; ?></a>
                    </li>
                <?php endforeach; ?>
                <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
                    <li class="last">
                        <a class="form_group_button" id="form_group_button_links"><?php echo $counter+1; ?>. Links</a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="clearer"></div>
        </div>
        <div class="form_buttons" id="form_buttons_<?php echo "{$system->module}_{$system->action}"; ?>">
            <input type="submit" value="Update" class="<?php echo ($system->action == 'link') ? 'ajax_link_create' : 'ajax'; ?> button" />
            <input type="button" value="Cancel" class="button" onclick="history.go(-1);" />
            <input type="hidden" name="module" id="module" value="<?php echo $system->module; ?>" />
        </div>
    <?php endif; ?>
</form>