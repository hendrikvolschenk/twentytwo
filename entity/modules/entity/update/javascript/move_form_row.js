// extend the Entity object to add the up and down functionality
$.extend(true, Entity, {
    // holds all form-related methods
    form : {
        // holds the form-row related methods
        row : {
            // moves a row up
            // @param event e The click event to move the row up
            moveUp : function (e) {
                // move the row
                Entity.form.row.move(e, 'up');
            },
            // moves a row down
            // @param event e The click event to move the row up
            moveDown : function (e) {
                // move the row
                Entity.form.row.move(e, 'down');
            },
            // moves a row
            // @param event e The click event to move the row up
            // @param string direction The direction in which to move the row (up|down)
            move : function (e, direction) {
                // the button that was pressed
                var button = $(e.target),
                // the row that needs to move
                moveRow = button.closest('tr');
                // check if this is the top row in the table or in the bottom row
                if ((direction === 'up' && !moveRow.is(':first-child')) || (direction === 'down' && !moveRow.is(':last-child'))) {
                    // check if the row needs to move up or down and move it
                    moveRow[direction === 'up' ? 'insertBefore' : 'insertAfter'](direction === 'up' ? moveRow.prev() : moveRow.next());
                }
                // re-color form rows
                Entity.form.row.recolor(moveRow);
            },
            // re-colors all rows that are siblings of the current
            recolor : function (row) {
                // find the table this row is in
                var table = row.closest('table'),
                // all rows in this table
                rows = table[0].rows;
                // add the 'odd' class to odd rows
                $(rows).filter(':odd').removeClass('odd even').addClass('odd');
                // add the 'even' class to even rows
                $(rows).filter(':even').removeClass('odd even').addClass('even');
                
            }
        }
    }
});

// extend the Events object to add the two click events (up and down)
$.extend(true, Events, {
    // events are loaded as options
    options : {
        // moving a form row up
        moveFormRowUp : {
            identifier : '.move_up',
            event      : 'click',
            action     : Entity.form.row.moveUp
        },
        // moving a form row down
        moveFormRowDown : {
            identifier : '.move_down',
            event      : 'click',
            action     : Entity.form.row.moveDown
        }
    }
});