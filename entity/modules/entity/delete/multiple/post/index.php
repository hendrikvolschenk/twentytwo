<?php
    //go through each of the Entities to delete
    foreach ($entities as $entityUrl) {
        //get the Entity as an object
        $entity = EntityFactory::build($system->module);
        //load the entity
        $entity->load($entityUrl, $database);
        //delete the entity
        $entity->delete($database);
    }
    //set an array to return
    $return = array(
        'validated' => 'true',
        'redirect' => System::buildUrl('', $system->admin, $system->module, 'view', null, 'all')
    );
    //show the output as JSON
    echo json_encode($return);
?>