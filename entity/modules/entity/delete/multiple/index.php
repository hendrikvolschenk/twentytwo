<h1>Delete Multiple <?php echo ucwords(functionsString::getPlural(functionsString::removeUnderscores($system->module))); ?></h1>
<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, $system->action, null, $system->url); ?>" method="post">
    <p>
        Are you sure you would like to delete the selected <?php echo count($entities); ?> <?php echo ucwords(functionsString::getPlural(functionsString::removeUnderscores($system->module))); ?>?
        <br /><br />
        <input type="submit" value="Delete" class="ajax button" /> <input type="button" value="Cancel" class="button" onclick="history.go(-1);" />
    </p>
</form>