<div id="toolbar">
<div class="heading-box">
            <h1><?php echo ($entity->status == 'D') ? 'Permanently ' : ''; ?>Delete a <?php echo $entity->form->name; ?>: "<?php echo $entity->{$entity->form->urlColumn}; ?>"</h1>
        </div>

    <?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
        <ul>
            <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
                <?php
                    //create a new Form Action object
                    $formAction = EntityFactory::build('form_action');
                    //load the Form Action
                    $formAction->load($formActionUrl, $database);
                ?>
                <?php if ($formAction->entity == '1'): ?>
                    <li<?php echo ($system->action == strtolower(str_replace(' ', '_', $formAction->action))) ? ' class="selected"' : ''; ?>>
                        <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, $system->url); ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>" title="<?php echo $formAction->action; ?>">
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    
    
</div>
 <div class="row">
        <div class="col-md-4 col-md-offset-1">

<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, 'delete', $system->view, $system->url); ?>" method="post">
    <p>
        Are you sure you would like to<?php echo ($entity->status == 'D') ? ' permanently' : ''; ?> delete the <?php echo $entity->form->name; ?>: "<?php echo $entity->{$entity->form->urlColumn}; ?>"?
        <br /><br />
        <?php if ($entity->status === 'D'): ?>
            <?php echo $entity->getLinkedEntitiesList($entity, $database, $system); ?>
            <br /><br />
        <?php endif; ?>
        <input type="submit" class="ajax_general button" value="Delete" /> <input type="button" class="button" value="Cancel" onclick="history.go(-1);" />
    </p>
</form>
</div>
</div>