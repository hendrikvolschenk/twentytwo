<?php
    //check whether the Entity has already been deleted
    if ($entity->status === 'D') {
        //this item must be permanently deleted
        $entity->permanentlyDelete($database);
        //check if any other Entities need to be deleted together with this one
        if (isset($_POST['linked_entity']) && is_array($_POST['linked_entity'])) {
            //go through each item to delete
            foreach ($_POST['linked_entity'] as $linkedEntityUrl) {
                //get the type for this Entity
                $type = Entity::getType($database, $linkedEntityUrl);
                //build a new object for this Entity type
                $linkedEntity = EntityFactory::build($type);
                //load the linked Entity
                $linkedEntity->load($linkedEntityUrl, $database);
                //permanently delete the linked Entity
                $linkedEntity->permanentlyDelete($database);
            }
        }
        //delete was successful
        $validation = array(
            'validated' => 'true',
            'redirect' => System::buildUrl('', $system->admin, 'trash', 'view', null, 'all')
        );
    } else {
        //check whether the entity could be deleted
        if ($entity->delete($database) == false) {
            //something went wrong, show an error
            $validation = array(
                'validated' => 'false',
                'error_message' => "There was an error while Deleting the {$entity->settings['name']}. Please try again."
            );
        } else {
            //delete was successful
            $validation = array(
                'validated' => 'true',
                'redirect' => System::buildUrl('', $system->admin, $system->module, 'view', null, 'all')
            );
        }
    }
    //show the output
    echo json_encode($validation);
?>