<?php
    //create a new Form Validation object
    $formValidation = new FormValidation();
    
    //validate the POST data
    $validation = $formValidation->validate($entity->form, $_POST, $database);
    
    //see if the data contained any errors
    if ($validation['validated'] == 'true') {
        //this Entity can be created
        $entity->create($formValidation->input, $entity->form, $database);
    }
    
    //set the redirect on success
    $validation['url'] = $entity->url;
    
    //print the validation results,
    //the 'ajax' class on the submit button fires the ajax call to this page
    //it is expecting a JSON return
    echo json_encode($validation);
?>