<?php
    global $template;
    //echo 'III<pre>',print_r(get_defined_vars()),'</pre>';
    //build the include as an object
    $include = EntityFactory::build($formLink->include);
    //load the object with an empty url to not load an Entity, but at least load the settings
    $include->load(' ', $database);
    //get the relationship of the include to the module
    $relationship = functionsString::getPlural($formLink->relationship);
    //get the relationship as an array
    $relationship = $entity->$relationship;
    //a variable to count how many objects are linked to the include
    $includeCount = 1;
?>
<table class="form_link data_table_simple" id="form_link_<?php echo $module ?>_<?php echo $formLink->include; ?>">
    <thead>
        <tr>
            <th><?php echo ucwords(preg_replace('/([A-Z])/', ' $1', $include->form->urlColumn)); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($relationship[$formLink->include]) && is_array($relationship[$formLink->include])): ?>
            <?php foreach($relationship[$formLink->include] as $includeUrl): ?>
                <?php
                    //build the include as an object
                    $includeObject = EntityFactory::build($formLink->include);
                    //load the object
                    $includeObject->load($includeUrl, $database);
                ?>
                
                <tr class="table_row link" id="link_row_<?php echo $includeObject->url; ?>">
                    <td>
                        <?php if ($system->action != 'view'): ?>
                            <a class="remove_row_link">x</a>
                            <a class="move_up">&uarr;</a><a class="move_down">&darr;</a>
                        <?php endif; ?>
                        <a href="/!modal/entity/<?php echo $formLink->include; ?>/view/<?php echo $includeObject->url; ?>" class="colorbox_iframe"><?php echo "{$includeObject->{$includeObject->form->urlColumn}}"; ?></a>
                        <?php if ($system->action != 'view'): ?>
                            <input type="hidden" name="<?php echo $formLink->include; ?>[]" value="<?php echo $includeObject->id; ?>" />
                        <?php endif; ?>
                    </td>
                </tr>
                
                <?php //echo $template->loadModule($system, $entity, $database, $formLink->include, 'view', '!table_row', $includeObject->url); ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<?php if ($formLink->create == 1 && ($system->action =='create' || $system->view == '!create' || $system->action == 'clone' || $system->action == 'update')): ?>
    <a href="<?php echo $createFile; ?>?link_to=<?php echo $system->module; ?>" class="colorbox_iframe button">
        New <?php echo $include->form->name; ?>
    </a>&nbsp;
<?php endif; ?>
<?php if ($formLink->select == 1 && ($system->action =='create' || $system->view =='!create' || $system->action == 'clone' || $system->action == 'update')): ?>
    <a href="<?php echo $selectFile; ?>?link_to=<?php echo $system->module; ?>" class="colorbox_iframe button">
        Select <?php echo functionsString::getPlural($include->form->name); ?>
    </a>&nbsp;
<?php endif; ?>
<div class="clearer"></div>