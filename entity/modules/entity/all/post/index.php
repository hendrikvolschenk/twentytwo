<?php
    //empty variables as placeholders
    $validated = 'false';
    $errorMessage = 'Undefined error';
    $redirect = '';
    //get the name of the action as a method name
    $methodName = str_replace(' ', '', ucwords($system->formAction->action));
    //check if the method exists
    if (!method_exists($entity, $methodName)) {
        //show an error message
        $validated = 'false';
        $errorMessage = "The '{$system->formAction->action}' method does not exist for {functionsString::getPlural($entity->form->name)}.";
    } else {
        //call the method
        $validated = $entity->{$methodName}($database);
        $errorMessage = ($validated == false) ? "There was an error while trying to {$system->formAction->action} the {$entity->form->name} {$entity->{$entity->form->urlColumn}}" : '';
        $redirect = System::buildUrl(null, $system->admin, $system->module, 'view', $system->view, 'all');
    }
    //build the validation array
    $validation = array(
        'validated' => $validated,
        'errorMessage' => $errorMessage,
        'redirect' => $redirect
    );
    //show the validation as json
    echo json_encode($validation);
?>