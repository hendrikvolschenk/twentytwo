<div id="toolbar">
    <h1><?php echo "{$system->formAction->action} {$entity->form->name} {$entity->{$entity->form->urlColumn}}"; ?></h1>
</div>
<form action="<?php echo System::buildUrl('!post', $system->admin, $system->module, strtolower(str_replace(' ', '_', $system->formAction->action)), $system->view, $system->url); ?>" method="post">
    <p>
        Are you sure you would like to <?php echo $system->formAction->action ?> the <?php echo $entity->form->name; ?>: "<?php echo $entity->{$entity->form->urlColumn}; ?>"?
        <br /><br />
        <input type="submit" class="ajax_general button" value="<?php echo $system->formAction->action; ?>" /> <input type="button" class="button" value="Cancel" onclick="history.go(-1);" />
    </p>
</form>