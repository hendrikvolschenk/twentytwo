<tr class="table_row link" id="link_row_<?php echo $entity->url; ?>">
    <td>
        <a class="remove_row_link">x</a>
        <a class="move_up">&uarr;</a><a class="move_down">&darr;</a>
        <a href="/!modal/entity/<?php echo $system->module; ?>/view/<?php echo $entity->id; ?>" class="colorbox_iframe"><?php echo $entity->{$entity->form->urlColumn}; ?></a>
        <input type="hidden" name="<?php echo $system->module; ?>[]" value="<?php echo $entity->id; ?>" />
    </td>
    <!--
    <?php if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])): ?>
        <?php foreach ($entity->form->children['form_link'] as $child): ?>
            <?php
                $formLinkLink = EntityFactory::build('form_link');
                $formLinkLink->load($child, $database);
                $formLinkLinkRelationship = FunctionsString::getPlural($formLinkLink->relationship);
            ?>
            <td>
                <?php if (isset($includeObject->{$formLinkLinkRelationship}[$formLinkLink->include]) && is_array($includeObject->{$formLinkLinkRelationship}[$formLinkLink->include])): ?>
                    <?php
                        $linkedEntity = EntityFactory::build($formLinkLink->include);
                        $linkedEntity->load($includeObject->{$formLinkLinkRelationship}[$formLinkLink->include][0], $database);
                        $linkedEntityUrlColumn = $linkedEntity->form->urlColumn;
                    ?>
                    <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $linkedEntity->form->formModule, 'view', null, $linkedEntity->url); ?>">
                        <?php echo $linkedEntity->$linkedEntityUrlColumn; ?>
                    </a>
                    <?php if (count($includeObject->{$formLinkLinkRelationship}[$formLinkLink->include]) > 1): ?>
                        <span class="more_results"> and <?php echo count($includeObject->{$formLinkLinkRelationship}[$formLinkLink->include]) - 1; ?> more.</span>
                    <?php endif; ?>
                <?php else: ?>
                    <span class="no_results">None</span>
                <?php endif; ?>
            </td>
        <?php endforeach; ?>
    <?php endif; ?>
    -->
</tr>