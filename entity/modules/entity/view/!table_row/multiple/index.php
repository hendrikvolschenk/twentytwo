<?php foreach ($entities as $entityUrl): ?>
    <?php
        //create the new Entity
        $entity = EntityFactory::build($system->module);
        //load the Entity
        $entity->load($entityUrl, $database);
        //get the url column of the Entity
        $urlColumn = $entity->form->urlColumn;
    ?>
    <tr class="table_row link" id="link_row_<?php echo $entity->url; ?>">
        <td>
            <a class="remove_row_link">x</a>
            <a href="/!modal/entity/<?php echo $system->module; ?>/view/<?php echo $entity->url; ?>" class="colorbox_iframe"><?php echo $entity->$urlColumn; ?></a>
            <input type="hidden" name="<?php echo $system->module; ?>[]" value="<?php echo $entity->id; ?>" />
            <a class="move_up">&uarr;</a><a class="move_down">&darr;</a>
        </td>
    </tr>
<?php endforeach; ?>