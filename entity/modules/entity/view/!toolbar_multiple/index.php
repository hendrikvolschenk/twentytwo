<?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
    <ul>
        <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
            <?php
                //create a new Form Action object
                $formAction = EntityFactory::build('form_action');
                //load the Form Action
                $formAction->load($formActionUrl, $database);
            ?>
            <?php if ($formAction->multiple == '1'): ?>
                <li<?php echo ($system->action == strtolower(str_replace(' ', '_', $formAction->action))) ? ' class="selected"' : ''; ?>>
                    <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $entity->form->formModule, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, 'multiple'); ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?> ajax_forward_multiple" title="<?php echo (isset($formAction->moduleMenuTitle) && !empty($formAction->moduleMenuTitle)) ? $formAction->moduleMenuTitle : $formAction->action; ?>">
                        <?php echo (isset($formAction->moduleMenuTitle) && !empty($formAction->moduleMenuTitle)) ? $formAction->moduleMenuTitle: $formAction->action; ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>