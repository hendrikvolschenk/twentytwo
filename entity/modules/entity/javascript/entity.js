var Entity, Events;

// The Entity object that holds all the necessary Javascript for the page to function properly
Entity = {

    // a function to initialise Entity
    initialize : function () {
        // bind events
        Events.bind();
        // load all necessary extensions
        Entity.extensions.load();
        // swap to the correct form group (if necessary)
        Entity.form.swapFormGroup();
    },

    // The form object holds all the functions necessary to post/submit a form
    form       : {

        // Does an ajax call to the action attribute of the form
        // @param event e The event that triggered the form submission
        // @param object Parameters for the submission of the form
        postForm             : function (e, parameters) {
            // the button that was pressed to submit this form
            var button = $(e.target),
            // The actual form that was submitted
            form = button.parents('form:first'),
            // options for the submission of the form
            options = {
                // where the form is sending its data to
                action               : form.attr('action'),
                // whether this form must be sent via POST or GET
                method               : form.attr('method') || 'post',
                // a string of all the form fields that will be sent along with the call
                fields               : form.serialize(),
                // The module of the form
                module               : $(form.find('#module')[0]).val(),
                // the type of response the post page returns (xml | json | script | or html)
                dataType             : 'json',
                // additional parameters to be sent along with the fields (serialized, starting with ampersand (&))
                additionalParameters : '',
                // The timeout period before the ajax call stops
                timeout              : 60000,
                // a callback function for when the form submission is done and successful
                // @param string returnText The string returned by the ajax call
                callbackSuccess      : function (returnText) {
                    console.log(returnText);
                    // parse the return text as json
                    returnText = JSON.parse(returnText);
                    //check whether the validation has passed or failed
                    if (returnText.validated == 'false') {
                        $('div#toolbar').after('<div class="error">' + returnText.errorMessage + '</div>');
                    } else {
                        //see if there is a success message to be shown
                        if (returnText.hasOwnProperty('successMessage')) {
                            //show the success message on-screen
                            $('div#toolbar').after('<div class="success">' + returnText.successMessage + '</div>');
                        } else {
                            //redirect to the correct page
                            window.location.replace(returnText.redirect);
                        }
                    }
                },
                // an error function for when the ajax call fails
                // @param jqXHR jqXHR jQuery XML Http Request object
                // @param string textStatus A string describing the error ("timeout" | "error" | "abort" | "parsererror")
                callbackError        : function (jqXHR, textStatus, errorThrown) {
                    console.log('jqXHR', jqXHR);
                    console.log('textStatus', textStatus);
                    console.log('errorThrown', errorThrown);
                }
            };
            // check if any elRTE editor inputs need to be added
            if (Entity.extensions.extensions.htmlElement.length > 0) {
                // set the value for this element
                Entity.extensions.extensions.htmlElement.val(Entity.extensions.extensions.htmlElement.elrte('val'));
                // re-get the values of all the input fields with this one now added
                options.fields = form.serialize();
            }
            // apply the parameters sent to the current list of options
            $.extend(true, options, parameters);

            //change the loading state of the button
            Entity.form.changeLoadingState(button);
            // do the ajax call
            $.ajax({
                url     : options.action,
                type    : options.method,
                data    : options.fields + options.additionalParameters,
                success : function (returnText) {
                    //run the success callback function
                    options.callbackSuccess(returnText);
                    //change the loading state of the button
                    Entity.form.changeLoadingState(button);
                },
                error   : function (jqXHR, textStatus, errorThrown) {
                    options.callbackError(jqXHR, textStatus, errorThrown);
                    //change the loading state of the button
                    Entity.form.changeLoadingState(button);
                }
            });
            // stop the form from POSTing
            return false;
        },

        // When a form gets submitted to create a new Entity
        // @param event e The event that triggered the create call
        create               : function (e) {
            // parameters to be sent to the postForm method
            var parameters = {
                // a callback function for when the form submission is done and successful
                // @param string returnText The string returned by the ajax call
                callbackSuccess : function (returnText) {
                    console.log(returnText);
                    // parse the return text as json
                    returnText = JSON.parse(returnText);
                    //check whether the validation has passed or failed
                    if (returnText.validated == 'false') {
                        //show the validation errors on the screen
                        Entity.form.showValidationErrors(returnText);
                    } else {
                        // redirect to the correct page
                        window.location.replace(returnText.redirect);
                    }
                }
            };
            // submit the form
            Entity.form.postForm(e, parameters);
            // stop the form from POSTing
            return false;
        },

        // When a form in a modal gets submitted to create a new Entity
        // @param event e The event that triggered the createInline call
        createInline         : function (e) {
            // parameters to be sent to the postForm method
            var parameters = {
                // a callback function for when the form submission is done and successful
                // @param string returnText The string returned by the ajax call
                callbackSuccess : function (returnText) {
                    console.log(returnText);
                    // parse the return text as json
                    returnText = JSON.parse(returnText);
                    //check whether the validation has passed or failed
                    if (returnText.validated == 'false') {
                        //show the validation errors on the screen
                        Entity.form.showValidationErrors(returnText);
                    } else {
                        // add this newly created item to the parent page
                        Entity.form.addParentItem({
                            // this form's module
                            module : $('#module').val(),
                            // the new Entity's url
                            url    : returnText.url
                        });
                    }
                }
            };
            // submit the form
            Entity.form.postForm(e, parameters);
            // stop the form from POSTing
            return false;
        },

        // When a form in a modal gets submitted to select Entities
        // @param event e The click event that triggered the selectInline call
        selectInline         : function (e) {
            // the rows in the table (this comes from a dataTables object)
            var rows = Entity.extensions.extensions.dataTable.fnGetNodes(),
            // the rows that are currently selected
            selectedRows = [],
            // the urls of the rows that are selected (comma-delimited)
            selectedUrls,
            // a list of options to send along when adding these items to the parent page
            parameters = {
                module : $('#module').val()
            };
            // see if any rows were found
            if (rows.length > 0) {
                // go through each of the rows
                $(rows).each(function () {
                    // the checkbox in this row
                    var checkbox,
                    // the row itself
                    row = $(this);
                    // check if this row was/is selected
                    if (row.hasClass('selected')) {
                        // get the checkbox
                        checkbox = row.children('td').first().children('input').first();
                        // add the checkbox's ID to the array of selected items
                        selectedRows.push(checkbox.attr('id'));
                    }
                });
            }
            // see if the array of selected rows contains anything
            if (selectedRows.length > 0) {
                // join the urls of the selected rows together
                selectedUrls = selectedRows.join(',');
                // add this combined url to the parameters
                parameters.url = selectedUrls;
                // add the selected items to the parent page
                Entity.form.addParentItem(parameters);
            }
            // stop the form from POSTing
            return false;
        },

        // Shows validation errors on the screen
        // @param object jsonData The data returned by the create or update method
        // @param boolean showErrorText Whether the error should be shown as text
        showValidationErrors : function (jsonData, showErrorText) {
            showErrorText = showErrorText || true;
            // remove all validation errors
            Entity.form.removeValidationErrors();
            //go through all the form groups
            $.each(jsonData.groups, function(groupKey, value){
                //check if this group is valid
                if (value.validated === 'false') {
                    //add the error class to the group becaue it contains errors
                    $('#form_group_button_' + groupKey).addClass('validation_error');
                    //go through all the rows in this group
                    $.each(jsonData.groups[groupKey].rows, function(rowKey, value){
                        //check if the row is valid
                        if ((typeof(value) !== 'undefined' && value !== null) && value.validated === 'false') {
                            //add an error class to the faulty input element
                            $('#' + rowKey).addClass('validation_error');
                            //if the field is hidden, the error should not be shown
                            if (!$('#form_row_' + rowKey + '_field').hasClass('hidden') && showErrorText === true) {
                                //add the error message below the field
                                $('#form_row_' + rowKey + '_field').after('<tr class="form_row_error" id="form_row_' + rowKey + '_error"><td>' + value.errorMessage + '</td></tr>');
                            }
                        }
                    });
                }
            });
        },

        // removes validation errors from the screen
        removeValidationErrors : function () {
            // all elements where the class needs to be removed
            var removeClass = $('.validation_error'),
            // all elements that need to be removed
            removeElement = $('.form_row_error');
            // remove the class
            removeClass.removeClass('validation_error');
            // remove the elements
            removeElement.remove();
        },

        // adds an item to the parent frame
        // @param object parameters A list of parameters necessary
        addParentItem        : function (parameters) {
            // a list of options required
            var options = {
                // the module where this entity must be fetched
                module : 'user',
                // the url of the Entity to fetch
                url    : 'visitor'
            };
            // add the parameters to the options array
            $.extend(true, options, parameters);
            //insert the link into the parent page
            //set the url to go fetch
            var getUrl = '/!/entity/' +  options.module + '/view/!table_row/' + options.url;
            //get the entity's view
            $.ajax({
                url: getUrl,
                method: 'post',
                success: function(data) {
                    //load the Entity onto the page below the iFrame
                    parent.$('#form_row_' + options.module + '_field td div table tbody').append(data);
                    //check if the first row is an empty placeholder
                    if (parent.$('#form_row_' + options.module + '_field td div table tbody').find('tr').first().find('td').first().hasClass('dataTables_empty')) {
                        //remove the first row
                        parent.$('#form_row_' + options.module + '_field td div table tbody').find('tr').first().remove();
                    }
                    //re-paint the table
                    parent.$('#form_row_' + options.module + '_field td div table tbody').find('tr:odd').addClass('even');
                    parent.$('#form_row_' + options.module + '_field td div table tbody').find('tr:even').addClass('odd');
                    //remove the loading class from the button and re enable it
//                        inputButton.removeClass('loading').removeAttr('disabled');
                    //close the colorbox
                    parent.$.fn.colorbox.close();
                }
            });

        },

        // removes a linked entity from an entity
        // @param event e The event that triggered the removal of this item
        removeLinkedItem     : function (e) {
            // the actual link/button that was clicked to remove this item
            var button = $(e.target);
            $(button).closest('tr').fadeOut(300, function() {
                $(this).remove();
            });
        },

        // swaps to a different form group
        // @param event e The event that triggered this form group swap
        swapFormGroup        : function (e) {
            // the Id of the button that was pressed to swap this form group
            var buttonId = e ? $(e.target).attr('id') : $('.form_group_button:first').attr('id');
            // check if a formGroupNumber was passed along
            if (!buttonId) {
                // see if there are any form groups at all
                if ($('.form_group').length > 0) {
                    // get the ID of the button of the first form group
                    buttonId = $('.form_group_button:first').attr('id');
                }
            }
            // check if a button ID has been sent along or been set in the statement above
            if (buttonId) {
                //hide all the form groups and remove their 'selected' classes
                $('.form_group').hide().removeClass('selected');
                //get the id of the Form Group that needs to be shown
                var formGroupId = '#' + buttonId.replace('form_group_button', 'form_group');
                //un-hide the selected Form Group and add the selected class
                $(formGroupId).toggle(500).addClass('selected');
                //remove the 'selected' class from all the Form Group buttons
                $('.form_group_button').removeClass('selected');
                //add the 'selected' class to the current Form Group button
                $('#' + buttonId).addClass('selected');
            }
        },

        // selects a checkbox in a table row
        // @param event e The click event that triggered the selection of this row
        selectTableRow       : function (e) {
            // the element that was clicked on
            var element = $(e.target),
            // the closest form row to this element
            row = element.closest('tr'),
            // all the table rows
            allRows,
            // the selected rows count
            selectedRows = 0,
            // the checkbox inside the row
            checkbox = row.children('td').first().children('input').first(),
            // the table this row is in
            table = row.parents('table.data_table'),
            // the multiple-selection toolbar
            multiSelectToolbar = row.parents('div.dataTables_wrapper').prev('#edit_multiple'),
            // the actions that can be performed on multiple items
            actions = multiSelectToolbar.find('li'),
            // the atext hat shows the full amount
            amountText = multiSelectToolbar.prev('p#edit_multiple_text');
            // the amount indicator
            amountIndicator = amountText.children('span').first();
            // check whether the row has a selected class
            if (row.hasClass('selected')) {
                // de-select this row
                row.removeClass('selected');
                // un-check the checkbox
                checkbox.prop('checked', false);
            } else {
                // add the selected class to this row
                row.addClass('selected');
                // check the checkbox
                checkbox.prop('checked', true);
            }
            // see if there are any listed actions to perform
            if (actions.length === 0) {
                // hide the multiple toolbar and text
                multiSelectToolbar.addClass('hidden');
                amountText.addClass('hidden');
            } else {
                // see if there are any highlighted rows
                if (table.find('tr.selected').length > 0) {
                    // show the multiple toolbar and text
                    multiSelectToolbar.removeClass('hidden');
                    amountText.removeClass('hidden');
                } else {
                    // hide the multiple toolbar and text
                    multiSelectToolbar.addClass('hidden');
                    amountText.addClass('hidden');
                }
                // get all the rows in this table
                allRows = Entity.extensions.extensions.dataTable.fnGetNodes();
                // go through all the rows
                $(allRows).each(function () {
                    // check if this row is selected
                    if ($(this).hasClass('selected')) {
                        // add this row to the list of selected rows
                        selectedRows++;
                    }
                });
                // update this amount on the indicator
                amountIndicator.html(selectedRows);
            }
        },

        //adds and removed loading states from the form's button
        //@param HTMLElement button The form's submit button
        changeLoadingState   : function (button) {
            //check if the button has a loading class
            if (button.hasClass('loading')) {
                //remove the loading class from the button
                button.removeClass('loading');
                //enable the button
                button.removeAttr('disabled');
            } else {
                //add the loading class to the button
                button.addClass('loading');
                //disable the button
                button.attr('disabled', 'disabled');
            }
        },

        //applies multiple items to a url
        //@param event e The click event that fired the multiple action
        actionMultiple       : function (e) {
            //the button that was clicked
            var button = $(e.target),
            //the url the button redirects to
            url = button.attr('href'),
            //the currently selected table rows
            selectedRows = button.parents('div#edit_multiple').prev('div.dataTables_wrapper').find('tr.selected').find('input:checked'),
            //a list of entuty urls to be actioned
            urls = [];
            //see if any selected rows were found
            if (selectedRows.length > 0) {
                //go through each selected row
                selectedRows.each(function () {
                    //add this checkbox's id attribute to the urls array
                    urls.push($(this).attr('id'));
                });
            }
            //redirect to the new url
            document.location.href = url.replace('multiple', urls.join(','));
            return false;
        }

    },

    // extensions that need to be bound/added
    // I might need to add modernizr checks to each extension to do before activating it
    extensions : {

        // a list of loaded extensions
        extensions : {},

        // set a list of options
        options : {
            // all links that need to open a colorbox window
            colorbox        : {
                identifier : '.colorbox',
                extension  : 'colorbox',
                options    : ''
            },
            // all the links that need to open in a colorbox window AND an iFrame
            colorboxFrame   : {
                identifier : '.colorbox_iframe',
                extension  : 'colorbox',
                options    : {
                    iframe: 'true',
                    width:'90%',
                    height:'90%'
                }
            },
            // all html form elements (textareas with rich-text editing capabilities)
            htmlElement     : {
                identifier : '.html',
                extension  : 'elrte',
                options    : {
                    allowSource : true,
                    lang        : 'en',
                    fmAllow     : false,
                    toolbar     : 'tiny'
                }
            },
            // all dataTables elements
            dataTable       : {
                identifier : '.data_table',
                extension  : 'dataTable',
                options    : ''
            },
            // simple data tables element
            dataTableSimple : {
                identifier : '.data_table_simple',
                extension  : 'dataTable',
                options    : {
                    "bPaginate"     : false,
                    "bLengthChange" : false,
                    "bFilter"       : false,
                    "bSort"         : false,
                    "bInfo"         : false,
                    "bAutoWidth"    : false
                }
            }

        },

        // activate all the extensions
        load    : function () {
            // go through each extension
            $.each(Entity.extensions.options, function (index, extension) {
                // bind the extension to the element
                Entity.extensions.extensions[index] = $(extension.identifier)[extension.extension](extension.options);
                console.log('Added extension ' + extension.extension + ' to element ' + extension.identifier + ' (Entity.extensions.extensions.' + index + ')');
            });
        }

    }
};

// events that need to be bound
Events     = {

    // Set a list of options
    options : {
        // Any general form submission
        generalFormButton  : {
            identifier : '.ajax_general',
            event      : 'click',
            action     : Entity.form.postForm
        },
        // The button used to create Entities
        createButton       : {
            identifier : '.ajax',
            event      : 'click',
            action     : Entity.form.create
        },
        // The button used to create Entities from a modal window
        createInlineButton : {
            identifier : '.ajax_link_create',
            event      : 'click',
            action     : Entity.form.createInline
        },
        // The button used to link entities to an Entity (select)
        selectInlineButton : {
            identifier : '.ajax_link',
            event      : 'click',
            action     : Entity.form.selectInline
        },
        // swap between form groups
        swapFormGroup      : {
            identifier : '.form_group_button',
            event      : 'click',
            action     : Entity.form.swapFormGroup
        },
        // remove a linked entity from another entity
        removeLinkedEntity : {
            identifier : '.remove_row_link',
            event      : 'click',
            action     : Entity.form.removeLinkedItem
        },
        // select a table-row
        selectTableRow     : {
            identifier : 'table.data_table tr',
            event      : 'click',
            action     : Entity.form.selectTableRow
        },
        //ation multiple Entities
        actionMultiple     : {
            identifier : '.ajax_forward_multiple',
            event      : 'click',
            action     : Entity.form.actionMultiple
        }
    },

    // Binding events to the correct functions
    bind    : function () {
        // go through each event
        $.each(Events.options, function (index, event) {
            // if the event is "document ready" the action can be run right away as it is already ready
            if (event.identifier === 'document' && event.event === 'ready') {
                // a variable to hold the action
                var action = event.action;
                action();
            // anything else, bind the event
            } else {
                // attach the event
                $(document).on(event.event, event.identifier, event.action);
            }
            console.log('Bound the ' + event.event + ' event of the element ' + event.identifier);
        });
    },

    unbind  : function () {
        // go through each event
        $.each(Events.options, function (index, event) {
            // remove the event
            $(event.identifier).off(event.event, event.action);
        });
    }

};