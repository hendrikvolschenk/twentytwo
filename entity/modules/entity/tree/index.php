<div id="toolbar">
  <?php if (isset($entity->form->children['form_action']) && is_array($entity->form->children['form_action'])): ?>
        <ul>
            <?php foreach ($entity->form->children['form_action'] as $formActionUrl): ?>
                <?php
                    //create a new Form Action object
                    $formAction = EntityFactory::build('form_action');
                    //load the Form Action
                    $formAction->load($formActionUrl, $database);
                ?>
                <?php if ($formAction->displayEntity == '1'): ?>
                    <?php if ((empty($formAction->disallowedStatusses) || !strstr($formAction->disallowedStatusses, $entity->status)) && (empty($formAction->allowedStatusses) || strstr($formAction->allowedStatusses, $entity->status))): ?>
                        <li<?php echo ($system->action == strtolower(str_replace(' ', '_', $formAction->action))) ? ' class="selected"' : ''; ?>>
                            <a href="<?php echo System::buildUrl($system->templateView, $system->admin, $system->module, strtolower(str_replace(' ', '_', $formAction->action)), $system->view, $system->url); ?>" class="<?php echo strtolower(str_replace(' ', '_', $formAction->action)); ?>" title="<?php echo $formAction->action; ?>">
                                <?php echo (isset($formAction->entityTitle) && !empty($formAction->entityTitle)) ? $formAction->entityTitle: $formAction->action; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <h1><?php echo $entity->{$entity->form->urlColumn}; ?></h1>
</div>
<?php echo $entity->getLinkedEntitiesList($entity, $database, $system, false); ?>
<br /><br />