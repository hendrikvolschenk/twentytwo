<?php $allForms = EntityFactory::buildList($database, 'form'); ?>
<?php if ($allForms !== false): ?>
    <ul>
        <?php foreach ($allForms as $formUrl): ?>
            <?php
                $form = EntityFactory::build('form');
                $form->load($formUrl, $database);
                $formEntity = EntityFactory::build($form->formModule);
                $formEntity->load(' ', $database);
            ?>
            <?php if ($form->display == '1' && $system->user->hasFormAccess($form->id, $database, true)): ?>
                <?php if (isset($form->children['form_action']) && is_array($form->children['form_action'])): ?>
                    <li class="main_menu form<?php echo ($system->module == $form->formModule) ? ' selected' : ''; ?>" id="main_menu_form_<?php echo $form->url; ?>">
                        <?php echo $formEntity->form->name; ?>
                        <ul>
                        <?php foreach ($form->children['form_action'] as $formActionUrl): ?>
                            <?php
                                //get the Form Action as an object
                                $formAction = EntityFactory::build('form_action');
                                //load the Form Action
                                $formAction->load($formActionUrl, $database);
                            ?>
                            <?php if ($formAction->displayMainMenu == '1'): ?>
                                <li class="main_menu_form_action<?php echo ($system->module == $form->formModule && $system->action == strtolower(str_replace(' ', '_', $formAction->action))) ? ' selected' : ''; ?>" id="main_menu_form_action_<?php echo $form->url; ?>_<?php echo $formAction->url; ?>">
                                    <a href="<?php echo System::buildUrl(null, true, $form->formModule, strtolower(str_replace(' ', '_', $formAction->action)), null, null); ?>">
                                        <?php echo (isset($formAction->mainMenuTitle) && !empty($formAction->mainMenuTitle)) ? $formAction->mainMenuTitle : $formAction->action; ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>