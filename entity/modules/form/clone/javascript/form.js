// Extend the Entity object with the newly required functionality
$.extend(true, Entity, {
    // The form object holds all the functions necessary to post/submit a form
    form : {
        // The getModuleName function creates the 'form_module' name from the #name field
        // @param event e The event that was triggered to fetch this module name
        getModuleName : function (e) {
            //convert the string to lowercase
            //replace spaces with underscores
            //replace special characters with nothing
            var formModule = $('#name').val().toLowerCase().replace(/\W/g, '_');
            //set the hidden input's value to the new value
            $('#form_module').val(formModule);
        }
    }
});

// Extend the Events object to add the necessary event hooks
$.extend(true, Events, {
    // Set a list of options
    options : {
        // when a form's Name gets changed
        changeFormName : {
            identifier : '#name',
            event      : 'blur',
            action     : Entity.form.getModuleName
        }
    }
});