<?php
    //see if any form groups were selected/created
    //don't throw an error here if there is none, the included page will validate for this
    if (isset($_POST['form_group']) && is_array($_POST['form_group'])) {
        //create a new Form Group object
        $formGroup = EntityFactory::build('form_group');
        //load the first Form Group
        $formGroup->load($_POST['form_group'][0], $database);
        //create a new Form Row object
        $formRow = EntityFactory::build('form_row');
        //load the first Form Row
        $formRow->load($formGroup->children['form_row'][0], $database);
        //set the POST url_column equal to the url of the first row
        $_POST['url_column'] = $formRow->field;
    }
    //include the default "create" page located at /entity/modules/entity/create/post/index.php
    require_once(dirname(__FILE__) . '/../../../entity/create/post/index.php');
?>