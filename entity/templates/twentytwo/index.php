<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css' />
    <?php echo $styles . $javascript; ?>
    <script>
      $(document).ready(function(){
        Entity.initialize();
        console.log(Entity);
      });
    </script>
  </head>
  <body>
    <div id="page_header">
      <h1><span class="first_word">twenty</span>two</h1>
      <?php echo $template->loadModule($system, $entity, $database, 'user', 'view', '!badge', $system->user->url, 'own'); ?>
    </div>
    <div id="left_menu">
      <?php echo $template->loadModule($system, $entity, $database, 'form', 'view', '!menu', 'all', 'this', true); ?>
    </div>
    <div id="page_content"><?php echo $content; ?></div>
    <div class="clear"></div>
    <div id="footer" class="padding_large">&copy; 2013 - <?php echo date('Y'); ?> - Hendrik Volschenk</div>
  </body>
</html>