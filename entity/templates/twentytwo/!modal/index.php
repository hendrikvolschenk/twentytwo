<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css' />
    <?php echo $styles . $javascript; ?>
    <script>
      $(document).ready(function(){
        Entity.initialize();
        console.log(Entity);
      });
    </script>
  </head>
  <body>
    <div id="page_content" class="padding_xlarge"><?php echo $content; ?></div>
  </body>
</html>