<?php
/**
* Validates a Form.
* Default validation occurs first based on the MySql column type
* Extra validation can also occur after
* 
* array(
*     validated=>true|false,
*     //if true
*     redirect=>string,
*     //if false
*     group1=>array(
*         validated=>true|false
*         rows=>array(
*             row1=>array(
*                 validated=>true|false
*                 //if false
*                 errorMessage=>string
*             ),
*             ...
*         )
*     ),
*     ...
* )
* 
* */
class FormValidation
{
    /**
    * Holds the input/POST data
    * 
    * @var array
    * */
    public $input = null;
    
    /**
    * Validates the entire form by calling the correct methods
    * 
    * @param Form $form The form that must be validated
    * @param array $input The POST array to be validated
    * @param Connection $database The database connection (to be passed to validateGroup())
    * 
    * @return array Result of the validation
    * */
    public function validate(Form $form, $input, Connection $database)
    {
        //set the class' input variable
        $this->input = $input;
        //check that the form has groups linked to it
        if (isset($form->children['form_group']) && is_array($form->children['form_group'])) {
            //set up an empty return variable
            $return = array('validated'=>'true');
            //go through all the groups linked to this form
            foreach ($form->children['form_group'] as $formGroupUrl) {
                //validate the form group
                $return['groups'][$formGroupUrl] = $this->validateGroup($formGroupUrl, $database);
                //check if this row was valid/invalid
                $return['validated'] = ($return['groups'][$formGroupUrl]['validated'] == 'false') ? 'false' : $return['validated'];
            }
            
            
            //check if any includes were created for this form
            if (isset($form->children['form_link']) && is_array($form->children['form_link'])) {
                //validate the Form Links
                $return['groups']['links'] = $this->validateFormLinks($form->children['form_link'], $database);
                //check if this row was valid/invalid
                $return['validated'] = ($return['groups']['links']['validated'] == 'false') ? 'false' : $return['validated'];
            }
            
            //check if the return is positive
            if ($return['validated'] == 'true') {
                //remove "groups" as all is valid
                unset($return['groups']);
            }
            
            //return the validation results
            return $return;
        } else {
            //throw an error, this form has no groups
            throw new Exception("The form for {$form->module} has no form groups linked to it");
        }
    }
    
    /**
    * Validates a group in the form
    * 
    * @param string $url The url of the FormRow
    * @param Connection $database The database connection
    * 
    * @return array Validation results
    * */
    private function validateGroup($url, Connection $database)
    {
        //get the Form Group as an object
        $formGroup = EntityFactory::build('form_group');
        //load the Form Group
        $formGroup->load($url, $database);
        //check that this group has rows linked to it
        if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])) {
            //set the default validated variable to true
            $return = array('validated'=>'true');
            //go through all the rows linked to this group
            foreach ($formGroup->children['form_row'] as $formRowUrl) {
                //create a new Form Row object
                $formRow = EntityFactory::build('form_row');
                //load the current row
                $formRow->load($formRowUrl, $database);
                //validate the FormRow
                $return['rows'][$formRow->field] = $this->validateRow($formRowUrl, $database);
                //check if this row was valid/invalid
                $return['validated'] = ($return['rows'][$formRow->field]['validated'] == 'false') ? 'false' : $return['validated'];
            }
            return $return;
        } else {
            //throw an error, this form group has no rows
            throw new Exception("The form group for $module > {$formGroup->title} has no rows");
        }
    }
    
    /**
    * Validates a row in the form
    * 
    * @param string $url The url of the FormRow to be validated
    * @param Connection $database The database connection
    * 
    * @return array Validation results
    * */
    private function validateRow($url, Connection $database)
    {
        //get the Form Row as an object
        $formRow = EntityFactory::build('form_row');
        //load the Form Group
        $formRow->load($url, $database);
        //get the input for this row
        $input = (isset($this->input[$formRow->field])) ? $this->input[$formRow->field] : null;
        //prepare the input
        $this->input[$formRow->field] = $input = $this->prepare($input, $formRow);
        //first check whether this field is nullable
        $result = $this->validateNull($formRow->null, $input);
        //see if validation should continue for this row
        if ($result['validated'] == 'true' && !empty($input)) {
            //check what other validation this field must go through
            $result = $this->validateByType($input, $formRow);
            //see if validation shoul continue for this row
            if ($result['validated'] == 'true') {
                //HENDRIK: Call special validation
            }
        }
        //return the result
        return $result;
    }
    
    /**
    * Validates the Form Links that the form has
    * 
    * @param array $links A list of Form Link urls to validate
    * @param Connection $database The database connection
    * 
    * @return array Validation results
    * */
    private function validateFormLinks($links, Connection $database)
    {
        //add an error to this
        $return = array('validated'=>'true');
        //go through each link
        foreach ($links as $link) {
            //get the form link as an object
            $formLink = EntityFactory::build('form_link');
            //load the form link
            $formLink->load($link, $database);
            //validate the specific link
            $return['rows'][$formLink->include] = $this->validateLink($link, $database);
            //check if the link was valid/invalid
            $return['validated'] = ($return['rows'][$formLink->include]['validated'] == 'false') ? 'false' : $return['validated'];
        }
        //return the validation results
        return $return;
    }
    
    /**
    * Validates a single Form Link
    * 
    * @param string $link The Form Link's url
    * @param Connection $database The database connection
    * 
    * @return array Validation results
    * */
    private function validateLink($link, Connection $database)
    {
        //get the Form Link as an object
        $formLink = EntityFactory::build('form_link');
        //load the Form Link
        $formLink->load($link, $database);
        //check if any includes were added for this type
        if (isset($this->input[$formLink->include]) && is_array($this->input[$formLink->include])) {
            //check if more than one was included and if that is allowed
            if (count($this->input[$formLink->include]) > 1 && $formLink->multiple == 0) {
                //there was an error, this include may only contain one entry
                return $this->buildValidationArray('You may only select one link');
            }
        } else {
            //there are none, see if this include was required/mandatory
            if ($formLink->required == 1) {
                //there was an error, this include needed to have at least one entry
                return $this->buildValidationArray('This link is required, please select/create one');
            }
        }
        //default success return
        return $this->buildValidationArray();
    }
    
    /**
    * Calls the correct preparation function to prepare the variable accordingly
    * 
    * @param mixed $input The value that was input into the field
    * @param FormRow $formRow The Rorm Row being validated
    * 
    * @return mixed The prepared variable
    * */
    private function prepare($input, FormRow $formRow)
    {
        //make the first letter of $type uppercase, the rest lowercase
        $type = ucfirst(strtolower($formRow->type));
        //get the method name as a string
        $method = "prepare$type";
        //check if the correct preparation method exists
        if (method_exists($this, $method) && $method != 'prepare') {
            //call the relevant preparation function
            return $this->$method($input, $formRow);
        }
        //return the input value as a default
        return $input;
    }
    
    /**
    * Prepares a Checkbox/TINYINT(1) row
    * If the value is null or an empty string, the value should be set to 0
    * 
    * @param integer $integer The integer value to check and prepare
    * @param FormRow $formRow The Rorm Row being validated
    * 
    * @return string
    * */
    private function prepareCheckbox($integer, FormRow $formRow)
    {
        //check if the maximum length of this field is 1
        if ($formRow->lengthMax == 1) {
            //check if the integer is empty
            if ($integer == '' || $integer == null) {
                //return a "0" so the validation does not fail
                return '0';
            }
        }
        //by default, return the integer
        return $integer;
    }
    
    /**
    * Prepares a VARCHAR/Multiple row
    * 
    * @param array $array An array sent via POST
    * @param FormRow $formRow The Rorm Row being validated
    * */
    private function prepareMultiple($array, FormRow $formRow)
    {
        //see if there are any values in $array
        if (is_array($array)) {
            //return an imploded version of the array
            return implode(',', $array);
        } else {
            //return null
            return null;
        }
    }
    
    /**
    * Builds a validation array so every function doesn't have to
    * 
    * @param string $errorMessage The error message that has occurred, if empty, no error has occurred
    * 
    * @return array
    * */
    private function buildValidationArray($errorMessage = null)
    {
        //see if the validation has passed
        if ($errorMessage == null) {
            //return a successful validation array
            return array('validated' => 'true');
        } else {
            //return an unsuccessful validation array
            return array(
                'validated' => 'false',
                'errorMessage' => $errorMessage
            );
        }
    }
    
    /**
    * Validates whether the input is empty, if so, whether the field is allowed to be empty
    * 
    * @param integer $null Whether the field is allowed to be null
    * @param mixed $input The input to validate
    * 
    * @return array Validation results
    * */
    private function validateNull($null, $input)
    {
        //check if the input is empty and if that is allowed
        if ($null == 0 && ($input == null || $input == '')) {
            //an error has occurred
            return $this->buildValidationArray('This field must not be empty');
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Class the validation function based on which type of field this is
    * 
    * @param mixed $input The input being validated
    * @param FormRow $formRow The Rorm Row being validated
    * 
    * @return array Validation results
    * */
    private function validateByType($input, FormRow $formRow)
    {
        //see which function should be called
        switch ($formRow->typeParsed) {
            //see whether a string validation function should be called
            case 'CHAR':
            case 'VARCHAR':
            case 'TEXT':
                //call the string validation function
                return $this->validateString($input, $formRow->lengthMin, $formRow->lengthMax);
            //see whether a tinyint is being used as a checkbox or integer
            case 'TINYINT':
                //see what the maximum allowed length of the number is
                if ($formRow->lengthMax == 1) {
                    //this is a checkbox, validate accordingly
                    return $this->validateList($input, array(0,1));
                }
            //see whether an integer validation function should be called
            case 'TINYINT':
            case 'SMALLINT':
            case 'MEDIUMINT':
            case 'INT':
            case 'BIGINT':
                //call the integer validation function
                return $this->validateInteger($input, $formRow->lengthMax, $formRow->valueMin, $formRow->valueMax);
            //see whether a decimal validation function should be called
            case 'DECIMAL':
            case 'FLOAT':
            case 'DOUBLE':
            case 'REAL':
                //call the decimal validation function
                return $this->validateDecimal($input, $formRow->lengthMax, $formRow->lengthMin, $formRow->valueMin, $formRow->valueMax);
            //see whether a date validation function must be called
            case 'DATE':
                //call the date validation function
                return $this->validateDate($input, $formRow->valueMin, $formRow->valueMax, false);
            //see whether a datetime validation function must be called
            case 'DATETIME':
                //call the datetime validation function
                return $this->validateDate($input, $formRow->valueMin, $formRow->valueMax, true);
            //see whether a list validation function must be called
            case 'SET':
            case 'ENUM':
                //call the list validation function
                return $this->validateList($input, $formRow->allowedValues);
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Validates a string
    * 
    * @param string $string The string to be validated
    * @param integer $minLength The minimum length ,in charachters, that the string is allowed to be
    * @param integer $maxLength The maximum length ,in charachters, that the string is allowed to be
    * 
    * @return array Validation results
    * */
    private function validateString($string, $minLength, $maxLength)
    {
        //see if this string is too long or toot short
        if (strlen($string) < $minLength || strlen($string) > $maxLength) {
            //return an error array
            return $this->buildValidationArray("This field must be between $minLength and $maxLength characters");
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Validates an integer
    * 
    * @param integer $integer The integer that needs to be validated
    * @param integer $maxLength The maximum length of the integer
    * @param integer $min The minimum amount the integer is allowed to be
    * @param integer $max The maximum amount the integer is allowed to be
    * 
    * @return array Validation results
    * */
    private function validateInteger($integer, $maxLength, $min, $max)
    {
        //check if the field falls within the minumum and maximum
        if ($integer < $min || $integer > $max) {
            //return an error array
            return $this->buildValidationArray("This field's value must be between $min and $max");
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Validates a decimal amount
    * 
    * @param decimal $decimal The decimal amount that needs to be validated
    * @param integer $numberLength The length of the number part of the decimal, ex: in 123.45, $numberLength = strlen(123) = 3
    * @param integer $decimalLength The length of the allowed decimal part of the decimal, ex: in 123.45, $decimalLength = strlen(45) = 2
    * @param decimal $min The minimum amount the decimal is allowed to be
    * @param decimal $max The maximum amount the decimal is allowed to be
    * 
    * @return array Validation results
    * */
    private function validateDecimal($decimal, $numberLength, $decimalLength, $min, $max)
    {
        //break the deimal into two parts, before and after the dot
        $parts = explode('.', $decimal);
        //check that the parts are the correct length
        if ((isset($parts[0]) && strlen($parts[0]) > $numberLength) || (isset($parts[1]) && strlen($parts[1]) > $decimalLength)) {
            //return an error array
            return $this->buildValidationArray("This field may contain only $numberLength characters before the dot and $decimalLength characters after the dot");
        }
        //check that the decimal value falls within the min and max boundaries
        if ($decimal < $min || $decimal > $max) {
            //return an error array
            return $this->buildValidationArray("This field must be larger or equal to $min and smaller or equal to $max");
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Validates a date or date-and-time
    * 
    * @param date|datetime $date The date to be validated
    * @param date|datetime $min The minumum date (past) that $date is allowed to be
    * @param date|datetime $max The maximum date (future) that $date is allowed to be
    * @param boolean $time Whether $date has a time and time should be validated
    * 
    * @return array Validation results
    * */
    private function validateDate($date, $min, $max, $time)
    {
        //split the date into two parts, date and time
        $dateTime = explode(' ', $date);
        //split the date into three parts, year, month and day
        $yearMonthDay = explode('-', $dateTime[0]);
        //see whether the date consists of three parts
        if (count($yearMonthDay) == 3) {
            //see whether the date is valid
            if (checkdate($yearMonthDay[1], $yearMonthDay[2], $yearMonthDay[0]) == false) {
                //return an error array
                return $this->buildValidationArray("This field is not a valid date");
            }
        } else {
            //return an error array
            return $this->buildValidationArray("This field is not a valid date");
        }
        //see whether the time exists and must be validated
        if ($time == true) {
            //check whether a time is set
            if (isset($dateTime[1])) {
                //split the time into two parts, hours and minutes
                $hoursMinutes = explode(':', $dateTime[1]);
                //see that the hours are set correctly
                if ($hoursMinutes[0] >= 0 && $hoursMinutes[0] < 24) {
                    //see if the minutes are set correctly
                    if ($hoursMinutes[1] < 0 || $hoursMinutes[1] > 59) {
                        //return an error array
                        return $this->buildValidationArray("Valid hours are between 0 and 24");
                    }
                } else {
                    //return an error array
                    return $this->buildValidationArray("Valid hours are between 0 and 24");
                }
            }
        }
        //see whether the date falls within the bounds set by $min and $max
        if ($date < $min || $date > $max && !empty($max) && !empty($min)) {
            //return an error array
            return $this->buildValidationArray("This field's value must be between $min and $max");
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
    /**
    * Validates a value to a list of values for array or set types
    * 
    * @param string $value The value that needs to be validated
    * @param array $list The list that $value needs to be validated against
    * 
    * @return array Validation results
    * */
    private function validateList($value, $list)
    {
        //see whther the value exists within the array
        if (!in_array($value, $list)) {
            //return an error array
            return $this->buildValidationArray("This field's value does not fall within the sepecified values");
        }
        //return a default successful array
        return $this->buildValidationArray();
    }
    
}
?>