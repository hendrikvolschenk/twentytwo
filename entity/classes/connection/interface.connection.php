<?php
/**
* The Connection interface provides an interface for any database activity
* */
interface Connection
{
    
    /**
    * A method for connecting to the connection
    * 
    * @param string $host The database host being connected to
    * @param string $username The username for the database
    * @param string $password The password for the username
    * @param string $database The database being connected to
    * */
    public function connect($host, $username, $password, $database);
    
    /**
    * A method for disconnecting from the connection
    * */
    public function disconnect();
    
    /**
    * A method for running a query
    * 
    * @param string $query The MySql query to be executed
    * */
    public function runQuery($query);
    
    /**
    * A method for fetching the reult from a query ran by runQuery()
    * 
    * @param resource $query The query from which the result is needed
    * */
    public function fetchResult($query);
    
}
?>