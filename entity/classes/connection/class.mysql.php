<?php
/**
* The Database class handles basic database operations such as running queries, starting and ending transactions
* */
class MySql implements Connection
{
    
    /**
    * The connection variable holds the MySql connection string
    * 
    * @var resource identifier
    * */
    public $connection = null;
    
    
    /**
    * The databaseConnect method connects to the database if possible
    * 
    * @param string $host The database host being connected to
    * @param string $username The username for the database
    * @param string $password The password for the username
    * @param string $database The database being connected to
    * */
    public function Connect($host, $username, $password, $database)
    {
        //connect to the database and suppress any errors
        $connection = @mysql_connect($host, $username, $password);
        //see if the database connection succeeded
        if (!$connection) {
            //throw an error about the database settings not working
            throw new Exception("Unable to connect to MySql server with username '$username' on host '$host'");
            //return an empty connection
            $this->connection = null;
        } else {
            //select the database
            $database_selected = mysql_select_db($database);
            //check if the database selection worked
            if (!$database_selected) {
                //throw an error about the database selection failing
                throw new Exeption("Unable to select database '$database'");
                //return an empty connection
                $this->connection = null;
            }
            //return the connection resource identifier
            $this->connection = $connection;
        }
    }
    
    /**
    * Disconnects from the current Connection
    * */
    public function disconnect()
    {
        //disconnect from the MySql database Connection
        mysql_close($this->connection);
    }
    
    /**
    * The runQuery method runs a single mysql query and returns a result
    * 
    * @param string $query The MySql query to be executed
    * 
    * @return boolean false On failure
    * @return array The result of the query
    * */
    public function runQuery($query)
    {
        //return a query result
        $result = mysql_query($query, $this->connection) or die('MySql Error: ' . mysql_error() . $query);
        //check if a select was performed
        if (!strstr($query, 'SELECT')) {
            //return a boolean
            return (!$result) ? false : true;
        } else {
            //check if anything was found
            if ($result && (mysql_num_rows ($result) > 0)) {
                //resturn the result
                return $result;
            } else {
                //return an error
                return false;
            }
        }
    }
    
    /**
    * Fetches a database result and returns an array or a boolean false on error
    * 
    * @param $query The query that has already been executed
    * @param string $columns The columns that must be returned
    * 
    * @return array|boolean An array of items or false on failure
    * */
    public function fetchResult($query)
    {
        //see if the query failed
        if ($query != false) {
            //set an empty return array
            $returnarray = array();
            //go through the result
            while ($row = mysql_fetch_assoc ($query)) {
                //check if more than one column is being returned
                if (count($row) > 1) {
                    //add the row to the row array
                    $rowArray = $row;
                } else {
                    //add the single item to the row array
                    $rowArray = reset($row);
                }
                //add the row array to the return array
                $returnArray[] = $rowArray;
            }
            //return the array
            return $returnArray;
        }
        //return a default false
        return false;
    }
    
    /**
    * The __toString method will print the class to screen
    * */
    public function __toString()
    {
        //print the current object
        return '<pre>' . print_r($this, true) . '</pre>';
    }
    
}
?>