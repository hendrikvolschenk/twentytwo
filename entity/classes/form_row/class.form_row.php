<?php
/**
* A class to handle Form Rows
* It provides an override for Entity::load()
* It also handles the creation of HTML Form Rows
* */
class FormRow extends Entity
{

    /**
    * Stores the database field currently being loaded/built
    *
    * @var string
    * */
    public $field = null;

    /**
    * Stores the label for the field
    *
    * @var string
    * */
    public $label = null;

    /**
    * Stores the placeholder text to be shown inside the field
    *
    * @var string
    * */
    public $placeholder = null;

    /**
    * Stores the parsed database field type
    * Example: MEDIUMINT
    *
    * @var string
    * */
    public $typeParsed = null;

    /**
    * Holds the type of input to be built
    * Current (2013-02-19) html5 input types: color, date, datetime, datetime-local, email, month, number, range, search, tel, time, url, week
    * Html 4 and lower input types: checkbox, hidden, password, radio, text
    *
    * @var string
    * */
    public $type = null;

    /**
    * Whether the field is signed or not
    * This is only applicable to integer-type fields
    *
    * @var integer
    * */
    public $signed = null;

    /**
    * Whether the field is nullable
    *
    * @var integer
    * */
    public $null = null;

    /**
    * Holds the default value for the field
    *
    * @var string
    * */
    public $defaultValue = null;

    /**
    * Whether the field is disabled or not
    * If disabled the field will show, but not be editable
    *
    * @var integer
    * */
    public $disabled = null;

    /**
    * Whether the field is only allowed to be read, not altered
    *
    * @var integer
    * */
    public $readonly = null;

    /**
    * Holds the value to be inserted into the field
    *
    * @var string
    * */
    public $input = null;

    /**
    * Holds the description of the field\
    * The description is displayed below the field to give more information
    *
    * @var string
    * */
    public $description = null;

    /**
    * Holds the minimum value the field is allowed to hold
    * This is for INTEGER, DATE(YYYY-MM-DD) and DECIMAL(XX.YY) types
    *
    * @var string
    * */
    public $valueMin = null;

    /**
    * Holds the maximum value the field is allowed to hold
    * This is for INTEGER, DATE(YYYY-MM-DD) and DECIMAL(XX.YY) types
    *
    * @var string
    * */
    public $valueMax = null;

    /**
    * Holds the step amount for all type="numer" fields
    * The step defines the increment amount for the number "slider"
    *
    * @var string
    * */
    public $step;

    /**
    * Holds the minimum amount of characters the field is allowed to hold
    * This is for INTEGER, VARCHAR, CHAR, TEXT and DECIMAL types
    * For DECIMAL types it only holds the length of the part after the decimal point
    *
    * @var string
    * */
    public $lengthMin = null;

    /**
    * Holds the maximum amount of characters the field is allowed to hold
    * This is for INTEGER, VARCHAR, CHAR, TEXT and DECIMAL types
    * For DECIMAL types it only holds the length of the part before the decimal point
    *
    * @var string
    * */
    public $lengthMax = null;

    /**
    * Holds a list of allowable values for the field
    * Applicable to ENUM and TINYINT(1) fields
    *
    * @var array
    * */
    public $allowedValues = null;

    /**
    * Holds a list of functions to be run to populate $this->input before validation
    *
    * @var array
    * */
    public $functions = null;

    /**
    * Holds a list of validation functions to be run on $this->input
    * These validation functions only run once the general validation functions have run
    *
    * @var array
    * */
    public $functionsValidation = null;


    /**
    * An overide for the parent create function
    * Before doing the create a temporary Form Row object is created to get the `type_raw` and `type_parsed` values
    *
    * @param array $post A key-value pair array containing the MySql column name as the key and the value as the value
    * @param Form $form The form object containing information about the type(s)
    * @param Connection $database The connection to the database
    *
    * @return boolean Whether the create was successful or not
    * */
    public function create(Array $post, Form $form, Connection $database)
    {
        //set the type_parsed
        $post['type_parsed'] = $this->getMySqlDataType($post['type'], $post);
        //get whether the field is signed or not or whether signed should be null
        $post['signed'] = $this->getMySqlSigned($post);
        //use the parent create function to handle the rest of the create
        parent::create($post, $form, $database);
    }

    //DO ABOVE FOR UPDATE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /**
    * An override for the parent update function
    *
    * @param array $post A key-value pair array containing the MySql column name as the key and the value as the value
    * @param Entity $entity The Entity being updated
    * @param Connection $database The connection to the database
    *
    * @return boolean Whether the update was successful or not
    * */
    public function update(Array $post, Entity $entity, Connection $database)
    {
        //set the type_parsed
        $post['type_parsed'] = $this->getMySqlDataType($post['type'], $post);
        //get whether the field is signed or not or whether signed should be null
        $post['signed'] = $this->getMySqlSigned($post);
        //use the parent create function to handle the rest of the create
        parent::update($post, $entity, $database);
    }



    /**
    * After an update is done this method check whether an update to the database table needs to be done
    *
    * @param FormRow $oldFormRow The previos values of the Form Row
    * @param FormRow $formRow The new Form Row being created
    * @param Connection $database A database connection
    * */
    public function updateDatabaseRow (FormRow $oldFormRow, FormRow $formRow, Connection $database)
    {
        //a list of fields which need to change for the database to be affected
        $fields = array('field', 'type', 'type_parsed', 'allowed_values', 'null', 'value_min', 'value_max', 'length_min', 'length_max');
        //go through each of the fields
        foreach ($fields as $field) {
            // the name of $field on this entity
            $objectValue = lcfirst(str_replace(' ', '', FunctionsString::removeUnderscores($field)));
            //check whether the value has changed
            if ($this->$objectValue !== $_POST[$field]) {
                //create a new Form Group object
                $formGroup = EntityFactory::build('form_group');
                //load the parent Form Group of this Form Row
                $formGroup->load($this->parents['form_group'][0], $database);
                //create a new Form object
                $form = EntityFactory::build('form');
                //load the parent Form of the Form Group
                $form->load($formGroup->parents['form'][0], $database);
                //Create a new Form Query object
                $formQuery = new FormQuery();
                //get a query to update the database row
                $sqlQuery = $formQuery->updateDatabaseTableRow($form->formModule, $oldFormRow, $formRow);
                //run the query
                $database->runQuery($sqlQuery);
                //break out of the loop
                break;
            }
        }
    }





    /**
    * A method to find the MySql data-type for the selected row type
    *
    * @param string $type The type the user selected
    * @param Array $post The post array
    *
    * @return string The raw MySql data type
    * */
    private function getMySqlDataType($type, $post)
    {
        switch ($type) {
            //a colour is stored as a set of RGB codes
            case 'color':
            case 'email':
            case 'hidden':
            case 'month':
            case 'password':
            case 'tel':
            case 'text':
            case 'url':
            case 'week':
                return 'VARCHAR';
            case 'date':
                return 'DATE';
            case 'datetime':
                return 'DATETIME';
            case 'number':
            case 'range':
                //TINYINT
                //Signed: -128 / 127
                //Unsigned: 0 / 255
                if (($post['value_min'] >= -128 && $post['value_max'] <= 127) || ($post['value_min'] >= 0 && $post['value_max'] <= 255)) {
                    return 'TINYINT';
                }
                //SMALLINT
                //Signed: -32768 / 32767
                //Unsigned: 0 / 65535
                if (($post['value_min'] >= -32768 && $post['value_max'] <= 32767) || ($post['value_min'] >= 0 && $post['value_max'] <= 65535)) {
                    return 'SMALLINT';
                }
                //MEDIUMINT
                //Signed: -8388608 / 8388607
                //Unsigned: 0 / 16777215
                if (($post['value_min'] >= -8388608 && $post['value_max'] <= 8388607) || ($post['value_min'] >= 0 && $post['value_max'] <= 16777215)) {
                    return 'MEDIUMINT';
                }
                //INT
                //Signed: -2147483648 / 2147483647
                //Unsigned: 0 / 4294967295
                if (($post['value_min'] >= -2147483648 && $post['value_max'] <= 2147483647) || ($post['value_min'] >= 0 && $post['value_max'] <= 4294967295)) {
                    return 'INT';
                }
                //BIGINT
                //Signed: -9223372036854775808 / 9223372036854775807
                //Unsigned: 0 / 18446744073709551615
                if (($post['value_min'] >= -9223372036854775808 && $post['value_max'] <= 9223372036854775807) || ($post['value_min'] >= 0 && $post['value_max'] <= 18446744073709551615)) {
                    return 'BIGINT';
                }
            case 'decimal':
                return 'DECIMAL';
            case 'time':
                return 'TIME';
            case 'checkbox':
                return 'TINYINT';
            case 'radio':
                return 'SET';
            case 'textarea':
            case 'html':
                return 'TEXT';
            case 'dropdown':
                return 'ENUM';
        }
    }

    /**
    * Returns whether a MySql field is SIGNED or UNSIGNED or whether it is not applicable
    *
    * @param Array $post The POST variables
    *
    * @return int 0 The field is UNSIGNED
    * @return int 1 The field is SIGNED
    * @return null null It is not applicable for this field
    * */
    private function getMySqlSigned(Array $post)
    {
        //the type parsed determines whether signed is necessary or not
        if ($post['type_parsed'] == 'TINYINT' ||
        $post['type_parsed'] == 'SMALLINT' ||
        $post['type_parsed'] == 'MEDIUMINT' ||
        $post['type_parsed'] == 'INT' ||
        $post['type_parsed'] == 'BIGINT' ||
        $post['type_parsed'] == 'DECIMAL') {
            //if the minimum value is smaller than 0 then the number will be signed
            if ($post['value_min'] < 0) {
                //this field is signed
                return 1;
            } else {
                //this field is unsigned
                return 0;
            }
        }
        //it is not applicable, return null
        return null;
    }

    /**
    * An override for the parent load method
    *
    * @param string $url The unique url of the Entity
    * @param Connection $database The database interface connection
    * @param boolean $loadForm Whether the form for this Entity should be loaded
    * */
    public function load($url, Connection $database, $loadForm = true)
    {
        //call the parent load method
        parent::load($url, $database, $loadForm);
        //make an array of the comma delimited allowedValues variable if possible
        $this->allowedValues = ($this->allowedValues == null) ? null : explode(',', $this->allowedValues);
    }

    /**
    * Builds a form row for the given field
    *
    * @param mixed $input The input that was entered into the field
    *
    * @return string
    * */
    public function buildInput($input = null)
    {
        //set the class's input variable
        $this->input = $input;
        //get the last part of the method name
        $methodName = ucwords($this->type);
        //get the name of the method being tried first
        $method = "build$methodName";
        //see if a method exists for the specified type
        //this method must exist inside this (FormRow) class
        if (method_exists($this, $method)) {
            //call the function to build the row
            return $this->$method();
        } else {
            //there is no specific method that handles this input type, use the default
            return $this->buildTextbox();
        }
    }

    /**
    * Adds the parts of the input that will be required on all inputs
    *
    * @return string
    * */
    private function addDefaultInputValues()
    {
        //add the name attribute
        $name = "name=\"{$this->field}" . (($this->type == 'multiple') ? '[]' : '') . "\" ";
        //see if the field is disabled
        $disabled = (empty($this->disabled)) ? '' : 'disabled="disabled" ';
        //see if the field is set to readonly
        $readonly = (empty($this->readonly)) ? '' : 'readonly="readonly" ';
        //build the default values
        $field = "$name$disabled$readonly";
        //return the default values
        return $field;
    }

    /**
    * Builds an <input type="x" ... /> field
    *
    * @return string
    * */
    private function buildTextbox()
    {
        //add the id attribute
        $id = "id=\"{$this->field}\" ";
        //add the placeholder text if it isn't empty
        $placeholder = (empty($this->placeholder) || $this->type == 'date') ? '' : "placeholder=\"{$this->placeholder}\" ";
        //see if the field has any input
        $input = (empty($this->input)) ? '' : "value=\"{$this->input}\" ";
        //see if this field has a maximum length
        $maxLength = (empty($this->lengthMax) || $this->type == 'number') ? '' : "maxlength=\"$this->lengthMax\" ";
        //see if this field has a maximum AND minimum length and set a regexp pattern to validate accordingly
        $pattern = (empty($this->lengthMax) || empty($this->lengthMin) || $this->type == 'number') ? '' : "pattern=\"{{$this->lengthMin},{$this->lengthMax}}\" ";
        //see if a step was defined if this type is number
        $step = (empty($this->step)) ? '' : "step=\"{$this->step}\" ";
        //see if the field is required or not
        $required = (strtoupper($this->null) == 'NO') ? '' : 'required="required" ';
        //see if a minimum value has been specified
        $min = (empty($this->valueMin)) ? '' : "min=\"{$this->valueMin}\" ";
        //see if a maximum value has been specified
        $max = (empty($this->valueMax)) ? '' : "max=\"{$this->valueMax}\" ";
        //build the input
        $field = "<input {$this->addDefaultInputValues()} type=\"{$this->type}\" class=\"text {$this->type}\" $id$placeholder$input$maxLength$pattern$step$required$min$max />";
        //return the input
        return $field;
    }

    /**
    * Buils a <textarea ...></textarea> field
    *
    * @param boolean $html Whether this is a normal textarea or  if it's a WYSIWYG editor
    *
    * @return string
    * */
    private function buildTextarea($html = false)
    {
        //add the id attribute
        $id = "id=\"{$this->field}\" ";
        //add the placeholder text if it isn't empty
        $placeholder = (empty($this->placeholder)) ? '' : "placeholder=\"{$this->placeholder}\" ";
        //see if this field has any input
        $input = (empty($this->input)) ? '' : $this->input;
        //whether this is a WYSIWYG editor or not
        $html = ($html == false) ? '' : ' html';
        //build the input
        $field = "<textarea $id$placeholder class=\"textarea$html\" {$this->addDefaultInputValues()}>$input</textarea>";
        //return the field
        return $field;
    }

    /**
    * Build a <select>...</select> field
    *
    * @return string
    * */
    private function buildDropdown()
    {
        //add the id attribute
        $id = "id=\"{$this->field}\" ";
        //build the first piece of the dropdown
        $field = "<select class=\"select\" $id {$this->addDefaultInputValues()}>";
        //add the first empty value to the dropdown
        $field .= '<option value="">Select One</option>';
        //see if any options were added for this dropdown list
        if (empty($this->allowedValues)) {
            //no values were given for a dropdown
            throw new Exception("Please supply allowed values for the '{$this->label}' field.");
        } else {
            //go through all the allowed values
            foreach ($this->allowedValues as $allowedValue) {
                //set the selected value to nothing
                $selected = '';
                //see if a value has been set for this field and if that value is equal to the current value
                if ($this->input == $allowedValue) {
                    //this value is selected
                    $selected = ' selected="selected"';
                } else {

                    //see if a default value has been set for this field and if that value is equal to the current value
                    //the input value also has to be empty for this to be true, otherwise a next allowedValue that could be selected might be overwritten
                    if (($this->defaultValue == $allowedValue) && empty($this->input)) {
                        //this value is selected
                        $selected = ' selected="selected"';
                    }
                }
                //add this option to the list
                $field .= "<option$selected>$allowedValue</option>";
            }
        }
        //close off the field
        $field .= '</select>';
        //return the field
        return $field;
    }

    /**
    * Build a <select>...</select> field where multiple items are selectable
    *
    * @return string
    * */
    private function buildMultiple()
    {
        //add the id attribute
        $id = "id=\"{$this->field}\" ";
        //add the multiple attribute
        $multiple = 'multiple="multiple" ';
        //build the first piece of the dropdown
        $field = "<select class=\"select\" $id$multiple {$this->addDefaultInputValues()}>";
        //add the first empty value to the dropdown
        $field .= '<option value="">Select One</option>';
        //see if any options were added for this dropdown list
        if (empty($this->allowedValues)) {
            //no values were given for a dropdown
            throw new Exception("Please supply allowed values for the '{$this->label}' field.");
        } else {
            //go through all the allowed values
            foreach ($this->allowedValues as $allowedValue) {
                //set the selected value to nothing
                $selected = '';
                //see if a value has been set for this field and if that value is equal to the current value
                if (strstr($this->input, $allowedValue)) {
                    //this value is selected
                    $selected = ' selected="selected"';
                } else {

                    //see if a default value has been set for this field and if that value is equal to the current value
                    //the input value also has to be empty for this to be true, otherwise a next allowedValue that could be selected might be overwritten
                    if ((strstr($this->defaultValue, $allowedValue)) && empty($this->input)) {
                        //this value is selected
                        $selected = ' selected="selected"';
                    }
                }
                //add this option to the list
                $field .= "<option$selected>$allowedValue</option>";
            }
        }
        //close off the field
        $field .= '</select>';
        //return the field
        return $field;
    }

    /**
    * Builds a <input type="radio" ... /> field
    *
    * @return string
    * */
    private function buildRadio()
    {
        //see if any allowed values were added for this radio button list
        if (empty($this->allowedValues)) {
            //no values were given for a dropdown
            throw new Exception("Please supply allowed values for the '{$this->label}' field.");
        } else {
            //set up the field variable
            $field = '';
            //go through all the allowed values
            foreach ($this->allowedValues as $allowedValue) {
                //set the checked value to nothing
                $checked = '';
                //see if a value has been set for this field and if that value is equal to the current value
                if ($this->input == $allowedValue) {
                    //this value is checked
                    $checked = ' checked="checked"';
                } else {
                    //see if a default value has been set for this field and if that value is equal to the current value
                    //the input value also has to be empty for this to be true, otherwise a next allowedValue that could be selected might be overwritten
                    if (($this->defaultValue == $allowedValue) && empty($this->input)) {
                        //this value is checked
                        $checked = ' checked="checked"';
                    }
                }
                //build a div for the option
                $field .= "<div class=\"radio\" id=\"{$this->field}_" . strtolower(str_replace(' ', '_',$allowedValue)) . "\">
                               <input type=\"radio\" {$this->addDefaultInputValues()} value=\"$allowedValue\" id=\"radio_{$this->field}_" . strtolower(str_replace(' ', '_',$allowedValue)) . "\"$checked />
                               <label for=\"radio_{$this->field}_" . strtolower(str_replace(' ', '_',$allowedValue)) . "\">
                                   $allowedValue
                               </label>
                           </div>";
            }
            //return the field that was built
            return $field;
        }
    }

    /**
    * Builds a <input type="checkbox" ... /> field
    *
    * @return string
    * */
    private function buildCheckbox()
    {
        //add the id attribute
        $id = "id=\"{$this->field}\" ";
        //add the name attribute
        $name = "name=\"{$this->url}\" ";
        //add the class attribute
        $class='class="checkbox" ';
        //add the selected attribute
        $checked = ($this->input == 1) ? 'checked="checked" ' : '';
        //build the field
        $field = "<input $id$name$class$checked type=\"checkbox\" value=\"1\" />
                  <label for=\"{$this->url}\">{$this->label}</label>";
        //return the field
        return $field;
    }

    /**
    * Builds a <textarea> field with WYSIWYG capabilities
    *
    * @return string
    * */
    private function buildHtml()
    {
        return $this->buildTextarea(true);
    }


    /**
    * Returns the label for a form row
    *
    * @param string $module The module the Form Row is a part of
    * @param string $field The field that we're looking for the label for
    * @param Connection $database The current database connection
    *
    * @return string The label
    * */
    public static function getLabel($module, $field, Connection $database)
    {
        //write a MySql query that retreives the form label
        $sqlSelect = "SELECT `form_row`.`label`
                      FROM `form_row`
                      JOIN `entity_link` AS `row_group_link` ON `form_row`.`id` = `row_group_link`.`child`
                      JOIN `entity_link` AS `group_form_link` ON `row_group_link`.`parent` = `group_form_link`.`child`
                      JOIN  `form` ON  `group_form_link`.`parent` =  `form`.`id`
                      WHERE `form`.`form_module` = '$module'
                      AND `form_row`.`field` = '$field'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //get the resuts from the query
        $result = $database->fetchResult($sqlQuery);
        //check what to return
        return ($result == false) ? false : $result[0];
    }


    /**
    * Creates a view for each type, thus a color type could be displayed as a block with the color inside
    * checkbox, hidden, password, radio, text, color, date, datetime, email, month, number, range, tel, time, url, week
    *
    * @return string The html to be displayed on the page
    * */
    public function display()
    {
        //get the method that will display this type of row
        $methodName = 'display' . ucfirst($this->type);
        //check if a function exists with the name of the type
        if (method_exists($this, $methodName)) {
            //return what the method returns
            return $this->$methodName();
        } else {
            return $this->input;
        }
    }

    /**
    * Displays the value of a textarea
    *
    * @return string
    * */
    private function displayTextarea()
    {
        //return the value with line-breaks added
        return nl2br($this->input);
    }

    /**
    * Displays the value of a checkbox
    *
    * @return string
    * */
    private function displayCheckbox()
    {
        //either a "yes" or "No"
        return ($this->input == '1') ? 'Yes' : 'No';
    }

    /**
    * Displays a hidden field
    *
    * @return string
    * */
    private function displayHidden()
    {
        //return an empty string
        return '';
    }

    /**
    * Displays a password field
    *
    * @return string
    * */
    private function displayPassword()
    {
        //return a blocked field
        return '*****';
    }

    /**
    * Displays a color
    *
    * @return string
    * */
    private function displayColor()
    {
        //return a block with the color as a background color
        return "<div class=\"color\" style=\"background-color: {$this->input};\"></div>";
    }

    /**
    * Displays a date
    *
    * @return string
    * */
    private function displayDate()
    {
        //pass back the date as a string in the correct format
        return '<time datetime="' . date('Y-m-d', strtotime($this->input)) . '">' . date('j M Y', strtotime($this->input)) . '</time>';
    }

    /**
    * Displays a date and time
    *
    * @return string
    * */
    private function displayDatetime()
    {
        //pass back the date as a string in the correct format
        return '<time datetime="' . date('Y-m-d\TG:i:s', strtotime($this->input)) . '">' . date('j M Y G:i:s', strtotime($this->input)) . '</time>';
    }

    /**
    * Displays an email address
    *
    * @return string
    * */
    private function displayEmail()
    {
        //show an email mailto link
        return "<a href=\"mailto:{$this->input}\">{$this->input}</a>";
    }

    /**
    * Displays a month
    * An html5 month attribute's value is a four-digit year, a dash, and a two digit month: 1996-12
    *
    * @return string
    * */
    private function displayMonth()
    {
        //show the date in a proper, readable format
        return date('F Y', strtotime($this->input));
    }

    /**
    * Displays the time
    * The html time input tipe is allowed milliseconds,
    * mysql's time type not, thus milliseconds will be omitted
    *
    * @return string
    * */
    private function displayTime()
    {
        //show the time
        return '<time datetime="' . date('H:i:s', strtotime($this->input)) . '">' . date('H:i:s', strtotime($this->input)) . '</time>';
    }

    /**
    * Displays a url
    *
    * @return string
    * */
    private function displayUrl()
    {
        //show the url as a link (open in a new tab)
        return "<a href=\"{$this->input}\" target=\"_blank\">{$this->input}</a>";
    }

    /**
    * Displays a week
    *
    * @return string
    * */
    private function displayWeek()
    {
        //The format looks like 1996-W23, split with the dash
        $parts = explode('-', $this->input);
        //show the week in the correct format
        return str_replace('W', 'Week ', $parts[1]) . ' ' . $parts[0];
    }

    /**
    * Displays a decimal
    *
    * @return string
    * */
    private function displayDecimal()
    {
        //Format the string to have separating commas
        return number_format($this->input, (int)$this->lengthMin, '.', ',');
    }

}
?>