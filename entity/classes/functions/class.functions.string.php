<?php
/**
* A string manipulation class with various static string functions
* */
class FunctionsString
{
    
    /**
    * Converts the module name to the class name
    * 
    * @param string $moduleName The module name to be converted
    * 
    * @return string The derived class name
    * */
    public static function moduleToClassName($moduleName)
    {
        //replace underscores with spaces, all lowercase, uppercase first letters, replace spaces with nothing
        return str_replace(' ', '', ucwords(strtolower(str_replace('_', ' ',$moduleName))));
    }
    
    /**
    * Add the indefinite article to the word
    * 
    * @param string $word The word to add the indefinite article to
    * 
    * @return string The word with the indefinite article prepended
    * */
    public static function addIndefiniteArticle($word)
    {

        //return the word with the indefinite article added
        return IndefiniteArticle::A($word);
    }
    
    /**
    * Replaces underscores with spaces and converts each word's first letter to uppercase
    * 
    * @param string $string The string to be converted
    * 
    * @return string The word with underscores replaced
    * */
    public static function removeUnderscores($string)
    {
        //return the converted string
        return ucwords(str_replace('_', ' ', strtolower($string)));
    }
    
    /**
    * Returns the plural of a given word
    * 
    * @param string $word The word to be pluralized
    * 
    * @return string The plural of the passed word
    * */
    public static function getPlural($string){
        //set an array of ways to pluralize certain sounds
        $plural = array(
            array('/(quiz)$/i',"$1zes"),
            array('/^(ox)$/i',"$1en"),
            array('/([m|l])ouse$/i',"$1ice"),
            array('/(matr|vert|ind)ix|ex$/i',"$1ices"),
            array('/(x|ch|ss|sh)$/i',"$1es"),
            array('/([^aeiouy]|qu)y$/i',"$1ies"),
            array('/([^aeiouy]|qu)ies$/i',"$1y"),
            array('/(hive)$/i',"$1s"),
            array('/(?:([^f])fe|([lr])f)$/i',"$1$2ves"),
            array('/sis$/i',"ses"),
            array('/([ti])um$/i',"$1a"),
            array('/(buffal|tomat)o$/i',"$1oes"),
            array('/(bu)s$/i',"$1ses"),
            array('/(alias|status)$/i',"$1es"),
            array('/(octop|vir)us$/i',"$1i"),
            array('/(ax|test)is$/i',"$1es"),
            array('/s$/i',"s"),
            array('/$/',"s")
        );
        //words that don't follow the normal rules
        $irregular = array(
            array('move','moves'),
            array('sex','sexes'),
            array('child','children'),
            array('man','men'),
            array('person','people')
        );
        //words that cannot be pluralized
        $uncountable = array( 
            'sheep', 
            'fish',
            'series',
            'species',
            'money',
            'rice',
            'information',
            'equipment',
            'trash'
        );
        // save some time in the case that singular and plural are the same
        if(in_array(strtolower($string),$uncountable)){
            return $string;
        }
        // check for irregular singular forms
        foreach($irregular as $noun){
            if(strtolower($string) == $noun[0]){
                return $noun[1];
            }
        }
        // check for matches using regular expressions
        foreach($plural as $pattern){
            if(preg_match($pattern[0],$string)){
                return preg_replace( $pattern[0], $pattern[1], $string );
            }
        }
        //return the pluralized word
        return $string;
    }
    
    public static function telephone($telephone)
    {
        return '+27 (0)' . number_format(substr($telephone, 1, -1), 0, '.', ' ') . substr($telephone, strlen($telephone)-1, 1);
    }
    
    public static function url($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    
}
?>