<?php
/**
* Handles Form Link elements
* A Form Link is a way of establishing a link between one module and another
* Thus, it is possible to (for example) link a Product to a Product category via a Form Link
* */
class FormLink extends Entity
{
    
    
    /**
    * The Form that will be included
    * 
    * @var string
    * */
    public $include = null;
    
    /**
    * The title/name of the Form Link
    * 
    * @var string
    * */
    public $title = null;
    
    /**
    * A description of the Form Link being included
    * 
    * @var string
    * */
    public $description = null;
    
    /**
    * The relationship that the $include has with the Form being linked to
    * 
    * @var string = null;
    * */
    public $relationship = null;
    
    /**
    * Whether it is required to select/create one of these includes 0|1
    * 
    * @var string
    * */
    public $required = null;
    
    /**
    * Whether more than one $include can be included 0|1
    * 
    * @var string
    * */
    public $multiple = null;
    
    /**
    * Whether you are able to select an $include 0|1
    * 
    * @var string
    * */
    public $select = null;
    
    /**
    * Whether you are able to create an $include 0|1
    * 
    * @var string
    * */
    public $create = null;
    
    /**
    * Holds the module being included
    * 
    * @var string
    * */
    public $module = null;
    
    /**
    * Holds the location of the "select" option's file
    * 
    * @var string
    * */
    public $selectFile = null;
    
    /**
    * Holds the location of the "create" option's file
    * 
    * @var string
    * */
    public $createFile = null;
    
    
    /**
    * Builds a Form Link element on an html Form
    * 
    * @param string $module The current module a link is being built for
    * @param Connection $database The current database connection
    * @param Entity $entity The entity being linked on, This is for finding current existing links
    * @param System $system the system object for the Entity being created
    * */
    public function build($module, Connection $database, Entity $entity, System $system)
    {
        //set the class's module
        $this->module = $module;
        //get the "select" file location
        $selectFile = $this->selectFile = System::buildUrl('!modal', true, $module, 'link', '!select', null);
        //get the "create" file location
        $createFile = $this->createFile = System::buildUrl('!modal', true, $module, 'link', '!create', null);
        //get the current FormLink object as an object to be used on the include page
        $formLink = $this;
        //build the string filename of the file to include
        $buildFile = dirname(__FILE__) . "/../../modules/{$module}/link/index.php";
        //see if the build file exists within the specified module
        $buildFile = (file_exists($buildFile)) ? $buildFile: dirname(__FILE__) . '/../../modules/entity/link/index.php';
        //see if the required file exists
        if (file_exists($buildFile)) {
            //include the file
            require($buildFile);
        }
        else {
            echo "<div class=\"error\">The $module link could not be found, this form might fail because of this.</div>";
        }
    }
    
    /**
    * Finds the url of a Form Link based on the module and the module being linked
    * 
    * @param string $module The module being linked to
    * @param string $link The module being linked
    * @param Connection $database The database connection
    * 
    * @return string The url that was found
    * @return boolean false No url was found
    * */
    public function getUrl($module, $link, $database)
    {
        //build a query to find the url
        $sqlSelect = "SELECT `entity`.`url`
                      FROM `form_link`
                      JOIN `entity` ON `form_link`.`id` = `entity`.`id`
                      JOIN `entity_link` ON `form_link`.`id` = `entity_link`.`child`
                      JOIN `form` ON `entity_link`.`parent` = `form`.`id`
                      WHERE `form_link`.`include` = '$link'
                      AND `form`.`form_module` = '$module'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //get the query's result
        $result = $database->fetchResult($sqlQuery);
        //check if the query had returned anything
        if ($result != false && is_array($result)) {
            //return the url
            return $result[0];
        }
        //return a default false
        return false;
    }
    
}
?>