<?php
/**
* The Image class handles image operations such as resizing or saving, etc
* */
class Image extends Entity
{
    
    /**
    * The name/title of the Image
    * 
    * @var string
    * */
    public $title = null;
    
    /**
    * A description of the Image
    * 
    * @var string
    * */
    public $description = null;
    
    /**
    * The original name of the Image when it was uploaded
    * 
    * @var string
    * */
    public $filename = null;
    
    /**
    * The path of where the Image lies on the server
    * 
    * @var string
    * */
    public $path = null;
    
    /**
    * The size of the Image on the server in bytes
    * 
    * @var integer
    * */
    public $filesize = null;
    
    /**
    * The width of the Image in pixels
    * 
    * @var integer
    * */
    public $width = null;
    
    /**
    * The height of the Image in pixels
    * 
    * @var integer
    * */
    public $height = null;
    
    /**
    * The actual image file for resizing purposes
    * 
    * @var image
    * */
    private $image = null;
    
    /**
    * The type of image that this is IMAGETYPE_JPEG|IMAGETYPE_GIF|IMAGETYPE_PNG
    * 
    * @var string
    * */
    public $imageType = null;
    
    
    /**
    * Loads the actual image as an image resource identifier
    * */
    private function loadImage()
    {
        //get the size of the Image
        $imageInfo = getimagesize($this->path);
        //get what type of image this is
        $this->imageType = $imageInfo[2];
        //load the image based on the type of the image
        switch($this->imageType) {
            case IMAGETYPE_JPEG :
                $this->image = imagecreatefromjpeg($this->path);
                break;
            case IMAGETYPE_GIF :
                $this->image = imagecreatefromgif($this->path);
                break;
            case IMAGETYPE_PNG :
                $this->image = imagecreatefrompng($this->path); 
                break;
            default :
                throw new Exception('This image is of the incorrect type. Only jpg, gif and png files.');
        }
    }
    
    /**
    * Save the image to disk
    * 
    * @param string $filename The name/path of the image to be saved
    * @param integer $compression The quality of the image (0-100)
    * */
    private function saveImage($filename, $compression = 100) {
        //check what type of image we are working with and save the correct type
        switch ($this->imageType) {
            case IMAGETYPE_JPEG :
                imagejpeg($this->image, $filename, $compression);
                break;
            case IMAGETYPE_GIF :
                imagegif($this->image, $filename);
                break;
            case IMAGETYPE_PNG :
                imagepng($this->image, $filename);
                break;
        }
    }
    
    /**
    * Resizes an image to a specified height while keeping the aspect ratio
    * 
    * @param integer $height The new height of the image in pixels
    * */
    private function resizeToHeight($height) {
        //load the image
        $this->loadImage();
        //figure out the ration of the height compared to the original height
        $ratio = $height / $this->height;
        //alter the new width by the same ratio
        $width = $this->width * $ratio;
        //resize the image
        self::resize($width, $height);
    }
    
    /**
    * Resizes an image to a specified width while keeping the aspect ratio
    * 
    * @param integer $width The new width of the image in pixels
    * */
    private function resizeToWidth($width) {
        //load the image
        $this->loadImage();
        //figure out the ration of the width compared to the original width
        $ratio = $width / $this->width;
        //alter the new height by the same ratio
        $height = $this->height * $ratio;
        //resize the image
        self::resize($width, $height);
    }
    
    /**
    * Resize the actual image
    * 
    * @param integer $width The new width of the image in pixels
    * @param integer $height The new height of the image in pixels
    * */
    private function resize($width, $height) {
        //load the image
        $this->loadImage();
        //create the new (resized) image
        $newImage = imagecreatetruecolor($width, $height);
        //check whether it is a gif or pbg so we can retain transparency
        if ($this->imageType == IMAGETYPE_GIF || $this->imageType == IMAGETYPE_PNG) {
            //get the opacity level of the image
            $currentTransparent = imagecolortransparent($this->image);
            //if there is only one transparency level it is a gif
            if ($currentTransparent != -1) {
                $transparentColor = imagecolorsforindex($this->image, $currentTransparent);
                $currentTransparent = imagecolorallocate($newImage, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']);
                imagefill($newImage, 0, 0, $currentTransparent);
                imagecolortransparent($newImage, $currentTransparent);
            } elseif ($this->imageType == IMAGETYPE_PNG) {
                imagealphablending($newImage, false);
                $color = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
                imagefill($newImage, 0, 0, $color);
                imagesavealpha($newImage, true);
            }
        }
        //create the new image
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $width, $height, $this->width, $this->height);
        //load the image
        $this->image = $newImage;
    }
    
    /**
    * Automatically figure out the actual size to resize the image to
    * 
    * @param integer $width The desired width of the image
    * @param integer $height The desired height of the image
    * */
    public function autoResize($width, $height)
    {
        //the directory to check
        $directory = "entity/images/uploads/{$width}_{$height}";
        //see if the correct folder exists
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        //load the image
        $this->loadImage();
        //get the ratio by which the width is smaller/larger than the original width
        $widthRatio = $width / $this->width;
        //get the ratio by which the height is smaller/larger than the original height
        $heightRatio = $height / $this->height;
        //see if the width is larger than the height
        if ($widthRatio < $heightRatio) {
            //resize the image by height
            $this->resizeToHeight($height);
        } else {
            //resize the image by width
            $this->resizeToWidth($width);
        }
        //get a new filename for this image
        $newFilename = str_replace('/original/', "/{$width}_{$height}/", $this->path);
        //save the image
        $this->saveImage($newFilename);
    }
    
    /**
    * Resize an Image to an exact size
    * 
    * @param integer $width The desired width of the image
    * @param integer $height The desired height of the image
    * */
    public function resizeExact ($width, $height) {
        //the directory to check
        $directory = "entity/images/uploads/{$width}_{$height}";
        //see if the correct folder exists
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        //get a new filename for this image
        $newFilename = str_replace('/original/', "/{$width}_{$height}/", $this->path);
        //see if the file exists
        if (!file_exists($newFilename)) {
            //resize the Image
            self::resize($width, $height);
            //save the image
            $this->saveImage($newFilename);
        }
    }
    
    /**
    * Displays an Image
    * 
    * @param integer $width The width of the desired Image in pixels
    * @param integer $height The height of the desired Image in pixels
    * @param string $leftPosition The alignment of the Image horizontally if it is cut-off. middle|left|right
    * @param string $topPosition The alignment of the Image verically if it is cut-off. middle|top|bottom
    * @param string $extraClasses Extra classes to be added to the div
    * @param string $link The link for the anchor inside the div
    * @param boolean $modal Whether the link should open in a modal or not
    * @param boolean $raw Whether the image should be displayed as an image (true), or in an exact-sized div (false)(default)
    * @param string $id An id attribute for the image/div
    * 
    * @return string A string containing a path to the resized Image
    * */
    public function display($width, $height, $leftPosition='center', $topPosition='center', $extraClasses=null, $link=null, $modal=true, $raw=false, $id=false)
    {
        //build a path of where the resized image is
        $path = str_replace('/original/', "/{$width}_{$height}/", $this->path);
        //set the id to the correct form
        $id = ($id === false) ? '' : " id=\"{$id}\"";
        //check if an Image of the desired size is already on the system
        if (!file_exists($path)) {
            //resize the Image
            $this->autoResize($width, $height);
        }
        //check whether the image should be displayed by itself
        if ($raw === true) {
            //build the image tag
            $image = "<img src=\"/{$path}\" alt=\"{$this->title}\" class=\"{$extraClasses}\"{$id} />";
        } else {
            //build the div for this image to be in
            $image = "<div class=\"display_image size_{$width}_{$height}\" style=\"width: {$width}px; height: {$height}px; background-image: url(/$path); background-position: $leftPosition $topPosition;\"$id>";
            //add the anchor
            if ($link != null) {
                $image .= "<a href=\"{$link}\"";
                $image .= ($modal == true) ? " class=\"colorbox\" rel=\"image_gallery\"" : '';
                $image .= "></a>";
            }
            $image .= "</div>";
        }
        //return the path
        return $image;
    }
    
    
    /**
    * Permanently deletes an Image
    * 
    * @param Connection $database The database connection
    * */
    public function permanentlyDelete($database)
    {
        //delete the database entries
        parent::permanentlyDelete($database);
        //the main folder where all uploaded images are placed
        $uploadFolder = dirname(__FILE__) . '/../../images/uploads/';
        //check if the folder opens
        if ($uploadHandle = opendir($uploadFolder)) {
            //go through each file in the folder
            while (false !== ($sizeFolder = readdir($uploadHandle))) {
                //check that the folder isn't a directory handler
                if ($sizeFolder !== '.' && $sizeFolder !== '..') {
                    //get the full path of the size folder
                    $sizeFolder = $uploadFolder . $sizeFolder . '/';
                    //check if the size folder opens
                    if ($sizeFolderHandle = opendir($sizeFolder)) {
                        //go through all the files within this folder
                        while (false !== ($imageFile = readdir($sizeFolderHandle))) {
                            //check that this is the Image we are looking for
                            if ($imageFile === str_replace('entity/images/uploads/original/', '', $this->path)) {
                                //delete the actual Image file
                                unlink($sizeFolder . $imageFile);
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /**
    * Handles the upload of an image
    * 
    * @param array $file the uploaded file ($_FILES[name]) array that contains all the upload information
    * 
    * @return array An array with the success or failure result
    * */
    public static function upload ($file)
    {
        //A list of valid file types
        $validExtensions = array('jpeg', 'jpg', 'png', 'gif');
        //The maximum allowed file size (1024kb * 10 = 10mb)
        $maximumFileSize = 1024 * 1024 * 10;
        //The directory to upload to
        $path = 'entity/images/uploads/original/';
        // get uploaded file extension
        $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        // looking for format and size validity
        if (in_array($extension, $validExtensions) && $file['size'] < $maximumFileSize) {
            // unique file path
            $path = $path . uniqid(). '.' .$extension;
            // move uploaded file from temp to uploads directory
            if (move_uploaded_file($file['tmp_name'], $path)) {
                //get the size of the image (width/height) as an array
                $imageSize = getimagesize($path);
                //get the width of the image
                $width = $imageSize[0];
                //get the height of the image
                $height = $imageSize[1];
                //return the result as an array
                return array(
                    'uploaded' => 'true',
                    'fileName' => $file['name'],
                    'fileSize' => $file['size'],
                    'width' => $imageSize[0],
                    'height' => $imageSize[1],
                    'path' => $path
                );
            } else {
                $error = 'Upload Fail: Server/Upload error';
            }
        } else {
            $error = 'Upload Fail: Unsupported file format or it is too large to upload';
        }
        // return the error as an array
        return array('uploaded' => 'false', 'error' => $error);
    }
    
    
}
?>