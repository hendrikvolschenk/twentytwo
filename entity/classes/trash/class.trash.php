<?php
class Trash extends Entity
{
    
    /**
    * Returns an array of all matching Entities' urls
    * 
    * @param Connection $database The database connection
    * @param string $module The type of Entities to find
    * @param string|array The status(ses) to match
    * 
    * @return array A list of metching Entity urls
    * */
    public static function listAll(Connection $database, $module, $status = 'D')
    {
        //write a query that will find all Entity ids matching the module
        $sqlSelect = "SELECT DISTINCT(`url`)
                      FROM `entity`
                      WHERE `status` = 'D'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //return the result of the query
        return $database->fetchResult($sqlQuery);
    }
    
}
?>