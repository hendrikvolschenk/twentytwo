<?php

/**
* The System class initializes Entity Framework
* */
class System
{

    /**
    * The settings variable holds an array of all settings loaded from the configuration file
    *
    * @var array
    * */
    public $settings = null;

    /**
    * The templateView variable holds the view that is being applied to the template
    *
    * @var string
    * */
    public $templateView = null;

    /**
    * The admin variable holds whether admin mode is on or off
    *
    * @var boolean
    * */
    public $admin = false;

    /**
    * The module variable holds the current Entity Framework module
    *
    * @var string
    * */
    public $module = null;

    /**
    * The action variable holds the action the is being performed
    *
    * @var string
    * */
    public $action = null;

    /**
    * The view variable holds the view that will be applied to the action
    * If left blank the default view will be used
    *
    * @var string
    * */
    public $view = null;

    /**
    * The url variable holds the url of the Entity being loaded
    *
    * @var string
    * */
    public $url = null;

    /**
    * The user variable holds a User Entity of the currently logged-in user
    *
    * @var Entity
    * */
    public $user = null;

    /**
    * The formAction variable holds the Form Action object of the current action being performed
    *
    * @var FormAction
    * */
    public $formAction = null;


    /**
    * The loadSettings method loads the System settings from the configuration file
    * */
    public function loadSettings()
    {
        //set the configuration file's location
        $configurationFile = dirname(__FILE__) . '/../../system/configuration.ini';
        //only continue if the configuration file is in its place
        if (file_exists($configurationFile)) {
            //get the configuration settings as an array
            $configurationSettings = parse_ini_file($configurationFile);
            //check if any configuration settings were found
            if (count($configurationSettings > 0)) {
                //go through each setting
                foreach ($configurationSettings as $settingName=>$settingValue) {
                    //add the value of the setting in the array to be returned
                    $this->settings[strtoupper($settingName)] = $settingValue;
                }
            }
        } else {
            //throw an error about the missing configuration file
            throw new Exception("No configuration file found at '{$configurationFile}'");
        }
    }

    /**
    * The loadModuleActionUrl loads the module, action, view and url variables
    *
    * @param Connection $database The database connection
    * */
    public function loadModuleActionUrl(Connection $database)
    {
        //The templateView is the view that is applied to the template
        $this->templateView = (isset($_GET['template_view']) && !empty($_GET['template_view'])) ? $_GET['template_view'] : null;
        //Admin mode is turned on or off based on whether the "entity" GET variable is set
        $this->admin = (isset($_GET['entity']) && $_GET['entity'] === 'entity') ? true : false;
        //The module, action, view and url variables are set
        $this->module = (isset($_GET['module']) && !empty($_GET['module'])) ? $_GET['module'] : $this->settings['DEFAULT_MODULE'];
        $this->action = (isset($_GET['action']) && !empty($_GET['action'])) ? $_GET['action'] : $this->settings['DEFAULT_ACTION'];
        $this->view = (isset($_GET['view']) && !empty($_GET['view'])) ? $_GET['view'] : null;
        $this->url = (isset($_GET['url']) && !empty($_GET['url'])) ? $_GET['url'] : $this->settings['DEFAULT_URL'];
        //HENDRIK: Still need to load defaults if none are set
        //Load the Form Action
        $this->formAction = $this->loadFormAction($database);
    }

    /**
    * The loadFormAction method loads the Form object's formAction variable with a Form Action object
    *
    * @param Connection $database The database connection
    * */
    private function loadFormAction(Connection $database)
    {
        //get a new Form Action object
        $formAction = EntityFactory::build('form_action');
        //get the url of the form action to be loaded
        $formActionUrl = $formAction->getUrl($database, $this->module, $this->action);
        //load the Form Action
        $formAction->load($formActionUrl, $database);
        //return the Form Action
        return $formAction;
    }

    /**
    * The loadClasses method loads the PHP classes from the /entity/classes/ folder
    * */
    public function loadClasses()
    {
        //set the classes directory
        $classesDirectory = dirname(__FILE__) . '/../../classes';
        //load the main Entity class
        require_once("$classesDirectory/entity/class.entity.php");
        //open the classes folder for reading
        $classesFolder = opendir($classesDirectory);
        //read through the classes folder
        while(false !== ($className = readdir($classesFolder))){
            //check that this directory is not an indicator
            if ($className != '.' && $className != '..'){
                //set the class' directory
                $classDirectory = "$classesDirectory/$className";
                //open the class' folder for reading
                $classFolder = opendir($classDirectory);
                //read through the class' folder
                while (false !== ($classFile = readdir($classFolder))) {
                    //check that a class file is being included
                    if (strstr($classFile, 'class.') && $classFile != 'class.entity.php') {
                        //load the class
                        require_once("$classDirectory/$classFile");
                    }
                }
            }
        }
    }

    /**
    * Finds the IDs of all the Entities on the page
    * */
    public function loadEntities()
    {
        //if the url is empty, no need to check
        //if the url isn't 'all', no need to check
        if (!empty($this->url) && $this->url == 'all') {
            //get the Enitity onject
            return Entity::getAll($this->module);
        }
        //return null as a default
        return false;
    }

    /**
    * Builds a propely formatted url for links or redirects
    *
    * @param string $templateView The current view of the template
    * @param boolean $entity Whether admin mode is enabled or not
    * @param string $module The module being linked to
    * @param string $action The action being linked to in the module
    * @param string $view The view being applied to the action
    * @param string $url The specific implementation of the module
    *
    * @return string
    * */
    public static function buildUrl($templateView, $entity, $module, $action, $view = null, $url = null)
    {
        //add the exclamation and slash to the template if required
        $templateView = (!empty($templateView)) ? ((strpos($templateView, '!') === false) ? "/!$templateView" : "/$templateView") : '';
        //add the admin (/entity) parameter if required
        $entity = ($entity === true) ? '/entity' : '';
        //add the module
        $module = "/$module";
        //add the action
        $action = "/$action";
        //add the view if required
        $view = (!empty($view)) ? ((strpos($view, '!') === false) ? "/!$view" : "/$view") : '';
        //add the url if required
        $url = (!empty($url)) ? "/$url" : '';
        //return the full url
        return $templateView . $entity . $module . $action . $view . $url;
    }

    /**
    * Checks to see if the framework is installed
    *
    * @return boolean Whether the system is installed
    * */
    public function isInstalled()
    {
        // the location of the settings file
        $settingsFile = dirname(__FILE__) . '/../../system/configuration.ini';
        // see if the settings file exists
        return (file_exists($settingsFile)) ? true : false;
    }

    /**
    * Checks whether the home page is currently being viewed
    *
    * @method isHomePage
    * @return {Boolean} Whether the home page is shown
    * */
    public function isHomePage()
    {
        // check if the default module is selected
        if ($this->module === $this->settings['DEFAULT_MODULE']) {
            // check if the default action is selected
            if ($this->action === $this->settings['DEFAULT_ACTION']) {
                // check if the default url is selected
                if ($this->url === $this->settings['DEFAULT_URL']) {
                    // this is the home page
                    return true;
                }
            }
        }
        // this is not the home page
        return false;
    }

    /**
    * The __toString method will print the class to screen
    * */
    public function __toString()
    {
        //print the current object
        return '<pre>' . print_r($this, true) . '</pre>';
    }

}
?>