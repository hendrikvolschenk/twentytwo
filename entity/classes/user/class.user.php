<?php
class User extends Entity
{
    
    /**
    * The email address of the User
    * 
    * @var string
    * */
    public $emailAddress = null;
    
    /**
    * The User's name
    * 
    * @var string
    * */
    public $firstName = null;
    
    /**
    * The User's surname
    * 
    * @var string
    * */
    public $lastName = null;
    
    /**
    * An MD5 hashed version of the User's password
    * 
    * @var string
    * */
    public $password = null;
    
    
    /**
    * Checks whether a user's credentials are acceptable
    * 
    * @param string $emailAddress The email address that was filled in
    * @param string $password The password that the user has filled in
    * 
    * @return array Whether the Login was successful or not
    * */
    public function login ($emailAddress, $password, $database)
    {
        //create a Form Query object
        $formQuery = new FormQuery();
        //create a query tat will check whether the user credentials are correct or not
        $loginQuery = $formQuery->login($emailAddress, $password);
        //run the query and get the result
        $result = $database->runQuery($loginQuery);
        //return whether the login was successful or not
        if ($result == false) {
            $returnArray = array(
                'validated' => 'false',
                'groups' => array(
                    'user_login' => array(
                        'validated' => 'false',
                        'rows' => array(
                            'email_address' => array(
                                'validated' => 'false',
                                'errorMessage' => ''
                            ),
                            'password' => array(
                                'validated' => 'false',
                                'errorMessage' => 'The Email Address/Password combination is not correct.'
                            )
                        )
                    )
                )
            );
        } else {
            //get the user url
            $userUrl = mysql_fetch_row($result);
            $returnArray = array(
                'validated' => 'true',
                'userUrl' => $userUrl[0],
                'redirect' => '/entity/user/view/all'
            );
        }
        //return the result
        return $returnArray;
    }
    
    /**
    * Checks whether a user has access to a specified Form Action
    * 
    * @param FormAction $formAction The Form Action that access is required to
    * 
    * @return bool Whether or not the user has access
    * */
    public function hasAccess(FormAction $formAction)
    {
        //see whether the User's User Group has access to the Form Action
        if (isset($formAction->parents['user_group']) && in_array($this->children['user_group'][0], $formAction->parents['user_group'])) {
            //the user has access to this Form Action
            return true;
        } else {
            //see whether the User has access to the Form Action
            if (isset($formAction->parents['user']) && in_array($this->url, $formAction->parents['user'])) {
                return true;
            }
        }
        //return false by default
        return false;
    }
    
    /**
    * Checks whether a user has access to a Form
    * 
    * @param string $formId The id of the Form
    * @param Connection $database A database connection
    * @param boolean $mainMenuOnly Whether access is for main menu display reasons
    * 
    * @return boolean Whether the User has access to the Form or not
    * */
    public function hasFormAccess($formId, Connection $database, $mainMenuOnly=false)
    {
        //create a new User Group object
        $userGroup = EntityFactory::build('user_group');
        //load the User Group
        $userGroup->load($this->children['user_group'][0], $database);
        //create a query to check whether the User has access
        $sqlSelect = "SELECT `form_action`.`id`
                      FROM `form_action`
                      JOIN `entity` ON `form_action`.`id` = `entity`.`id`
                      JOIN `entity_link` ON `form_action`.`id` = `entity_link`.`child`
                      JOIN `user_group` ON `entity_link`.`parent` = `user_group`.`id`
                      WHERE `entity`.`status` = 'A'
                      AND `user_group`.`id` = {$userGroup->id}
                      AND `form_action`.`id` IN (
                          SELECT `form_action`.`id`
                          FROM `form_action`
                          JOIN `entity` ON `form_action`.`id` = `entity`.`id`
                          JOIN `entity_link` ON `form_action`.`id` = `entity_link`.`child`
                          JOIN `form` ON `entity_link`.`parent` = `form`.`id`
                          WHERE `entity`.`status` = 'A'
                          AND `form`.`id` = $formId ";
        $sqlSelect .= ($mainMenuOnly === true) ? 'AND `form_action`.`display_main_menu` = 1' : '';
        $sqlSelect .= ')';
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //return whether the user has access or not
        return ($sqlQuery == false) ? false : true;
    }
    
    
    /**
    * Returns an array of all matching Entities' urls
    * 
    * @param Connection $database The database connection
    * @param string $module The type of Entities to find
    * @param string|array The status(ses) to match
    * 
    * @return array A list of metching Entity urls
    * */
    public static function listAll(Connection $database, $module, $status = 'A')
    {
        //get the system variable
        global $system;
        //check if this is a minisite administrator
        if ($system->user->children['user_group'][0] === 'users') {
            //write a query that will find all Entity urls matching the module
            $sqlSelect = "SELECT DISTINCT(`entity`.`url`)
                          FROM `entity`
                          JOIN `entity_link` ON `entity`.`id` = `entity_link`.`parent`
                          JOIN `entity` AS `group_entity` ON `entity_link`.`child` = `group_entity`.`id`
                          WHERE `entity`.`module` = 'user'
                          AND `group_entity`.`module` = 'user_group'
                          AND `group_entity`.`url` != 'administrators'
                          AND `group_entity`.`url` != 'visitors'
                          AND `entity`.`status` = 'A'";
        } else {
            //return the default parent method
            return parent::listAll($database, $module, $status);
        }
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //return the result of the query
        return $database->fetchResult($sqlQuery);
    }
    
}
?>