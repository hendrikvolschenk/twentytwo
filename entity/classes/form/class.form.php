<?php
/**
* Builds all the necessary components for a form
* The form structure is defined within the table structure of the Entity's table
* Additional paremeters can be set in the `form_...` tables
* */
class Form extends Entity
{
    
    /**
    * The module's name (lowercase, spaces replaced by underscores)
    * 
    * @var string
    * */
    public $moduleName = null;
    
    /**
    * The module's name
    * 
    * @var string
    * */
    public $name;
    
    /**
    * The url column of the Form
    * This is the column that will be the most unique identifier in the Form
    * 
    * @var string
    * */
    public $urlColumn = null;
    
    /**
    * When a new Entity is created from this Form, what should the status be (A-Z)
    * 
    * @var string
    * */
    public $defaultStatus = null;
    
    /**
    * Whether this Form should be displayed in the main menu or not 1|0
    * 
    * @var string
    * */
    public $display = null;
    
    
    /**
    * Overwrites the create method in the parent Entity class
    * 
    * @param array $post A key-value pair array containing the MySql column name as the key and the value as the value
    * @param Form $form The form object containing information about the type(s)
    * @param Connection $database The connection to the database
    * 
    * @return boolean Whether the create was successful or not
    * */
    public function create(Array $post, Form $form, Connection $database)
    {
        //call the parent create method to insert the `form` table data
        parent::create($post, $form, $database);
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //fetch the query to build the database table
        $query = $formQuery->createDatabaseTable($this, $database);
        //run the query
        return $database->runQuery($query);
    }
    
    /**
    * Overrides the parent load function
    * 
    * @param string $url The unique url of the Entity
    * @param Connection $database The database interface connection
    * @param boolean $loadForm Whether the form for this Entity should be loaded
    * */
    public function load($url, Connection $database, $loadForm = true)
    {
        //call the parent load function
        parent::load($url, $database, $loadForm);
        //see if this Form has a url Column
        if (isset($this->urlColumn)) {
            //change the url column to one without underscores
            $this->urlColumn = lcfirst(str_replace(' ', '',ucwords(str_replace('_', ' ', $this->urlColumn))));
        }
    }
    
    /**
    * An additional function is required to find the form's url
    * 
    * @param Connection $connection A database connection
    * @param string $module What type of Entuty the form is being built for
    * */
    public static function getUrl(Connection $connection, $module)
    {
        //build a query to get the url of the form using the module and action to find it
       $sqlSelect = "SELECT `entity`.`url`
                     FROM `form`
                     JOIN `entity` ON `form`.`id` = `entity`.`id`
                     WHERE `form`.`form_module` = '$module'
                     AND `entity`.`status` = 'A'
                     LIMIT 1";
        //run the query
        $sqlQuery = $connection->runQuery($sqlSelect);
        //get the results of the query
        $result = $connection->fetchResult($sqlQuery);
        //return the first result in the array
        return (isset($result[0])) ? $result[0] : null;
    }
    
    /**
    * Builds the form's action attribute
    * 
    * @param string $module The module the form is posting to
    * @param string $action The action the form is performing
    * */
    public function getFormAction($module, $action)
    {
        //build the form's action
        $formAction = "/!/entity/{$module}/{$action}";
        //return the new form action
        return $formAction;
    }
    
    /**
    * Permanently deletes a Form
    * 
    * @param Connection $database The database connection
    * */
    public function permanentlyDelete($database)
    {
        //build a query to delete the database table
        $sqlDelete = "DROP TABLE IF EXISTS `{$this->formModule}`";
        //run the query
        $database->runQuery($sqlDelete);
        //call the parent permanentlyDelete function
        parent::permanentlyDelete($database);
    }
    
    
}
?>