<?php
/**
* The EntityFactory is used to load an entity
* It checks to see whether a class with the given module name exists
* If the class exists a new object (of type $module) is returned, if not en Entity object is returned
* */
class EntityFactory
{

    /**
    * The build method checks whether the specified $module class exists
    * If the class exists a new object (of type $module) is returned, if not en Entity object is returned
    *
    * @param string $module The module that is being loaded
    *
    * @return object The class the was loaded
    * */
    public static function build($module)
    {
        //convert the module name to a class name
        $class = FunctionsString::moduleToClassName($module);
        //check if the $module class exists and accordingly set the class to be loaded
        $class = (class_exists($class)) ? $class : 'Entity';
        //get the class as an object
        $object = new $class;
        //set the module variable - This is for the load() method to know where to query
        $object->module = $module;
        //return the new object
        return $object;
    }

    /**
    * Builds a list of Entities of the given module
    *
    * @param Database $database The database to be passed
    * @param string $module The module that is being searched for
    * @param string $status The status (string|array) that is being passed
    *
    * @return array An array of Entity ids
    * */
    public static function buildList(Connection $database, $module, $status = 'A')
    {
        //convert the module name to a class name
        $class = FunctionsString::moduleToClassName($module);
        //check whether the $module class exists
        if (class_exists($class)) {
            //check whether the static listAll method exists withing the $module class
            if (method_exists($class, "listAll")) {
                //return the result from the class/method found
                return call_user_func("{$class}::listAll", $database, $module, $status);
            }
        }
        //load the default list from the Entity class
        return Entity::listAll($database, $module, $status);
    }

}
?>