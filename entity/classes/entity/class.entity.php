<?php
/**
* The Entity class builds an Entity from a database table
* */
class Entity
{
    
    /**
    * The url variable holds the database url column's value which identifies the Entity
    * 
    * @var string
    * */
    public $url = null;
    
    /**
    * The id variable holds the unique id of the Entity
    * 
    * @var integer
    * */
    public $id = null;
    
    /**
    * The module variable holds what type of Entity this is
    * 
    * @var string
    * */
    public $module = null;
    
    /**
    * The created variable holds the creation date of this Entity
    * 
    * @var datetime
    * */
    public $created = null;
    
    /**
    * The modified variable holds the last date/time that the Entity was updated
    * 
    * @var datetime
    * */
    public $modified = null;
    
    /**
    * The status variable holds the current status of the Entity
    * 
    * @var character
    * */
    public $status = null;
    
    /**
    * The parents variable holds an array of parent Entities
    * 
    * @var array
    * */
    public $parents = null;
    
    /**
    * The children variable holds an array of child Entities
    * 
    * @var array
    * */
    public $children = null;
    
    /**
    * The settings variable holds an array of seetings returned from the `entity_setting` table
    * 
    * @var array
    * */
    public $settings = null;
    
    
    /**
    * The load method loads the Entity from the respective database tables
    * 
    * @param string $url The unique url of the Entity
    * @param Connection $database The database interface connection
    * @param boolean $loadForm Whether the form for this Entity should be loaded
    * */
    public function load($url, Connection $database, $loadForm = true)
    {
        //write a query that will select all data from the `entity` table and from the corresponding ($this->module) table
        $sqlSelect = "SELECT `entity`.`url`, `entity`.`module`, `created`, `modified`, `status`, `{$this->module}`.*
                      FROM `entity`
                      JOIN `{$this->module}` ON `entity`.`id` = `{$this->module}`.`id`
                      WHERE `entity`.`url` = '$url' OR `entity`.`id` = '$url'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //see if the query failed
        if ($sqlQuery != false) {
            //go through the result. I NEED TO CHANGE THIS TO USE THE FETCHRESULT() METHOD IN THE DB CLASS
            while ($row = mysql_fetch_assoc ($sqlQuery)) {
                //go through the row as individual key/value pairs
                foreach ($row as $key=>$value) {
                    //turn the key to camel case
                    $key = lcfirst(functionsString::moduleToClassName($key));
                    //assign the value to the key as an object variable
                    $this->$key = stripslashes($value);
                }
            }
            //add the links to this object
            $this->parents = $this->getLinks($database, 'parent');
            $this->children = $this->getLinks($database, 'child');
        }
        //avoid the continuous loop
        if ($loadForm === true) {
            //link the settings to this Entity
            $this->form = $this->getForm($database, $this->module);
        }
    }
    
    /**
    * The getLinks method gets all links this Entity has to either parents or children
    * 
    * @param Connection $database The database connection object
    * @param string $relationship The relation ship between the object and the list we're fetching. parent|child.
    * */
    protected function getLinks(Connection $database, $relationship = 'parent')
    {
        //the current onject's relationship to the item(s) being selected
        $objectRelationship = ($relationship == 'parent') ? 'child' : 'parent';
        //write a sql query that gets all links to this object
        $sqlSelect = "SELECT `entity`.`module`, `entity`.`url`
                      FROM `entity_link`
                      JOIN `entity` ON `entity_link`.`$relationship` = `entity`.`id`
                      WHERE `entity_link`.`$objectRelationship` = {$this->id}
                      AND `entity`.`status` = 'A'
                      ORDER BY `entity_link`.`order`, `entity`.`created` ASC";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //get the result
        $result = $database->fetchResult($sqlQuery);
        //check if anything was found
        if ($result != false && is_array($result)) {
            //set an empty return array variable
            $returnArray = array();
            //go through the rows
            foreach ($result as $resultRow) {
                //add the result to the return array
                $returnArray[$resultRow['module']][] = $resultRow['url'];
            }
            //return the return array
            return $returnArray;
        }
        //return default false
        return false;
    }
    
    /**
    * Loads the settings from the `entity_settings` table
    * 
    * @param Connection $database The database connection object
    * @param string $module The module that is being loaded
    * */
    protected function getForm(Connection $database, $module)
    {
        //set up the Form object
        $form = EntityFactory::build('form');
        //get the url of the Form
        $formUrl = $form->getUrl($database, $module);
        //chech if there is a Form for this page
        if ($formUrl != null) {
            //load the Form
            $form->load($formUrl, $database, false);
        }
        //return the Form
        return $form;
    }
    
    /**
    * Creates a new Entity
    * 
    * @param array $post A key-value pair array containing the MySql column name as the key and the value as the value
    * @param Form $form The form object containing information about the type(s)
    * @param Connection $database The connection to the database
    * 
    * @return boolean Whether the create was successful or not
    * */
    public function create(Array $post, Form $form, Connection $database)
    {
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //set the form
        $this->form = $form;
        //generate a url value for this Entity
        $url = $this->getNewUrl($post[strtolower(preg_replace('/\B([A-Z])/', '_$1', $this->form->urlColumn))], $database);
        //get the query for adding an entry to the `entity` table
        $query = $formQuery->createEntity($this->module, $url, $form->defaultStatus);
        //run the first query (into `entity`)
        $firstQuery = $database->runQuery($query);
        //check if the first query was successfull
        if ($firstQuery !== false) {
            //get the last inserted id
            $lastId = mysql_insert_id($database->connection);
            //get the query for adding an entry into the {$this->module} table
            $queryTable = $formQuery->create($this->module, $post, $form, $database);
            //check if any links need to be inserted
            if (isset($form->children['form_link']) && is_array($form->children['form_link'])) {
                //set the variable that will hold a list of links to create
                $allLinks = array('child' => array(), 'parent' => array());
                //go through the form links
                foreach ($form->children['form_link'] as $formLinkUrl) {
                    //get the Form Link as an object
                    $formLink = EntityFactory::build('form_link');
                    //load the Form Link
                    $formLink->load($formLinkUrl, $database);
                    //check if any of this type has been selected/created
                    if (isset($post[$formLink->include]) && is_array($post[$formLink->include])) {
                        //go through each of the post variables
                        foreach ($post[$formLink->include] as $includeId) {
                            //add this entry into an array
                            $allLinks[$formLink->relationship][] = $includeId;
                        }
                    }
                }
                //check if there are any child links
                if (!empty($allLinks['child'])) {
                    //get the query for adding an entry/entries into the `entity_link` table
                    $queryLinkChild = $formQuery->addLinks($allLinks['child'], $lastId, 'child');
                }
                //check if there are any parent links
                if (!empty($allLinks['parent'])) {
                    //get the query for adding an entry/entries into the `entity_link` table
                    $queryLinkParent = $formQuery->addLinks($allLinks['parent'], $lastId, 'parent');
                }
            }
            //run the second query ($module)
            $database->runQuery($queryTable);
            //see if the third query can be ran
            if (isset($queryLinkChild)) {
                //run the third query (`entity_link`)
                $database->runQuery($queryLinkChild);
            }
            //see if the third query can be ran
            if (isset($queryLinkParent)) {
                //run the third query (`entity_link`)
                $database->runQuery($queryLinkParent);
            }
            //reload this Entity
            $this->load($url, $database);
            //return a true (success)
            return true;
        }
        //oops, we couldn't find an id, the first query failed
        return false;
    }
    
    
    /**
    * Updates an Entity
    * 
    * @param array $post A key-value pair array containing the MySql column name as the key and the value as the value
    * @param Entity $entity The Entity being updated
    * @param Connection $database The connection to the database
    * 
    * @return boolean Whether the update was successful or not
    * */
    public function update(Array $post, Entity $entity, Connection $database)
    {
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //get a query to update an Entity
        $updateQuery = $formQuery->update($entity->module, $post, $entity, $database);
        //get a query to update the last modified date in the `entity` table
        $updateModifiedQuery = $formQuery->updateModified($entity->url);
        //set an empty array that will hold the list of Form Link Queries
        $formLinkQueries = array();
        //see if there are any Form Links linked to this Entity
        if (isset($entity->form->children['form_link']) && is_array($entity->form->children['form_link'])) {
            //go through the list of Form Links
            foreach ($entity->form->children['form_link'] as $formLinkUrl) {
                //get the Form Link as an object
                $formLink = EntityFactory::build('form_link');
                //load the Form Link
                $formLink->load($formLinkUrl, $database);
                //get the relationship of the Links
                $relationship = ($formLink->relationship == 'child') ? 'children' : 'parents';
                //set up an empty array to hold the "previous links"
                $previousLinks = array();
                //check if any of these type of items are currently linked to this Entity
                if (isset($entity->{$relationship}[$formLink->include]) && is_array($entity->{$relationship}[$formLink->include])) {
                    //go through each of the links
                    foreach ($entity->{$relationship}[$formLink->include] as $includeUrl) {
                        //get the include as an object
                        $includeObject = EntityFactory::build($formLink->include);
                        //load the object
                        $includeObject->load($includeUrl, $database);
                        //add this item's ID to the array
                        $previousLinks[] = $includeObject->id;
                    }
                }
                //get the "current links" (The links being updated to)
                $currentLinks = (isset($post[$formLink->include]) && is_array($post[$formLink->include])) ? $post[$formLink->include] : array();
                //get a list of MySql queries to alter these links
                //$linkQueries = $formQuery->updateLinks($entity, $previousLinks, $currentLinks, $formLink->relationship);
                
                
                //check if any previous links exist
                if (count($previousLinks) > 0) {
                    //get a query to delete all these links
                    $linkDeleteQuery = $formQuery->deleteLinks($previousLinks, $entity->id, $formLink->relationship);
                    //add this query to the list of queries
                    $formLinkQueries[] = $linkDeleteQuery;
                }
                //check if any current links exist
                if (count($currentLinks) > 0) {
                    //get a query to add all the new links (in order)
                    $linkAddQuery = $formQuery->addLinks($currentLinks, $entity->id, $formLink->relationship);
                    //add this query to the list of queries
                    $formLinkQueries[] = $linkAddQuery;
                }
                
                
                
                
                //see if any queries were returned
                /*if ($linkQueries != false && is_array($linkQueries)) {
                    //merge this list with the list of Form Link queries
                    $formLinkQueries = array_merge($formLinkQueries, $linkQueries);
                }*/
                //echo '<pre>',print_r($formLinkQueries),'</pre>';
            }
        }
        //check if the update query had any changes
        if ($updateQuery != null) {
            //run the update query
            $database->runQuery($updateQuery);
        }
        //check if there are any Form Link queries
        if (count($formLinkQueries) > 0) {
            //go through all the Form Link queries
            foreach ($formLinkQueries as $formLinkQuery) {
                //run the Form Link query
                $database->runQuery($formLinkQuery);
            }
        }
        //check if any changes were made
        if ($updateQuery != null && count($formLinkQueries) > 0) {
            //run the update modified query
            $database->runQuery($updateModifiedQuery);
        }
        //reload this Entity
        $this->load($this->url, $database);
    }
    
    
    /**
    * Deletes an Entity
    * 
    * @param Connection $database The database connection
    * */
    public function delete($database)
    {
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //get a query to delete an Entity
        $query = $formQuery->delete($this->url);
        //run the query
        $firstQuery = $database->runQuery($query);
        //check if the first query was successfull
        return ($firstQuery !== false) ? true : false;
    }
    
    /**
    * Permanently deletes an Entity
    * Also deletes all links to this Entity
    * 
    * @param Connection $database The database connection
    * */
    public function permanentlyDelete($database)
    {
        //delete all entries from the `entity_link` table
        $sqlDelete[] = "DELETE `entity_link`.*
                        FROM `entity_link`
                        WHERE `parent` = {$this->id}
                        OR `child` = {$this->id}";
        //delete the entry from this table
        $sqlDelete[] = "DELETE `{$this->module}`.*
                        FROM `{$this->module}`
                        WHERE `id` = {$this->id}";
        //delete the entry from the `entity` table
        $sqlDelete[] = "DELETE `entity`.*
                        FROM `entity`
                        WHERE `id` = {$this->id}";
        //go through all the delete queries
        foreach ($sqlDelete as $deleteQuery) {
            //run the query
            $database->runQuery($deleteQuery);
        }
        //echo '<pre>',print_r($sqlDelete),'</pre>';
        return true;
    }
    
    /**
    * Restores an Entity
    * */
    public function restore($database)
    {
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //get a query to delete an Entity
        $query = $formQuery->restore($this->url);
        //run the query
        $firstQuery = $database->runQuery($query);
        //check if the first query was successfull
        return ($firstQuery !== false) ? true : false;
    }
    
    /**
    * Returns an array of all matching Entities' urls
    * 
    * @param Connection $database The database connection
    * @param string $module The type of Entities to find
    * @param string|array The status(ses) to match
    * 
    * @return array A list of metching Entity urls
    * */
    public static function listAll(Connection $database, $module, $status = 'A')
    {
        //write a query that will find all Entity ids matching the module
        $sqlSelect = "SELECT DISTINCT(`url`)
                      FROM `entity`
                      WHERE `module` = '$module'
                      AND `status` = '$status'
                      ORDER BY `entity`.`created`";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //return the result of the query
        return $database->fetchResult($sqlQuery);
    }
    
    /**
    * Returns a single column value of an entity
    * 
    * @param Connection $database The database connection
    * @param string $url The url of the entity where a single value must be read from
    * @param string $field The field to be found
    * 
    * @param string The value found
    * */
    public static function getSingleFieldValue(Connection $database, $url, $field)
    {
        //write a query to get the field's value
        $sqlSelect = "SELECT `$field`
                      FROM `entity`
                      WHERE `url` = '$url'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //get the result of the query
        $result = $database->fetchResult($sqlQuery);
        //return the first result
        return ($result == false) ? $result : $result[0];
    }
    
    /**
    * Returns what type of Entity this is
    * 
    * @param Connection $database The database connection
    * @param string $url The url of the Entity
    * 
    * @return string The type of Entity that this is
    * */
    public static function getType(Connection $database, $url)
    {
        //write a query to get the type
        $sqlSelect = "SELECT `module`
                      FROM `entity`
                      WHERE `url` = '$url'";
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //get the result of the query
        $result = $database->fetchResult($sqlQuery);
        //return the first result
        return ($result == false) ? $result : $result[0];
    }
    
    /**
    * Generates a new url for an Entity
    * 
    * @param string $urlColumnValue The value that is inserted in the url column of the Entity
    * @param Connection $database The database connection
    * */
    private function getNewUrl($urlColumnValue, Connection $database)
    {
        //set a counter to increment
        $count = 0;
        //set a flag to see if we have found a unique url
        $correct = false;
        //start the increment loop
        while($correct == false){
            //replace all spaces with underscores
            $urlColumnValue = str_replace(' ','_',$urlColumnValue);
            //replace all non alphanumeric characters (including udnerscores) with nothing and add the counter
            $url = strtolower(preg_replace('/[^a-zA-Z0-9_]/','',$urlColumnValue)) . (($count == 0) ? '' : "_{$count}");
            //write a query to see if the url exists
            $sqlSelect = "SELECT `url` FROM `entity` WHERE `url` = '{$url}'";
            //ru the query
            $sqlQuery = $database->runQuery($sqlSelect);
            //see if a url was returned
            if($sqlQuery != false){
                //this url already exists, try again
                $count++;
            }
            else{
                //a suitable url was found
                $correct = true;
            }
        }
        //return the url
        return $url;
    }
    
    /**
    * Builds an unordered list of linked Entities and their children
    * 
    * @param Entity $currentEntity The Entity to start from
    * @param Connection $database The database connection
    * @param System $system The system variable
    * @param boolean $checkbox Whether a checkbox should be shown per link
    * @param integer $level How many levels deep the recursion has gone
    * */
    public function getLinkedEntitiesList(Entity $currentEntity, Connection $database, System $system, $checkbox = true, $level = 0)
    {
        //build a return string that must be returned
        $returnString = '<ul class="linked_items">';
        //a link where the current Entity links to
        $currentLink = System::buildUrl($system->templateView, $system->admin, $currentEntity->module, 'view', null, $currentEntity->url);
        //a checkbox to show on the list item
        $checkboxField = ($this === $currentEntity || $checkbox === false) ? null : "<input type=\"checkbox\" name=\"linked_entity[]\" value=\"{$currentEntity->url}\" />";
        //see if this Form has any images linked to it
        if (isset($currentEntity->form->children['image']) && is_array($currentEntity->form->children['image'])) {
            //create a new Image object
            $image = EntityFactory::build('image');
            //load the Image
            $image->load($currentEntity->form->children['image'][0], $database);
            //display the Image
            $imageElement = $image->display(10, 10);
        } else {
            //no Image was found
            $imageElement = null;
        }
        //add this item to the return string
        $returnString .= '<li>' . $imageElement . $checkboxField . '<a href="' . $currentLink . '" target="_blank" title="{$currentEntity->form->name}">' . $currentEntity->{$currentEntity->form->urlColumn} . ' <i>(' . $currentEntity->form->name . ')</i></a>';
        
        //get all Entities linked to the current one
        $allLinkedEntities = $this->getLinkedEntities($currentEntity, $database);
        //see if any Entities are linked to the current one
        if ($allLinkedEntities !== false && is_array($allLinkedEntities) && $level < 3) {
            //go through each linked Entity
            foreach ($allLinkedEntities as $linkedEntity) {
                //build a list for this linked Entity
                $returnString .= $this->getLinkedEntitiesList($linkedEntity, $database, $system, $checkbox, ($level + 1));
            }
        }
        //add a closing tag for the current item
        $returnString .= '</li>';
        //add the closing ul tag to the return string
        $returnString .= '</ul>';
        //return the string that was built
        return $returnString;
    }
    
    /**
    * Builds an array of Entities that are linked to the selected one
    * 
    * @param Entity $currentEntity The entity to get links for
    * @param Connection $database The database connection
    * */
    private function getLinkedEntities(Entity $currentEntity, Connection $database)
    {
        //set up an empty array to be filled in and returned
        $returnArray = array();
        //check if this Entity's Form has any Form Link Entites linked to it
        if (isset($currentEntity->form->children['form_link']) && is_array($currentEntity->form->children['form_link'])) {
            //go through each Form Link
            foreach ($currentEntity->form->children['form_link'] as $formLinkUrl) {
                //build a new Form Link object
                $formLink = EntityFactory::build('form_link');
                //load the Form Link
                $formLink->load($formLinkUrl, $database);
                //get the relationship as a string
                $relationship = ($formLink->relationship === 'child') ? 'children' : 'parents';
                //check if there are any of these Form Links linked to the current Entity
                if (isset($currentEntity->{$relationship}[$formLink->include]) && is_array($currentEntity->{$relationship}[$formLink->include])) {
                    //go through each linked Entity and load it
                    foreach ($currentEntity->{$relationship}[$formLink->include] as $includedEntityUrl) {
                        //build a new object for this linked Entity
                        $linkedEntity = EntityFactory::build($formLink->include);
                        //load the linked Entity
                        $linkedEntity->load($includedEntityUrl, $database);
                        //add the linked Entity to the reurn array
                        $returnArray[] = $linkedEntity;
                    }
                }
            }
        }
        //return the return array or false
        return (count($returnArray) > 0) ? $returnArray : false;
    }
    
    
    /**
    * Adds a link to the specified Entity
    * 
    * @param integer $link The ID of the Entity to link
    * @param string $relationship The ralationship the link has to the Entity (cihld|parent)
    * @param Connection $database The database connection
    * */
    public function addLink ($link, $relationship, $database) {
        //Create a new Form Query object
        $formQuery = new FormQuery();
        //Get a query to add this link
        $linkQuery = $formQuery->addLinks(array($link), $this->id, $relationship);
        //run the query
        $database->runQuery($linkQuery);
    }
    
    /**
    * Transforms the object into an array
    * 
    * @param Connection $database The database connection
    * 
    * @return array This object in array form
    * */
    public function toArray ($database)
    {
        //this object converted directly into an array
        $returnArray = (array)$this;
        //see if this object has any children
        if (isset($this->children) && is_array($this->children)) {
            //go through each child type
            foreach ($this->children as $key=>$value) {
                //go through each of the urls of this type
                foreach ($value as $newObjectUrl) {
                    //create a new object of the correct type
                    $newObject = EntityFactory::build($key);
                    //load the new object
                    $newObject->load($newObjectUrl, $database);
                    //set this value directly to the return array
                    $returnArray[$key][] = $newObject->id;
                }
            }
            //remove children from the return array
            unset($returnArray['children']);
        }
        //see if this object has any parents
        if (isset($this->parents) && is_array($this->parents)) {
            //go through each parent type
            foreach ($this->parents as $key=>$value) {
                //go through each of the urls of this type
                foreach ($value as $newObjectUrl) {
                    //create a new object of the correct type
                    $newObject = EntityFactory::build($key);
                    //load the new object
                    $newObject->load($newObjectUrl, $database);
                    //set this value directly to the return array
                    $returnArray[$key][] = $newObject->id;
                }
            }
            //remove children from the return array
            unset($returnArray['parents']);
        }
        //go through each array value to remove any camel casing
        foreach ($returnArray as $key=>$value) {
            //un-camel-case the key
            $newKey = preg_replace('/(?<!^)([A-Z])/', '_\\1', $key);
            //check if the new and old keys match
            if ($newKey !== $key) {
                //remove $key from the array
                unset($returnArray[$key]);
                //add the new key to the array
                $returnArray[strtolower($newKey)] = $value;
            }
        }
        //return the resulting array
        return $returnArray;
    }
    
    
    /**
    * The __toString method will print the class to screen
    * */
    public function __toString()
    {
        //print the current object
        return '<pre>' . print_r($this, true) . '</pre>';
    }
    
}
?>