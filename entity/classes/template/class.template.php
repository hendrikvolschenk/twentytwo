<?php
/**
* Loads the correct template file and loads the page variables such as the content and title
* */
class Template
{

    /**
    * Hold the page title to be displayed in the title bar | <title></title>
    *
    * @var string
    * */
    protected $title = null;

    /**
    * Holds the content to be shown on the page
    *
    * @var string
    * */
    public $content = null;

    /**
    * Holds the javascript to be added to the page
    *
    * @var string
    * */
    public $javascript = null;

    /**
    * The loadContent method loads content with the respective module, action and url in mind
    *
    * @param System $system The system class (to be sent to the content file)
    * @param Connection $database The database connection (to be sent to the content file)
    * @param Entity $entity The current entity (to be sent to the content file)
    * @param Array $entities A list of Entities' urls
    * */
    public function loadContent(System $system, Connection $database, Entity $entity, $entities = null)
    {
        //set a template variable
        $template = $this;

        //echo 'HHH<pre>',print_r(get_defined_vars()),'</pre>';

        //see if the POST page is being called
        $post = ($system->templateView == '!post') ? 'post/' : '';
        //see if multiple Entities are being shown
        $multiple = (strpos($system->url, ',') === false) ? '' : 'multiple/';
        //set the content file for if all the folders exist
        $contentFile = dirname(__FILE__) . "/../../modules/{$system->module}/{$system->action}/{$system->view}/{$system->url}/{$post}index.php";
        //set the content file for if the view folder doesn't exist for the specified url
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/{$system->module}/{$system->action}/{$system->view}/{$multiple}{$post}index.php";
        //set the content file for if the view folder doesn't exist for the specified module
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/entity/{$system->action}/{$system->view}/{$system->url}/{$multiple}{$post}index.php";
        //set the content file for if the view folder doesn't exist for the specified module and url
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/entity/{$system->action}/{$system->view}/{$multiple}{$post}index.php";
        //set the content file for if the view folder doesn't exist
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/{$system->module}/{$system->action}/{$system->url}/{$multiple}{$post}index.php";
        //set the content file for if the module folder doesn't exist
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/entity/{$system->action}/{$system->url}/{$multiple}{$post}index.php";
        //set the content file for if the url folder doesn't exist
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/{$system->module}/{$system->action}/{$multiple}{$post}index.php";
        //set the content file for if both the module and url folders don't exist
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/entity/{$system->action}/{$multiple}{$post}index.php";

        //set the content file for if both the module and url and action folders don't exist
        $contentFile = (file_exists($contentFile) && strpos($contentFile, '//') === false) ? $contentFile : dirname(__FILE__) . "/../../modules/entity/all/{$multiple}{$post}index.php";

        //start an output buffer to catch the inclusion of the content
        ob_start();

            //a check to see if this user is trying to update/view him/her-self
            if ($system->module === 'user' && ($system->action === 'update' || $system->action === 'view') && ($entity->id === $system->user->id)) {
                //the user has access anyway
                $hasAccessOverride = true;
            } else {
                //no access
                $hasAccessOverride = false;
            }

            if ($system->user->hasAccess($system->formAction) === false && $system->templateView != '!post' && $hasAccessOverride === false) {
                //output an error message
                echo '<div class="error">The page you want to view is off-limits, naughty naughty!</div>';
            }


            //see if this status is allowed
            if ((!empty($system->formAction->disallowedStatusses) && strstr($system->formAction->disallowedStatusses, $entity->status)) ||
                (!empty($system->formAction->allowedStatusses) && !strstr($system->formAction->allowedStatusses, $entity->status))) {
                //output an error message
                echo "<div class=\"error\">You cannot {$system->formAction->action} " . functionsString::addIndefiniteArticle($entity->form->name) . " of this status.</div>";
            }

            //see if the user has access to this page
            if ($system->user->hasAccess($system->formAction) === true || $hasAccessOverride === true) {
                //check if the content file exists
                if(file_exists($contentFile)){
                    //include the content file
                    require $contentFile;
                }
                else{
                    //output an error message
                    echo '<div class="error">The page you have requested cannot be found.</div>';
                }
            }

            //get the contents instead of doing an include
            $content = ob_get_contents();
        //stop the output buffer
        ob_end_clean();
        //set the content
        $this->content .= $content;
    }




    /**
    * Loads a module's content ready to be displayed on the page
    *
    * @param System $system The system class
    * @param Entity $entity The current page's Entity
    * @param Connection $database The datbase connection
    * @param string $module The module to be included
    * @param string $action The action of $module to be included
    * @param string $view The $action's view to include
    * @param string $url The url of the page to include
    * @param string $context Whether to load the module in it's own context or in the context of the current page (own|this)
    * @param boolean $allowAccess When using "this" context, whether to check if the user has permissions or not
    * */
    public function loadModule(System $system, Entity $entity, Connection $database, $module, $action, $view = null, $url = null, $context='own', $allowAccess=false)
    {
        //If this page must load within it's own context
        if ($context === 'own') {
            $urlView = ($view === null) ? '' : "/$view";
            $urlUrl = ($url === null) ? '' : "/$url";
            $webUrl = 'http://' . $_SERVER['HTTP_HOST'] . "/!/$module/$action$urlView$urlUrl";
            $opts = array(
                        'http'=>array(
                            'method'=>"GET",
                            'header'=>"Accept-language: en\r\n" .
                            "Cookie: " . session_name() . "=" . session_id() . "\r\n"
                        )
                    );

            $context = stream_context_create($opts);
            session_write_close();   // this is the key
            return $this->getModuleFiles('styles', $module, $action, $view, $url) .
                   $this->getModuleFiles('javascript', $module, $action, $view, $url) .
                   file_get_contents($webUrl, false, $context);
        } elseif ($context === 'this') {
            //the file that needs to be included
            $file = dirname(__FILE__) . "/../../modules/{$module}/{$action}/";
            //add the view part of the file
            $file .= ($view === null) ? '' : "{$view}/";
            //add the url part of the file
            $file .= ($url === null) ? '' : "{$url}/";
            //add the "index.php" part to the file
            $file .= 'index.php';
            //see if the file exists and if not change the module
            $file = (file_exists($file)) ? $file : str_replace("/{$module}/", '/entity/', $file);
            //see if the file exists and if not change the url
            $file = (file_exists($file)) ? $file : str_replace("{$url}/", '', $file);
            //see if the file exists
            if (file_exists($file)) {
                //get the url of the Form Action
                $formActionUrl = FormAction::getUrl($database, $module, $action);
                //get a new Form Action object
                $formAction = EntityFactory::build('form_action');
                //load the Form Action
                $formAction->load($formActionUrl, $database);
                //see if user has access to this
                if ($system->user->hasAccess($formAction) === true || $allowAccess === true) {
                    //start an output buffer
                    ob_start();
                        //include the required file
                        require($file);
                        //get the contents of the output buffer
                        $fileContents = ob_get_contents();
                    //stop the output buffer
                    ob_end_clean();
                    //return the file contents
                    return $this->getModuleFiles('styles', $module, $action, $view, $url) .
                    $this->getModuleFiles('javascript', $module, $action, $view, $url) .
                    $fileContents;
                } else {
                    //the user does not have access
                    return false;
                }
            } else {
                //the file does not exist
                return false;
            }

        }
    }





    /**
    * The loadTemplate method loads the template file from disk
    *
    * @param Connection $database The database connection
    * @param System $system The system class
    * @param Entity $entity The page entity
    * */
    public function loadTemplate(Connection $database, System $system, Entity $entity)
    {
        //make the title available on the template page
        $title = $this->title;
        //make the contant available on the template page
        $content = $this->content;
        //make the javascript variable available on the template page
        $javascript = $this->javascript;
        //make the styles variable available on the template page
        $styles = $this->styles;
        //add a reference to the template class
        $template = $this;
        //get the path of the template file
        $templateFile = dirname(__FILE__) . "/../../templates/{$system->settings['APPLICATION_TEMPLATE']}/{$system->templateView}/index.php";
        //see if the path above exists, otherwise remove the view
        $templateFile = (file_exists($templateFile)) ? $templateFile : dirname(__FILE__) . "/../../templates/{$system->settings['APPLICATION_TEMPLATE']}/index.php";
        //check if the template file exists
        if (file_exists($templateFile)) {
            //include the file
            require_once($templateFile);
        } else {
            //throw an Exception about the missing file
            throw new Exception("The '{$system->settings['APPLICATION_TEMPLATE']}' template could not be found.");
        }
    }

    /**
    * Gets the <title> of the current page
    *
    * @param string $module The module that is being used on this page
    * @param string $action The action that is being performed on this page
    * @param string $url The page url
    * @param string $name The name of the Entity that is being shown on this page
    * @param string $applicationName The name of the application
    * */
    public function getTitle($module, $action, $url, $name, $applicationName)
    {
        //set-up an empty title variable
        $title = '';




        /*if (!empty($url)) {
            if ($action === 'view') {
                $title .= $name;
            }
        }*/




        //check if the url was set
        if (empty($url)) {
            //add the action and module to the title
            $title .= FunctionsString::removeUnderscores($action) . ' ' . FunctionsString::removeUnderscores($module);
        } else {
            //check if the url is 'all'
            if ($url == 'all') {
                // check if the action is view
                if ($action === 'view') {
                    // the title is the partial title
                    $title .= 'All ' . strtolower(FunctionsString::getPlural(FunctionsString::removeUnderscores($module)));
                } else {
                    //add the action, "all" and the module to the title
                    $title .= FunctionsString::removeUnderscores($action) . ' ' . strtolower(FunctionsString::addIndefiniteArticle(FunctionsString::removeUnderscores($module)));
                }
            } elseif (strpos($url, ',') !== false) {
                $title .= FunctionsString::removeUnderscores($action) . ' multiple ' . strtolower(FunctionsString::getPlural(FunctionsString::removeUnderscores($module)));
            }else {
                // check if we are viewing
                if ($action === 'view') {
                    // set the title
                    $title .= $name;
                } else {
                    //add the name, action and module to the title
                    $title .= FunctionsString::removeUnderscores($action) . ' ' . strtolower(FunctionsString::removeUnderscores($module)) . ' ' . $name;
                }
            }
        }
        //add the website's name to the title
        $title .= " | $applicationName";
        //set the title variable of the class equal to the one just created
        $this->title = $title;
    }

    /**
    * Get the files that need to be included for a single module
    *
    * @param string $type The type of file being included
    * @param string $module The module being included
    * @param string $action The action being performed on the module
    * @param string $view The view applied to the action
    * @param string $url The url of the specific Entity instance
    *
    * @return string A list of files
    * */
    private function getModuleFiles($type, $module, $action, $view = null, $url = null)
    {
        //set up an empty files variable to be returned
        $files = null;
        //set up a list of folders to check
        $folders = array(
            "entity/modules/{$module}/{$type}/",
            "entity/modules/entity/{$action}/{$type}/",
            "entity/modules/{$module}/{$action}/{$type}/",
            "entity/modules/entity/{$action}/{$url}/{$type}/",
            "entity/modules/{$module}/{$action}/{$url}/{$type}/",
            "entity/modules/entity/{$action}/{$view}/{$type}/",
            "entity/modules/{$module}/{$action}/{$view}/{$type}/",
            "entity/modules/entity/{$action}/{$view}/{$url}/{$type}/",
            "entity/modules/{$module}/{$action}/{$view}/{$url}/{$type}/"
        );
        //go through each of the folders
        foreach ($folders as $folder) {
            //add all files in this folder to the return string
            $files .= $this->getFilesInFolder($folder, $type);
        }
        //set the object variable for this type
        return $files;
    }

    /**
    * Get the files that need to be included on this page
    *
    * @param System $system The system object
    * @param string $type The type of files we are looking for (styles | javasvcript)
    *
    * @return string The files to be included
    * */
    public function getFiles($system, $type)
    {
        //set up an empty files variable to be returned
        $files = null;
        //set up a list of folders to check
        $folders = array(
            "entity/{$type}/",
            "entity/modules/entity/{$type}/",
            "entity/modules/{$system->module}/{$type}/",
            "entity/modules/entity/{$system->action}/{$type}/",
            "entity/modules/{$system->module}/{$system->action}/{$type}/",
            "entity/modules/entity/{$system->action}/{$system->url}/{$type}/",
            "entity/modules/{$system->module}/{$system->action}/{$system->url}/{$type}/",
            "entity/modules/entity/{$system->action}/{$system->view}/{$type}/",
            "entity/modules/{$system->module}/{$system->action}/{$system->view}/{$type}/",
            "entity/modules/entity/{$system->action}/{$system->url}/{$system->view}/{$type}/",
            "entity/modules/{$system->module}/{$system->action}/{$system->url}/{$system->view}/{$type}/",
            "entity/templates/{$system->settings['APPLICATION_TEMPLATE']}/{$type}/",
            "entity/templates/{$system->settings['APPLICATION_TEMPLATE']}/{$system->templateView}/{$type}/"
        );
        //go through each of the folders
        foreach ($folders as $folder) {
            //add all files in this folder to the return string
            $files .= $this->getFilesInFolder($folder, $type);
        }
        //set the object variable for this type
        $this->$type = $files;
    }

    /**
    * A helper function to recurse through a certain folder and find all files
    *
    * @param string $folder The path to the folder
    * @param string $type The type of files we are looking for (styles | javasvcript)
    * @param string currentString The current string to be returned
    * */
    private function getFilesInFolder($folder, $type, $currentString = '')
    {
        //check whether this is a valid folder
        if (strpos($folder, '//') == false && is_dir($folder)) {
            //scan this directory for files/folders
            $scandir = scandir($folder);
            //check whether any results were found
            if ($scandir != false) {
                //go through each file/folder
                foreach ($scandir as $file) {
                    //remove the first two files/folders (. and ..)
                    if ($file != '.' && $file != '..') {
                        //check whether this is a file
                        if (!is_dir($folder . $file)) {
                            //check whether this is javascript or styles
                            if ($type === 'javascript') {
                                // see that the file is indeed a javascript file
                                if (preg_match('/^.*\.js$/', $file)) {
                                    //add this file to the current string
                                    $currentString .= '<script src="/' . $folder . $file . '"></script>' . "\n";
                                }
                            } else {
                                // see that this file is a stylesheet
                                if (preg_match('/^.*\.css$/', $file)) {
                                    //add this file to the current string
                                    $currentString .= '<link rel="stylesheet" type="text/css" href="/' . $folder . $file . '" />' . "\n";
                                }
                            }
                        //this is a folder
                        } else {
                            //go through the folder and add what is in there
                            $currentString = $this->getFilesInFolder("{$folder}{$file}/", $type, $currentString);
                        }
                    }
                }
            }
        }
        //always return the current string
        return $currentString;
    }

    public function __toString()
    {

        return '<pre>' . print_r($this, true) . '</pre>';
    }

}