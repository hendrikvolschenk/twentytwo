<?php
/**
* An Class for building queries
* The queries are built to either create, update, delete or restore an Entity or an array of Entities
* */
class formQuery
{
    
    /**
    * Creates a query to check whether user credentials that were entered are correct
    * 
    * @param string $emailAddress The email address of the user
    * @param string $password The password that was entered
    * 
    * @return string
    * */
    public function login($emailAddress, $password)
    {
        //build the query
        $sqlQuery = "SELECT `entity`.`url` FROM `entity`
                     JOIN `user` ON `entity`.`id` = `user`.`id`
                     WHERE `user`.`email_address` = '$emailAddress'
                     AND `user`.`password` = '$password'
                     AND `entity`.`status` = 'A'
                     LIMIT 1";
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Creates a query to insert data into the `entity` table
    * 
    * @param string $module The type of Entity that this is
    * @param string $url The url created for this Entity
    * @param string $status The status of this Entity
    * 
    * @return string
    * */
    public function createEntity($module, $url, $status)
    {
        //build the query to be returned
        $sqlQuery = "INSERT INTO `entity`
                     (`id`,`url`,`module`,`created`,`modified`,`status`)
                     VALUES (NULL, '$url', '$module', NOW(), NULL, '$status')";
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Creates a query to create an Entity
    * 
    * @param string $module The type of entity being created, also the database table to insert into
    * @param array $input An array of key-value field-names and -values corresponding with the database table given in $module
    * @param Form $form The Form object containing information on the form on the page, including the form groups
    * @param Connection $database The database connection
    * 
    * @return string
    * */
    public function create($module, $input, Form $form, Connection $database)
    {
        //check if the Form has any Form Groups
        if (isset($form->children['form_group']) && is_array($form->children['form_group'])) {
            //set up a fields variable that will hold the fieldnames
            $fields = '`id`, ';
            //set up a values variable that will hold the values of the fields
            $values = 'LAST_INSERT_ID(), ';
            //go through the Form Groups
            foreach ($form->children['form_group'] as $formGroupUrl) {
                //get the Form Group as an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl, $database);
                //see if the Form Group has any Form Rows
                if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])) {
                    //go through each Form Row
                    foreach ($formGroup->children['form_row'] as $formRowUrl) {
                        //get the Form Row as an object
                        $formRow = EntityFactory::build('form_row');
                        //load the Form Row
                        $formRow->load($formRowUrl, $database);
                        //add the field to the field list
                        $fields .= "`{$formRow->field}`, ";
                        //add the value to the field list
                        $values .= (!isset($input[$formRow->field]) || (empty($input[$formRow->field]) && $input[$formRow->field] != 0)) ? 'NULL, ' : '\'' . mysql_real_escape_string($input[$formRow->field]) . '\', ';
                    }
                } else {
                    //the form groups above have no rows linked to them, throw an error
                    throw new Exception("The form group for $module > {$formGroup->title} has no rows");
                }
            }
            //remove the last space and comma
            $fields = substr($fields, 0, -2);
            $values = substr($values, 0, -2);
            //build the query
            $sqlQuery = "INSERT INTO `$module` ($fields) VALUES($values)";
            //return the query
            return $sqlQuery;
        }
        //the form has no groups linked to it, throw an error (nothing was returned by the code above)
        throw new Exception("The form for $module has no form groups linked to it");
    }
    
    /**
    * Creates a query to update an entity
    * 
    * @param string $module The type of entity being created, also the database table to insert into
    * @param array $input An array of key-value field-names and -values corresponding with the database table given in $module
    * @param Entity $entity The Entity object being updated, either for fields not set in $input or for logging
    * @param Connection $database The database connection
    * 
    * @return string
    * */
    public function update($module, $input, Entity $entity, Connection $database)
    {
        //check if the Form has any Form Groups
        if (isset($entity->form->children['form_group']) && is_array($entity->form->children['form_group'])) {
            //set up a fields variable that will hold the fieldnames and values being updated
            $fields = '';
            //go through the Form Groups
            foreach ($entity->form->children['form_group'] as $formGroupUrl) {
                //get the Form Group as an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl, $database);
                //see if the Form Group has any Form Rows
                if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])) {
                    foreach ($formGroup->children['form_row'] as $formRowUrl) {
                        //get the Form Row as an object
                        $formRow = EntityFactory::build('form_row');
                        //load the Form Row
                        $formRow->load($formRowUrl, $database);
                        //get the fieldname as a variable
                        $fieldName = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $formRow->field))));
                        //check if the value has changed
                        if ($input[$formRow->field] != $entity->$fieldName) {
                            //add the update to the fields variable
                            $fields .= "`{$formRow->field}` = '" . mysql_real_escape_string($input[$formRow->field]) . '\', ';
                        }
                    }
                } else {
                    //the form groups above have no rows linked to them, throw an error
                    throw new Exception("The form for $module has form groups linked to it, but no rows in those groups");
                }
            }
            //remove the last comma from the fields
            $fields = substr($fields, 0, -2);
            //build the query
            $sqlQuery = "UPDATE `$module` SET $fields WHERE `id` = {$entity->id}";
            //return the query
            return ($fields == '') ? null : $sqlQuery;
        }
        //the form has no groups linked to it, throw an error (nothing was returned by the code above)
        throw new Exception("The form for $module has no form groups linked to it");
    }
    
    /**
    * Creates a query to delete an Entity
    * 
    * @param string $url The url of the Entity in the entity table
    * 
    * @return string
    * */
    public function delete($url)
    {
        //create and return a delete query
        return $this->changeStatus($url, 'D');
    }
    
    /**
    * Creates a query to restore an Entity
    * 
    * @param string $url The url of the Entity in the entity table
    * 
    * @return string
    * */
    public function restore($url)
    {
        //create and return a restore query
        return $this->changeStatus($url, 'A');
    }
    
    /**
    * Creates a query to change an Entity's status
    * 
    * @param string $url The url of the Entity in the entity table
    * @param string $status The new status of the Entity
    * 
    * @return string
    * */
    public function changeStatus($url, $status)
    {
        //build a query that changes the status
        $sqlQuery = "UPDATE `entity` SET `status` = '$status' WHERE `url` = '$url'";
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Creates a query to change the last modified date
    * 
    * @param string $url The url of the Entity in the entity table
    * 
    * @return string
    * */
    public function updateModified($url)
    {
        //build the query
        $sqlQuery = "UPDATE `entity` SET `modified` = NOW() WHERE `url` = '$url'";
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Creates a query to create a list of Entities
    * 
    * @param string $module The type of Entities being created
    * @param array $list An array of arrays of key-value pairs where the key si the field-name and the value is the input value
    * @param Form $form The Form object containing information on the form on the page, including the form groups
    * 
    * @return string
    * */
    public function createList($module, $list, Form $form)
    {
        //check if the Form has any Form Groups
        if (isset($form->children['form_group']) && is_array($form->children['form_group'])) {
            //go through the Form Groups
            foreach ($form->children['form_group'] as $formGroupUrl) {
                //get the Form Group as an object
                $formGroup = EntityFactory::build('form_group');
                //load the Form Group
                $formGroup->load($formGroupUrl);
                //see if the Form Group has any Form Rows
                if (isset($formGroup->children['form_row']) && is_array($formGroup->children['form_row'])) {
                    //set up a fields variable that will hold the fieldnames
                    $fields = '';
                    //set up a values variable that will hold the values of the fields
                    $values = '';
                    //go through each Form Row
                    foreach ($formGroup->children['form_row'] as $formRowUrl) {
                        //get the Form Row as an object
                        $formRow = EntityFactory::build('form_row');
                        //load the Form Row
                        $formRow->load($formRowUrl);
                        //add the field to the field list
                        $fields .= "`{$formRow->field}`, ";
                        //go through the list to insert
                        foreach ($list as $input) {
                            //add the open bracket to the values
                            $values .= '(';
                            //go through the fields in the list
                            foreach ($list as $field=>$value) {
                                //add the value to the field list
                                $values .= (!isset($input[$field]) || empty($input[$field])) ? 'NULL, ' : '\'' . mysql_real_escape_string($input[$field]) . '\', ';
                            }
                            //add the closing bracket to the values
                            $values .= '), ';
                        }
                        //remove the last space and comma
                        $values = substr($values, 0, -2);
                    }
                } else {
                    //the form groups above have no rows linked to them, throw an error
                    throw new Exception("The form for $module has form groups linked to it, but no rows in those groups");
                }
            }
            //build the query
            $sqlQuery = "INSERT INTO `$module` ($fields) VALUES $values";
            //return the query
            return $sqlQuery;
        }
        //the form has no groups linked to it, throw an error (nothing was returned by the code above)
        throw new Exception("The form for $module (create) has no form groups linked to it");
    }
    
    /**
    * Creates a query to delete a list of Entities
    * 
    * @param array $list An array of urls to be deleted
    * 
    * @return string
    * */
    public function deleteList($list)
    {
        //create and return the query
        return $this->changeStatusList($list, 'D');
    }
    
    /**
    * Creates a query to restore a list of Entities
    * 
    * @param array $list An array of urls to be deleted
    * 
    * @return string
    * */
    public function restoreList($list)
    {
        //create and return the query
        return $this->changeStatusList($list, 'A');
    }
    
    /**
    * Creates a query to change the status of a list of Entities
    * 
    * @param array $list The list that needs the status change
    * @param string $status The status to give the new list
    * 
    * @return string
    * */
    public function changeStatusList($list, $status)
    {
        //set up a urls variable that will hold all the urls to delete
        $urls = '';
        //go through the list
        foreach ($list as $url) {
            //add the url to te urls variable
            $urls .= "'$url', ";
        }
        //remove the last space and comma
        $urls = substr($urls, 0, -2);
        //create the query to do the update
        $sqlQuery = "UPDATE `entity` SET `status` = '$status' WHERE `url` IN ($urls)";
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Adds links to the `entity_link` table
    * 
    * @param array(integer) $list The list (of ids) that needs to be linked
    * @param integer $entity The id of the Entity being linked to
    * @param string $relationship The relationship of the list to the entity (child|parent)
    * @param integer $maxOrder The maximum order amount, the order will start at ($maxOrder + 1)
    * 
    * @return string
    * */
    public function addLinks($list, $entity, $relationship, $maxOrder = 0)
    {
        //build the start of the MySql query
        $sqlQuery = 'INSERT INTO `entity_link` (`id`, `parent`, `child`, `order`) VALUES ';
        //increase the maximum order amount
        $maxOrder++;
        //go through the list
        foreach ($list as $listItem) {
            //add the values for this list item into the query
            $sqlQuery .= "(NULL, " . (($relationship == 'parent') ? $listItem : $entity) . ', ' . (($relationship == 'parent') ? $entity : $listItem) . ", $maxOrder), ";
            //increase the maximum order amount
            $maxOrder++;
        }
        //remove the last space and comma
        $sqlQuery = substr($sqlQuery, 0, -2);
        //return the query
        return $sqlQuery;
    }
    
    
    
    
    
    /**
    * Delete a list of links from the `entity_link` table
    * 
    * @param array $list The list of links to delete
    * @param integer $entity The id of the entity these links are being deleted from
    * @param string $relationship The relationship that the links have to the Entity
    * 
    * @return string The query that was created
    * */
    public function deleteLinks(array $list, $entity, $relationship)
    {
        //get whether the entity needs to be selected as a parent or child
        $entityColumn = ($relationship == 'child') ? 'parent' : 'child';
        //build the query
        $sqlQuery = "DELETE FROM `entity_link` WHERE `$entityColumn` = $entity AND `$relationship` IN (" . implode(',', $list) . ")";
        //return the query
        return $sqlQuery;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
    * Add New Links to the `entity_link` table
    * Remove links from the `entity_link` table
    * Based on two arrays
    * 
    * @param Entity $entity The entity these links are being linked to
    * @param array $previous The array of previous items that were linked to this Entity
    * @param array $current The array of items that must now be linked to this Entity
    * @param string $relationship The relationship the links have towards the Entity (parent | child)
    * 
    * @return array[string] A list of MySql queries
    * */
    public function updateLinks(Entity $entity, array $previous, array $current, $relationship)
    {
        //get a list of links to delete
        $deleteList = array_diff($previous, $current);
        //get a list of links to add
        $addList  = array_diff($current, $previous);
        //an empty array to hold all the results
        $queries = array();
        //see if there are any items to delete
        if (is_array($deleteList) && count($deleteList) > 0) {
            //get the queries to delete the links
            $queries[] = $this->deleteLinks($deleteList, $entity->id, $relationship);
        }
        
        //see if there are any items to add
        if (is_array($addList) && count($addList) > 0) {
            //get the queries to add the links
            $addLinkQueries = $this->addLinks($addList, $entity->id, $relationship);
            //add this query to the list that needs to be returned
            $queries[] = $addLinkQueries;
        }
        //return the result
        return (count($queries) == 0) ? false : $queries;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
    * Create a new database table from a Form object
    * This Form object was generated by input into /form/create
    * 
    * @param Form $form The Form object that was created
    * @param Connection $database The database connection
    * 
    * @return string The query that was built
    * */
    public function createDatabaseTable(Form $form, Connection $database)
    {
        //the first line of the create query including the table name
        $sqlQuery = "CREATE TABLE `{$form->formModule}` (
                     `id` MEDIUMINT UNSIGNED NOT NULL, ";
        //go through the Form Groups linked to this Form
        foreach ($form->children['form_group'] as $formGroupUrl) {
            //get the Form Group as an object
            $formGroup = EntityFactory::build('form_group');
            //load the Form Group
            $formGroup->load($formGroupUrl, $database);
            //go through Form Rows linked to the Form Group
            foreach ($formGroup->children['form_row'] as $formRowUrl) {
                //get the Form Row as an object
                $formRow = EntityFactory::build('form_row');
                //load the Form Row
                $formRow->load($formRowUrl, $database);
                //add the Form Row to the query to create the table
                $sqlQuery .= $this->createDatabaseTableRow($formRow);
            }
        }
        //add the last line of the query
        $sqlQuery .= 'FOREIGN KEY (`id`) REFERENCES `entity`(`id`)
                      )ENGINE=InnoDB';
        //return the query
        return $sqlQuery;
    }
    
    /**
    * Creates the row part of the query for one Form Row object
    * 
    * @param FormRow $formRow The Form Row that the query is being created for
    * 
    * @return string The query that was built
    * Example: `username` VARCHAR(32) NOT NULL DEFAULT 'John Smith'
    * */
    private function createDatabaseTableRow(FormRow $formRow)
    {
        //add the NULL/NOT NULL
        $null = ($formRow->null == '1') ? "NULL " : 'NOT NULL ';
        //add the default value
        $default = (isset($formRow->default) && !empty($formRow->default)) ? "DEFAULT '{$formRow->default}' " : '';
        //get the name of the method that should build this type
        $functionName = 'createDatabaseTableRow' . ucfirst(strtolower($formRow->typeParsed));
        //check if the method exists within this class
        if (method_exists($this, $functionName)) {
            //call the method
            $sqlRow = $this->$functionName($formRow);
        } else {
            //throw an Exception
            throw new Exception("A function/method is required to build a '{$formRow->typeParsed}' row");
        }
        //return the row that was created
        return "$sqlRow $null $default, ";
    }
    
    /**
    * Creates a query to alter a table row
    * 
    * @param string $tableName The name of the database table this row belongs to
    * @param FormRow $oldFormRow The previos values of the Form Row
    * @param FormRow $formRow The new Form Row being created
    * 
    * @return string The MySql query
    * */
    public function updateDatabaseTableRow ($tableName, FormRow $oldFormRow, FormRow $formRow)
    {
        //add the initial part of the Query
        $sqlQuery = "ALTER TABLE `$tableName`
                     CHANGE COLUMN `{$oldFormRow->field}` ";
        //add the new database form  row part
        $sqlQuery .= $this->createDatabaseTableRow($formRow);
        //return the new MySql query
        return rtrim(trim($sqlQuery), ',');
    }
    
    /**
    * Build a 'varchar' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowVarchar(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` VARCHAR({$formRow->lengthMax}) ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'text' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowText(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` TEXT({$formRow->lengthMax}) ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'date' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowDate(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` DATE ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'datetime' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowDatetime(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` DATETIME ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a generic integer type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * @param string $type The type of integer: TINYINT | MEDIUMINT | INT
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowIntegerGeneric(FormRow $formRow, $type)
    {
        //check whether this type is within the array, and if so whther it is signed or unsigned
        $signed = ($formRow->signed == '1') ? 'SIGNED ' : 'UNSIGNED ';
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` $type $signed ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'tinyint' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowTinyint(FormRow $formRow)
    {
        //use the generic integer function to build this field
        return $this->createDatabaseTableRowIntegerGeneric($formRow, 'TINYINT');
    }
    
    /**
    * Build a 'smallint' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowSmallint(FormRow $formRow)
    {
        //use the generic integer function to build this field
        return $this->createDatabaseTableRowIntegerGeneric($formRow, 'SMALLINT');
    }
    
    /**
    * Build a 'mediumint' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowMediumint(FormRow $formRow)
    {
        //use the generic integer function to build this field
        return $this->createDatabaseTableRowIntegerGeneric($formRow, 'MEDIUMINT');
    }
    
    /**
    * Build a 'int' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowInt(FormRow $formRow)
    {
        //use the generic integer function to build this field
        return $this->createDatabaseTableRowIntegerGeneric($formRow, 'INT');
    }
    
    /**
    * Build a 'bigint' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowBigint(FormRow $formRow)
    {
        //use the generic integer function to build this field
        return $this->createDatabaseTableRowIntegerGeneric($formRow, 'BIGINT');
    }
    
    /**
    * Build a 'decimal' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowDecimal(FormRow $formRow)
    {
        //check whether this type is within the array, and if so whther it is signed or unsigned
        $signed = ($formRow->signed == '1') ? 'SIGNED ' : 'UNSIGNED ';
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` DECIMAL({$formRow->lengthMax},{$formRow->lengthMin}) $signed ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'time' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowTime(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` TIME ";
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'set' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowSet(FormRow $formRow)
    {
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` SET('" . str_replace(',', '\',\'', $formRow->allowedValues) . '\') ';
        //return the query row that has been built
        return $queryRow;
    }
    
    /**
    * Build a 'enum' type row for a MySql CREATE TABLE query
    * 
    * @param FormRow $formRow The Form Row that the query row is being created for
    * 
    * @return string The row query
    * */
    public function createDatabaseTableRowEnum(FormRow $formRow)
    {
        //a variable to hold the parsed "allowed values"
        $string = '';
        //go through each of the "allowed values"
        foreach ($formRow->allowedValues as $value) {
            //add this value to the string
            $string .= "'$value',";
        }
        //remove the last comma from the string
        $string = rtrim($string, ',');
        //add the name of the row and the database type with constraints
        $queryRow = "`{$formRow->field}` ENUM($string) ";
        //return the query row that has been built
        return $queryRow;
    }
    
}
?>