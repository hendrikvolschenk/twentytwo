<?php
class UserGroup extends Entity
{
    
    /**
    * Returns an array of all matching Entities' urls
    * 
    * @param Connection $database The database connection
    * @param string $module The type of Entities to find
    * @param string|array The status(ses) to match
    * 
    * @return array A list of metching Entity urls
    * */
    public static function listAll(Connection $database, $module, $status = 'A')
    {
        //get the system variable
        global $system;
        //check if this is a minisite administrator
        if ($system->user->children['user_group'][0] === 'users') {
            //write a query to fetch the relevant user groups
            $sqlSelect = "SELECT `entity`.`url`
                          FROM `entity`
                          WHERE `status` = '{$status}'
                          AND `module` = '{$module}'
                          AND `url` != 'administrators'
                          AND `url` != 'visitors'";
        } else {
            //return the default parent method's list
            return parent::listAll($database, $module, $status);
        }
        //run the query
        $sqlQuery = $database->runQuery($sqlSelect);
        //return the result of the query
        return $database->fetchResult($sqlQuery);
    }
    
}
?>