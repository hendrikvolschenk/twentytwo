<?php
/**
* Builds all the necessary components for a Form Action
* */
class FormAction extends Entity
{
    
    /**
    * The actual action that will be performed
    * 
    * @var string
    * */
    public $action = null;
    
    /**
    * A description of what the Form Action does
    * 
    * @var string
    * */
    public $description = null;
    
    /**
    * Whether this Form Action should be displayed in the main menu 0|1
    * 
    * @var string
    * */
    public $mainMenu = null;
    
    /**
    * Whether this Form Action should be displayed in the module menu 0|1
    * 
    * @var string
    * */
    public $moduleMenu = null;
    
    /**
    * Whether this Form Action should be displayed on a per-Entity basis 0|1
    * 
    * @var string
    * */
    public $entity = null;
    
    /**
    * The title that this Form Action should show in the main menu
    * 
    * @var string
    * */
    public $mainMenuTitle = null;
    
    /**
    * The title that this Form Action should show in the module menu
    * 
    * @var string
    * */
    public $moduleMenuTitle = null;
    
    /**
    * The title that this Form Action should show on a per Entity basis
    * 
    * @var string
    * */
    public $entityTitle = null;
    

    /**
    * An additional function is required to find the form's url
    * 
    * @param Connection $connection A database connection
    * @param string $module What type of Entuty the form is being built for
    * @raram string $action The action being performed on this module
    * */
    public static function getUrl(Connection $connection, $module, $action)
    {
        //build a query to get the url of the form using the module and action to find it
       $sqlSelect = "SELECT `entity`.`url`
                      FROM `form_action`
                      JOIN `entity` ON `form_action`.`id` = `entity`.`id`
                      JOIN `entity_link` ON `entity`.`id` = `entity_link`.`child`
                      JOIN `form` ON `entity_link`.`parent` = `form`.`id`
                      WHERE `form`.`form_module` = '$module'
                      AND `form_action`.`action` = '$action'
                      LIMIT 1";
        //run the query
        $sqlQuery = $connection->runQuery($sqlSelect);
        //get the results of the query
        $result = $connection->fetchResult($sqlQuery);
        //return the first result in the array
        return (isset($result[0])) ? $result[0] : null;
    }

}