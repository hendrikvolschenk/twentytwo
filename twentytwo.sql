﻿-- MySQL dump 10.13  Distrib 5.6.12, for Win32 (x86)
--
-- Host: localhost    Database: twentytwo
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(64) NOT NULL,
  `module` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `status` enum('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1918 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity`
--

LOCK TABLES `entity` WRITE;
/*!40000 ALTER TABLE `entity` DISABLE KEYS */;
INSERT INTO `entity` VALUES (64,'form','form','2013-03-28 20:44:45','2014-10-17 11:26:55','A'),(65,'general_form_information','form_group','2013-03-28 20:44:45',NULL,'A'),(66,'form_module','form_row','2013-03-28 20:53:06',NULL,'A'),(68,'form_group','form_link','2013-03-28 21:07:08',NULL,'A'),(69,'form_group_1','form','2013-03-28 21:07:08',NULL,'A'),(70,'form_row','form','2013-03-28 21:07:08',NULL,'A'),(71,'general_form_group_information','form_group','2013-03-28 21:54:22',NULL,'A'),(72,'title_2','form_row','2013-03-28 21:54:22',NULL,'A'),(73,'description_1','form_row','2013-03-28 21:54:22',NULL,'A'),(75,'form_row_1','form_link','2013-03-28 22:09:19',NULL,'A'),(82,'null','form_row','2013-03-28 23:14:49',NULL,'A'),(84,'disabled','form_row','2013-03-28 23:14:49',NULL,'A'),(85,'readonly','form_row','2013-03-28 23:14:49',NULL,'A'),(86,'value_minimum','form_row','2013-03-28 23:14:49',NULL,'A'),(87,'value_maximum','form_row','2013-03-28 23:14:49',NULL,'A'),(88,'step','form_row','2013-03-28 23:14:49',NULL,'A'),(89,'length_min','form_row','2013-03-28 23:14:49',NULL,'A'),(90,'length_max','form_row','2013-03-28 23:14:49',NULL,'A'),(91,'functions_prepare','form_row','2013-03-28 23:14:49',NULL,'A'),(92,'functions_validate','form_row','2013-03-28 23:14:49',NULL,'A'),(94,'values','form_group','2013-03-29 00:15:50','2014-01-19 21:27:24','A'),(111,'name','form_row','2013-03-29 21:12:39',NULL,'A'),(113,'default_status','form_row','2013-03-29 21:12:39',NULL,'A'),(184,'properties','form_group','2013-07-03 23:32:31','2013-07-03 23:32:31','A'),(185,'url_column','form_row','2013-07-03 23:38:05',NULL,'A'),(262,'form_link_link','form_link','2013-07-07 23:11:57',NULL,'A'),(263,'form_link','form','2013-07-07 23:17:33',NULL,'A'),(264,'form_link_information','form_group','2013-07-07 23:20:37',NULL,'A'),(279,'include','form_row','2013-07-08 20:21:32',NULL,'A'),(280,'title_6','form_row','2013-07-08 20:21:32',NULL,'A'),(281,'relationship','form_row','2013-07-08 20:21:32',NULL,'A'),(282,'required','form_row','2013-07-08 20:21:32',NULL,'A'),(283,'multiple','form_row','2013-07-08 20:21:32',NULL,'A'),(284,'create','form_row','2013-07-08 20:21:32',NULL,'A'),(285,'select','form_row','2013-07-08 20:21:32',NULL,'A'),(354,'display','form_row','2013-07-17 16:13:14',NULL,'A'),(395,'field','form_group','2013-09-19 13:46:29',NULL,'A'),(396,'field_1','form_row','2013-09-19 14:12:59',NULL,'A'),(397,'label','form_row','2013-09-19 14:12:59',NULL,'A'),(398,'description','form_row','2013-09-19 14:12:59',NULL,'A'),(399,'placeholder','form_row','2013-09-19 14:12:59',NULL,'A'),(400,'type','form_row','2013-09-19 14:12:59',NULL,'A'),(401,'form_1','form_link','2013-09-19 16:41:44',NULL,'A'),(403,'description_2','form_row','2013-09-21 01:15:47',NULL,'A'),(404,'form_action_1','form_link','2013-10-19 17:42:16',NULL,'A'),(405,'action','form_row','2013-10-19 17:46:13',NULL,'A'),(406,'description_3','form_row','2013-10-19 17:46:53',NULL,'A'),(407,'general_form_action_information','form_group','2013-10-19 17:47:57',NULL,'A'),(408,'display_main_menu','form_row','2013-10-19 17:52:23',NULL,'A'),(409,'display_module_menu','form_row','2013-10-19 17:54:06',NULL,'A'),(410,'display_entity','form_row','2013-10-19 17:54:54',NULL,'A'),(411,'display_1','form_group','2013-10-19 17:54:55',NULL,'A'),(413,'form_action_3','form','2013-10-19 17:59:38',NULL,'A'),(420,'create_1','form_action','2013-10-19 18:46:48',NULL,'A'),(421,'update','form_action','2013-10-19 18:47:43',NULL,'A'),(422,'delete_me','form_row','2013-10-19 18:49:11',NULL,'A'),(440,'title','form_row','2013-10-20 00:43:54',NULL,'A'),(441,'description_4','form_row','2013-10-20 00:44:11',NULL,'A'),(442,'user_group_information','form_group','2013-10-20 00:44:15',NULL,'A'),(443,'create_2','form_action','2013-10-20 00:44:39','2014-01-14 00:42:58','A'),(444,'update_1','form_action','2013-10-20 00:44:55','2014-01-14 00:44:33','A'),(445,'delete','form_action','2013-10-20 00:45:07','2014-01-14 00:45:43','A'),(446,'restore','form_action','2013-10-20 00:45:19',NULL,'A'),(447,'user_group','form','2013-10-20 01:03:53','2014-01-14 00:41:39','A'),(448,'create_3','form_action','2013-10-20 01:10:20','2014-01-12 21:30:47','A'),(449,'delete_1','form_action','2013-10-20 01:10:37',NULL,'A'),(450,'update_2','form_action','2013-10-20 01:10:52',NULL,'A'),(451,'restore_1','form_action','2013-10-20 01:11:35','2013-11-03 23:09:12','A'),(452,'login','form_action','2013-10-20 01:12:10',NULL,'A'),(453,'logout','form_action','2013-10-20 01:12:24',NULL,'A'),(454,'user_group_1','form_link','2013-10-20 01:13:11','2014-10-07 13:39:38','A'),(455,'email_address','form_row','2013-10-20 01:14:27',NULL,'A'),(456,'first_name','form_row','2013-10-20 01:14:50',NULL,'A'),(457,'last_name','form_row','2013-10-20 01:15:08',NULL,'A'),(458,'password','form_row','2013-10-20 01:16:04',NULL,'A'),(459,'personal_information','form_group','2013-10-20 01:16:07',NULL,'A'),(460,'user','form','2013-10-20 01:17:47','2014-01-12 01:41:01','A'),(461,'administrators','user_group','2013-10-20 01:19:42',NULL,'A'),(462,'righteoustrespassergmailcom','user','2013-10-20 01:19:45','2014-11-24 19:06:53','A'),(465,'view','form_action','2013-10-20 21:08:21','2014-01-12 21:32:43','A'),(466,'create_4','form_action','2013-10-20 21:26:09','2014-01-12 00:38:57','A'),(467,'view_1','form_action','2013-10-20 21:26:09','2014-08-04 19:36:35','A'),(468,'update_3','form_action','2013-10-20 21:26:09','2014-01-12 01:32:00','A'),(469,'delete_2','form_action','2013-10-20 21:26:09','2014-01-12 01:06:16','A'),(470,'restore_2','form_action','2013-10-20 21:26:09','2014-01-12 01:05:36','A'),(471,'user_1','form_link','2013-10-21 21:46:01',NULL,'A'),(472,'user_group_2','form_link','2013-10-21 21:46:01',NULL,'A'),(475,'users','user_group','2013-10-24 16:56:15',NULL,'A'),(476,'create_5','form_action','2013-10-25 15:16:01',NULL,'A'),(477,'create_6','form_action','2013-10-25 15:17:25',NULL,'A'),(478,'update_4','form_action','2013-10-25 15:21:22',NULL,'A'),(479,'view_2','form_action','2013-10-25 15:23:39',NULL,'A'),(480,'delete_3','form_action','2013-10-25 15:29:32',NULL,'A'),(485,'create_7','form_action','2013-10-25 15:35:29',NULL,'A'),(486,'update_5','form_action','2013-10-25 15:35:39',NULL,'A'),(487,'delete_4','form_action','2013-10-25 15:35:50',NULL,'A'),(488,'restore_3','form_action','2013-10-25 15:36:02',NULL,'A'),(490,'main_menu_title','form_row','2013-10-28 22:06:40',NULL,'A'),(491,'module_menu_title','form_row','2013-10-28 22:07:38',NULL,'A'),(492,'entity_title','form_row','2013-10-28 22:08:20',NULL,'A'),(493,'visitors','user_group','2013-11-03 22:32:12',NULL,'A'),(494,'visitor','user','2013-11-03 22:32:14','2015-02-25 22:31:47','A'),(495,'bbb','form_row','2013-11-14 20:38:50',NULL,'A'),(496,'ccc','form_row','2013-11-14 20:40:57',NULL,'A'),(497,'iii','form_row','2013-11-14 20:41:29',NULL,'A'),(498,'type_parsed','form_row','2013-11-14 20:42:37',NULL,'A'),(499,'jjj','form_row','2013-11-14 20:44:44',NULL,'A'),(500,'hhhh','form_row','2013-11-14 20:46:27',NULL,'A'),(515,'create_11','form_action','2013-11-14 21:07:21',NULL,'A'),(523,'jan','jano','2013-12-09 19:04:58',NULL,'A'),(531,'content','form_row','2014-01-06 22:44:21',NULL,'A'),(536,'create_14','form_action','2014-01-07 10:18:42',NULL,'A'),(545,'hendrik','human','2014-01-07 10:22:29',NULL,'A'),(553,'dfgdfg','form_action','2014-01-07 10:42:38',NULL,'A'),(555,'create_17','form_action','2014-01-07 13:37:43',NULL,'A'),(556,'delete_6','form_action','2014-01-07 13:38:04',NULL,'A'),(557,'restore_5','form_action','2014-01-07 13:38:23',NULL,'A'),(558,'view_7','form_action','2014-01-07 13:38:46',NULL,'A'),(559,'title_3','form_row','2014-01-07 13:40:08',NULL,'A'),(560,'description_5','form_row','2014-01-07 13:40:47',NULL,'A'),(561,'general_image_information','form_group','2014-01-07 13:41:12',NULL,'A'),(562,'filename','form_row','2014-01-07 13:43:34',NULL,'A'),(563,'path','form_row','2014-01-07 13:44:10',NULL,'A'),(564,'filesize','form_row','2014-01-07 13:45:35',NULL,'A'),(565,'width','form_row','2014-01-07 13:46:12',NULL,'A'),(566,'height','form_row','2014-01-07 13:46:37',NULL,'A'),(567,'technical_information','form_group','2014-01-07 13:46:43',NULL,'A'),(568,'image','form','2014-01-07 13:46:47',NULL,'A'),(592,'first_image_gallery','image_gallery','2014-01-08 18:55:11',NULL,'A'),(607,'thumbnail','form_link','2014-01-10 18:58:17',NULL,'A'),(615,'thumbnail_1','form_link','2014-01-12 00:38:15',NULL,'A'),(621,'thumbnail_2','form_link','2014-01-12 01:40:56','2015-02-25 22:32:35','A'),(623,'view_11','form_action','2014-01-12 21:13:02',NULL,'A'),(624,'update_9','form_action','2014-01-12 21:13:48',NULL,'A'),(625,'restore_7','form_action','2014-01-12 21:14:51',NULL,'A'),(627,'link_1','form_action','2014-01-12 21:18:08',NULL,'A'),(637,'update_11','form_action','2014-01-14 00:32:19',NULL,'A'),(638,'link_2','form_action','2014-01-14 00:33:01',NULL,'A'),(641,'view_12','form_action','2014-01-14 00:40:50','2014-01-14 00:43:31','A'),(642,'link_3','form_action','2014-01-14 00:41:25',NULL,'A'),(643,'delete_10','form_action','2014-01-14 09:37:36',NULL,'A'),(644,'create_21','form_action','2014-01-14 09:39:55',NULL,'A'),(645,'view_13','form_action','2014-01-14 09:40:34',NULL,'A'),(646,'update_12','form_action','2014-01-14 09:40:58',NULL,'A'),(647,'delete_11','form_action','2014-01-14 09:41:38',NULL,'A'),(648,'restore_10','form_action','2014-01-14 09:42:04',NULL,'A'),(649,'link_4','form_action','2014-01-14 09:44:03',NULL,'A'),(650,'link_5','form_action','2014-01-14 09:44:42',NULL,'A'),(651,'create_22','form_action','2014-01-14 09:46:20',NULL,'A'),(652,'view_14','form_action','2014-01-14 09:46:47',NULL,'A'),(653,'update_13','form_action','2014-01-14 09:47:10',NULL,'A'),(654,'delete_12','form_action','2014-01-14 09:47:32',NULL,'A'),(655,'restore_11','form_action','2014-01-14 09:47:58',NULL,'A'),(656,'link_6','form_action','2014-01-14 09:48:31',NULL,'A'),(659,'view_15','form_action','2014-01-16 09:44:06',NULL,'A'),(660,'trash_row','form_row','2014-01-16 09:45:01',NULL,'A'),(661,'trash_group','form_group','2014-01-16 09:45:03',NULL,'A'),(662,'trash','form','2014-01-16 09:45:05',NULL,'A'),(663,'allowed_values','form_row','2014-01-19 21:26:57',NULL,'A'),(664,'allowed_statusses','form_row','2014-01-19 21:36:21',NULL,'A'),(665,'allowed_statusses_1','form_row','2014-01-19 21:37:37',NULL,'A'),(666,'disallowed_statusses','form_row','2014-01-19 21:39:40',NULL,'A'),(667,'statusses','form_group','2014-01-19 21:40:15',NULL,'A'),(668,'delete_me_1','form_action','2014-01-19 21:49:20',NULL,'A'),(669,'deleeeete','form_action','2014-01-19 21:54:19',NULL,'A'),(670,'deleeeete_1','form_action','2014-01-19 21:55:11',NULL,'A'),(671,'del','form_action','2014-01-19 22:23:26',NULL,'A'),(674,'clone_1','form_action','2014-01-20 14:12:37',NULL,'A'),(677,'clone_3','form_action','2014-01-20 14:54:54',NULL,'A'),(679,'clone_5','form_action','2014-01-20 14:57:15',NULL,'A'),(735,'create_27','form_action','2014-01-27 21:58:20',NULL,'A'),(825,'view_23','form_action','2014-03-03 20:57:42',NULL,'A'),(826,'title_8','form_row','2014-03-03 20:58:31',NULL,'A'),(827,'html','form_row','2014-03-03 20:58:58',NULL,'A'),(828,'one_4','form_group','2014-03-03 20:59:01',NULL,'A'),(829,'view_24','form_action','2014-03-03 21:02:56',NULL,'A'),(830,'create_31','form_action','2014-03-03 21:03:36',NULL,'A'),(831,'title_9','form_row','2014-03-03 21:03:52',NULL,'A'),(832,'gggg','form_row','2014-03-03 21:04:50',NULL,'A'),(833,'uno','form_group','2014-03-03 21:04:55',NULL,'A'),(857,'multiple_1','form_row','2014-03-06 14:24:41',NULL,'A'),(870,'situated_in_the_picturesque_suikerbosrand','article','2014-03-07 15:11:58',NULL,'A'),(876,'link_9','form_action','2014-03-07 15:31:40',NULL,'A'),(913,'name_12','form_row','2014-03-09 17:12:46',NULL,'A'),(915,'phone','form_row','2014-03-09 17:14:06',NULL,'A'),(1040,'contact_details','article','2014-03-12 11:51:26',NULL,'A'),(1041,'gps_coordinates','article','2014-03-12 11:51:47',NULL,'A'),(1053,'rooms__rates','article','2014-03-13 21:36:28',NULL,'A'),(1054,'righteoustrespassergmailcom_3','reservation','2014-03-13 23:47:31',NULL,'A'),(1055,'hendrik_volschenk','message','2014-03-14 00:05:53',NULL,'A'),(1056,'jjjjjjjj','message','2014-03-14 00:09:12',NULL,'A'),(1057,'0825641706','callback','2014-03-14 00:13:34',NULL,'A'),(1058,'0825641706_1','callback','2014-03-14 00:13:46',NULL,'A'),(1059,'righteoustrespassergmailcom_4','reservation','2014-03-14 01:14:28',NULL,'A'),(1060,'0825641706_2','callback','2014-03-14 01:14:47',NULL,'A'),(1061,'hendrik_volschenk_1','message','2014-03-14 01:16:50',NULL,'A'),(1062,'pvcdesignsgmailcom','reservation','2014-03-14 09:06:21',NULL,'A'),(1063,'999999999','callback','2014-03-14 09:06:54',NULL,'A'),(1064,'kkj','message','2014-03-14 09:07:37',NULL,'A'),(1070,'righteoustrespassergmailcom_5','reservation','2014-03-17 22:22:47',NULL,'A'),(1071,'righteoustrespassergmailcom_6','reservation','2014-03-17 22:30:55',NULL,'A'),(1072,'righteoustrespassergmailcom_7','reservation','2014-03-17 22:31:17',NULL,'A'),(1073,'righteoustrespassergmailcom_8','reservation','2014-03-17 22:33:37',NULL,'A'),(1074,'righteoustrespassergmailcom_9','reservation','2014-03-17 22:44:03',NULL,'A'),(1075,'righteoustrespassergmailcom_10','reservation','2014-03-17 22:45:02',NULL,'A'),(1076,'toetstoetstoets','reservation','2014-03-17 22:46:09',NULL,'A'),(1077,'righteoustrespassergmailcom_11','reservation','2014-03-17 22:59:42',NULL,'A'),(1078,'righteoustrespassergmailcom_12','reservation','2014-03-17 23:01:12',NULL,'A'),(1083,'barrybarrycom','reservation','2014-03-18 20:02:04',NULL,'A'),(1084,'hendrikcoolbroscom','reservation','2014-03-18 20:06:42',NULL,'A'),(1085,'yy','message','2014-03-18 20:10:14',NULL,'A'),(1086,'hendrik_volschenk_2','message','2014-03-18 20:11:12',NULL,'A'),(1087,'hendrik_volschenk_3','message','2014-03-18 20:11:56',NULL,'A'),(1088,'hendrik_volschenk_4','message','2014-03-18 20:13:12',NULL,'A'),(1089,'0825641706_3','callback','2014-03-18 20:19:32',NULL,'A'),(1090,'0825641706_4','callback','2014-03-18 20:21:08',NULL,'A'),(1091,'0825641706_5','callback','2014-03-18 20:22:00',NULL,'A'),(1092,'robmarshmwebcoza','reservation','2014-03-19 10:16:21',NULL,'A'),(1093,'robert','message','2014-03-19 10:25:21',NULL,'A'),(1094,'debbiestrobergmecom','reservation','2014-03-19 19:59:59',NULL,'A'),(1095,'debcolmwebcoza','reservation','2014-03-19 20:03:36',NULL,'A'),(1096,'kobiegerjjdevcoza','reservation','2014-03-24 15:04:47',NULL,'A'),(1097,'besterrcgmailcom','reservation','2014-03-25 12:24:01',NULL,'A'),(1098,'wynandcoetzer87gmailcom','reservation','2014-03-26 13:54:47',NULL,'A'),(1099,'shannon_kalil','message','2014-03-28 16:11:07',NULL,'A'),(1100,'rinskelynnwoodlaercoza','reservation','2014-03-31 19:05:00',NULL,'A'),(1101,'rinskelynnwoodlaercoza_1','reservation','2014-03-31 19:05:00',NULL,'A'),(1102,'areet23gmailcom','reservation','2014-04-01 22:17:18',NULL,'A'),(1103,'areet23gmailcom_1','reservation','2014-04-01 22:17:19',NULL,'A'),(1105,'nicolenes6mecom','reservation','2014-04-07 16:43:59',NULL,'A'),(1106,'jessierabbittgmailcom','reservation','2014-04-07 18:27:48',NULL,'A'),(1107,'jessica_wilson','message','2014-04-07 18:29:59',NULL,'A'),(1108,'home','article','2014-04-09 10:18:06',NULL,'A'),(1109,'about','article','2014-04-09 10:20:51',NULL,'A'),(1110,'facts','article','2014-04-09 10:27:16',NULL,'A'),(1111,'contact_1','article','2014-04-09 10:39:21',NULL,'A'),(1138,'decking','product_category','2014-04-10 13:13:22',NULL,'A'),(1139,'garden','product_category','2014-04-10 13:16:03',NULL,'A'),(1140,'schools','product_category','2014-04-10 13:16:15',NULL,'A'),(1141,'furniture','product_category','2014-04-10 13:16:30',NULL,'A'),(1142,'pets','product_category','2014-04-10 13:16:50',NULL,'A'),(1143,'raw_materials','product_category','2014-04-10 13:17:03',NULL,'A'),(1146,'ba112','product_price','2014-04-10 15:43:20',NULL,'A'),(1155,'b001','product_price','2014-04-11 13:36:18',NULL,'A'),(1156,'b002','product_price','2014-04-11 13:36:37',NULL,'A'),(1157,'b003','product_price','2014-04-11 13:36:55',NULL,'A'),(1158,'b004','product_price','2014-04-11 13:37:11',NULL,'A'),(1159,'queen_bench_1','product','2014-04-11 13:37:14',NULL,'A'),(1161,'b005','product_price','2014-04-11 13:41:19',NULL,'A'),(1162,'b006','product_price','2014-04-11 13:41:37',NULL,'A'),(1163,'b007','product_price','2014-04-11 13:41:54',NULL,'A'),(1164,'b008','product_price','2014-04-11 13:42:11',NULL,'A'),(1165,'school_bench_1','product','2014-04-11 13:42:15',NULL,'A'),(1167,'b009','product_price','2014-04-11 13:45:54',NULL,'A'),(1168,'b010','product_price','2014-04-11 13:46:10',NULL,'A'),(1169,'b011','product_price','2014-04-11 13:46:25',NULL,'A'),(1170,'b012','product_price','2014-04-11 13:46:40',NULL,'A'),(1171,'lapa_bench_1','product','2014-04-11 13:46:45',NULL,'A'),(1173,'b013','product_price','2014-04-11 13:49:26',NULL,'A'),(1174,'log_bench_1','product','2014-04-11 13:49:29',NULL,'A'),(1176,'b014','product_price','2014-04-11 13:54:03',NULL,'A'),(1177,'b015','product_price','2014-04-11 13:54:22',NULL,'A'),(1178,'b016','product_price','2014-04-11 13:54:35',NULL,'A'),(1179,'b017','product_price','2014-04-11 13:54:50',NULL,'A'),(1180,'garden_bench_1','product','2014-04-11 13:54:52',NULL,'A'),(1182,'b018','product_price','2014-04-11 13:57:23',NULL,'A'),(1183,'b019','product_price','2014-04-11 13:57:36',NULL,'A'),(1184,'storage_bench_1','product','2014-04-11 13:57:38',NULL,'A'),(1186,'b020','product_price','2014-04-11 14:01:47',NULL,'A'),(1187,'pergola_bench_1','product','2014-04-11 14:01:50',NULL,'A'),(1189,'b021','product_price','2014-04-11 14:21:59',NULL,'A'),(1190,'sundowner_set_1','product','2014-04-11 14:22:00',NULL,'A'),(1192,'b022','product_price','2014-04-11 14:28:46',NULL,'A'),(1193,'swing_bench_2','product','2014-04-11 14:28:49',NULL,'A'),(1195,'b023','product_price','2014-04-11 14:31:18',NULL,'A'),(1196,'pool_lounger_1','product','2014-04-11 14:31:20',NULL,'A'),(1198,'b024','product_price','2014-04-11 14:35:31',NULL,'A'),(1199,'b025','product_price','2014-04-11 14:35:45',NULL,'A'),(1200,'hex_picnic_set','product','2014-04-11 14:35:48',NULL,'A'),(1203,'b026','product_price','2014-04-11 14:38:14',NULL,'A'),(1204,'b027','product_price','2014-04-11 14:38:30',NULL,'A'),(1205,'hex_patio_set_2','product','2014-04-11 14:38:32',NULL,'A'),(1208,'b028','product_price','2014-04-11 14:41:41',NULL,'A'),(1209,'garden_set_1','product','2014-04-11 14:41:45',NULL,'A'),(1211,'b029','product_price','2014-04-11 14:45:09',NULL,'A'),(1212,'b030','product_price','2014-04-11 14:45:26',NULL,'A'),(1213,'b031','product_price','2014-04-11 14:45:49',NULL,'A'),(1214,'cocktail_set_1','product','2014-04-11 14:45:52',NULL,'A'),(1216,'b046','product_price','2014-04-11 14:51:15',NULL,'A'),(1217,'b031_1','product_price','2014-04-11 14:52:07',NULL,'A'),(1218,'b032','product_price','2014-04-11 14:52:27',NULL,'A'),(1220,'b046_1','product_price','2014-04-11 14:53:52',NULL,'A'),(1221,'b031_2','product_price','2014-04-11 14:54:05',NULL,'A'),(1222,'b032_1','product_price','2014-04-11 14:54:21',NULL,'A'),(1223,'b033','product_price','2014-04-11 14:54:41',NULL,'A'),(1224,'b034','product_price','2014-04-11 14:55:02',NULL,'A'),(1225,'b035','product_price','2014-04-11 14:55:19',NULL,'A'),(1226,'b036','product_price','2014-04-11 14:55:34',NULL,'A'),(1227,'pub_bench_2','product','2014-04-11 14:55:38',NULL,'A'),(1229,'b037','product_price','2014-04-11 14:59:54',NULL,'A'),(1230,'b038','product_price','2014-04-11 15:00:12',NULL,'A'),(1231,'b039','product_price','2014-04-11 15:00:39',NULL,'A'),(1232,'b040','product_price','2014-04-11 15:00:54',NULL,'A'),(1233,'b041','product_price','2014-04-11 15:01:07',NULL,'A'),(1234,'b042','product_price','2014-04-11 15:01:25',NULL,'A'),(1235,'pub_bench_no_gaps','product','2014-04-11 15:01:27',NULL,'A'),(1237,'b043','product_price','2014-04-11 15:03:28',NULL,'A'),(1238,'b044','product_price','2014-04-11 15:03:50',NULL,'A'),(1239,'b045','product_price','2014-04-11 15:04:02',NULL,'A'),(1240,'kiddies_picnic_set_1','product','2014-04-11 15:04:05',NULL,'A'),(1242,'b047','product_price','2014-04-11 15:06:15',NULL,'A'),(1246,'b048','product_price','2014-04-11 15:14:05',NULL,'A'),(1247,'lapa_set_1','product','2014-04-11 15:14:08',NULL,'A'),(1250,'b047_1','product_price','2014-04-11 15:16:19',NULL,'A'),(1251,'family_set_3','product','2014-04-11 15:16:21',NULL,'A'),(1253,'b049','product_price','2014-04-11 15:19:22',NULL,'A'),(1254,'b050','product_price','2014-04-11 15:19:50',NULL,'A'),(1255,'b051','product_price','2014-04-11 15:20:16',NULL,'A'),(1256,'dog_kennel_1','product','2014-04-11 15:20:18',NULL,'A'),(1258,'b037_1','product_price','2014-04-11 15:23:17',NULL,'A'),(1259,'storage_box_1','product','2014-04-11 15:23:30',NULL,'A'),(1261,'b050_1','product_price','2014-04-11 15:25:44',NULL,'A'),(1262,'b051_1','product_price','2014-04-11 15:26:18',NULL,'A'),(1263,'b052','product_price','2014-04-11 15:26:32',NULL,'A'),(1264,'b052_1','product_price','2014-04-11 15:26:51',NULL,'A'),(1265,'school_desks_1','product','2014-04-11 15:26:53',NULL,'A'),(1267,'b053','product_price','2014-04-11 15:43:15',NULL,'A'),(1268,'b054','product_price','2014-04-11 15:43:30',NULL,'A'),(1269,'b055','product_price','2014-04-11 15:43:43',NULL,'A'),(1270,'b056','product_price','2014-04-11 15:44:04',NULL,'A'),(1271,'b057','product_price','2014-04-11 15:44:26',NULL,'A'),(1272,'b058','product_price','2014-04-11 15:44:43',NULL,'A'),(1273,'tables','product','2014-04-11 15:44:46',NULL,'A'),(1275,'pf001','product_price','2014-04-11 15:50:44',NULL,'A'),(1276,'pf002','product_price','2014-04-11 15:51:10',NULL,'A'),(1277,'pf003','product_price','2014-04-11 15:51:25',NULL,'A'),(1278,'picket_fencing_1','product','2014-04-11 15:51:27',NULL,'A'),(1280,'gd001','product_price','2014-04-11 15:55:53',NULL,'A'),(1281,'gd001_1','product_price','2014-04-11 15:55:53',NULL,'A'),(1282,'gd002','product_price','2014-04-11 15:56:14',NULL,'A'),(1283,'gd003','product_price','2014-04-11 15:56:33',NULL,'A'),(1284,'garden_edging_1','product','2014-04-11 15:56:39',NULL,'A'),(1286,'br001','product_price','2014-04-11 15:59:23',NULL,'A'),(1287,'br002','product_price','2014-04-11 15:59:40',NULL,'A'),(1288,'br003','product_price','2014-04-11 15:59:58',NULL,'A'),(1289,'bridges_1','product','2014-04-11 16:00:02',NULL,'A'),(1291,'d001','product_price','2014-04-11 16:11:32',NULL,'A'),(1292,'d002','product_price','2014-04-11 16:11:48',NULL,'A'),(1293,'d003','product_price','2014-04-11 16:12:14',NULL,'A'),(1294,'d004','product_price','2014-04-11 16:12:33',NULL,'A'),(1295,'d005','product_price','2014-04-11 16:12:49',NULL,'A'),(1296,'d006','product_price','2014-04-11 16:13:09',NULL,'A'),(1297,'d007','product_price','2014-04-11 16:13:38',NULL,'A'),(1298,'d008','product_price','2014-04-11 16:13:55',NULL,'A'),(1299,'dustbins_1','product','2014-04-11 16:13:58',NULL,'A'),(1301,'fb001','product_price','2014-04-11 16:18:40',NULL,'A'),(1302,'fb002','product_price','2014-04-11 16:19:11',NULL,'A'),(1303,'fb003','product_price','2014-04-11 16:19:46',NULL,'A'),(1304,'fb004','product_price','2014-04-11 16:20:23',NULL,'A'),(1305,'fb005','product_price','2014-04-11 16:20:43',NULL,'A'),(1306,'flower_boxes_1','product','2014-04-11 16:20:48',NULL,'A'),(1308,'dk001','product_price','2014-04-11 16:33:25',NULL,'A'),(1309,'dk002','product_price','2014-04-11 16:33:40',NULL,'A'),(1310,'dk003','product_price','2014-04-11 16:33:59',NULL,'A'),(1311,'decking_2','product','2014-04-11 16:34:02',NULL,'A'),(1313,'pv12','product_price','2014-04-11 16:37:03',NULL,'A'),(1314,'pavilions','product','2014-04-11 16:37:16',NULL,'A'),(1316,'p001','product_price','2014-04-11 16:41:26',NULL,'A'),(1317,'p002','product_price','2014-04-11 16:41:46',NULL,'A'),(1318,'p003','product_price','2014-04-11 16:42:39',NULL,'A'),(1319,'p004','product_price','2014-04-11 16:43:27',NULL,'A'),(1321,'p001_1','product_price','2014-04-11 16:45:08',NULL,'A'),(1322,'p002_1','product_price','2014-04-11 16:45:25',NULL,'A'),(1323,'p003_1','product_price','2014-04-11 16:45:42',NULL,'A'),(1324,'p004_1','product_price','2014-04-11 16:46:06',NULL,'A'),(1325,'p007','product_price','2014-04-11 16:46:33',NULL,'A'),(1326,'p008','product_price','2014-04-11 16:47:09',NULL,'A'),(1327,'p009','product_price','2014-04-11 16:47:33',NULL,'A'),(1328,'p010','product_price','2014-04-11 16:48:02',NULL,'A'),(1329,'pallets_2','product','2014-04-11 16:48:05',NULL,'A'),(1331,'j001','product_price','2014-04-11 16:57:32',NULL,'A'),(1332,'j002','product_price','2014-04-11 16:57:55',NULL,'A'),(1333,'r001','product_price','2014-04-11 16:58:11',NULL,'A'),(1334,'st001','product_price','2014-04-11 16:58:33',NULL,'A'),(1335,'c001','product_price','2014-04-11 16:58:55',NULL,'A'),(1336,'dr001','product_price','2014-04-11 16:59:27',NULL,'A'),(1337,'b023_1','product_price','2014-04-11 16:59:50',NULL,'A'),(1338,'mb001','product_price','2014-04-11 17:00:10',NULL,'A'),(1339,'mb002','product_price','2014-04-11 17:00:36',NULL,'A'),(1340,'sl001','product_price','2014-04-11 17:01:03',NULL,'A'),(1341,'sl002','product_price','2014-04-11 17:01:28',NULL,'A'),(1342,'sl003','product_price','2014-04-11 17:01:43',NULL,'A'),(1343,'sl004','product_price','2014-04-11 17:02:12',NULL,'A'),(1344,'sl005','product_price','2014-04-11 17:02:41',NULL,'A'),(1345,'rr001','product_price','2014-04-11 17:03:25',NULL,'A'),(1346,'fp001','product_price','2014-04-11 17:03:43',NULL,'A'),(1347,'sand1','product_price','2014-04-11 17:04:13',NULL,'A'),(1348,'swing1','product_price','2014-04-11 17:04:38',NULL,'A'),(1349,'swing2','product_price','2014-04-11 17:04:53',NULL,'A'),(1350,'inst','product_price','2014-04-11 17:05:10',NULL,'A'),(1351,'jungle_gyms_1','product','2014-04-11 17:05:19',NULL,'A'),(1353,'plk001','product_price','2014-04-11 17:10:40',NULL,'A'),(1354,'plk002','product_price','2014-04-11 17:10:59',NULL,'A'),(1355,'plk003','product_price','2014-04-11 17:11:21',NULL,'A'),(1356,'plk004','product_price','2014-04-11 17:11:52',NULL,'A'),(1357,'plk005','product_price','2014-04-11 17:12:23',NULL,'A'),(1358,'plk006','product_price','2014-04-11 17:12:48',NULL,'A'),(1359,'plk007','product_price','2014-04-11 17:13:21',NULL,'A'),(1360,'plk008','product_price','2014-04-11 17:13:57',NULL,'A'),(1361,'plk009','product_price','2014-04-11 17:14:15',NULL,'A'),(1362,'plk010','product_price','2014-04-11 17:14:37',NULL,'A'),(1363,'plk012','product_price','2014-04-11 17:15:10',NULL,'A'),(1364,'plk013','product_price','2014-04-11 17:15:46',NULL,'A'),(1365,'plk014','product_price','2014-04-11 17:16:25',NULL,'A'),(1366,'plk015','product_price','2014-04-11 17:17:00',NULL,'A'),(1367,'plk016','product_price','2014-04-11 17:17:22',NULL,'A'),(1368,'plk017','product_price','2014-04-11 17:17:41',NULL,'A'),(1369,'planks_1','product','2014-04-11 17:17:44',NULL,'A'),(1371,'p018','product_price','2014-04-11 17:22:29',NULL,'A'),(1372,'p019','product_price','2014-04-11 17:24:16',NULL,'A'),(1373,'p020','product_price','2014-04-11 17:24:38',NULL,'A'),(1374,'p021','product_price','2014-04-11 17:24:55',NULL,'A'),(1375,'p022','product_price','2014-04-11 17:25:41',NULL,'A'),(1376,'p023','product_price','2014-04-11 17:26:02',NULL,'A'),(1377,'p024','product_price','2014-04-11 17:26:21',NULL,'A'),(1378,'p025','product_price','2014-04-11 17:26:48',NULL,'A'),(1379,'p026','product_price','2014-04-11 17:27:17',NULL,'A'),(1380,'poles_1','product','2014-04-11 17:27:38',NULL,'A'),(1382,'p027','product_price','2014-04-11 17:37:56',NULL,'A'),(1383,'p028','product_price','2014-04-11 17:38:25',NULL,'A'),(1384,'p029','product_price','2014-04-11 17:38:51',NULL,'A'),(1385,'p030','product_price','2014-04-11 17:39:20',NULL,'A'),(1386,'p031','product_price','2014-04-11 17:39:54',NULL,'A'),(1387,'made_to_order_planks','product','2014-04-11 17:39:58',NULL,'A'),(1390,'tree','form_action','2014-06-19 10:08:07',NULL,'A'),(1391,'tree_1','form_action','2014-06-19 10:19:59',NULL,'A'),(1392,'tag','form_row','2014-06-20 16:33:09',NULL,'A'),(1453,'images','form_link','2014-08-05 15:14:02',NULL,'A'),(1473,'thumbnail_4','form_link','2014-08-06 20:53:51','2014-08-06 20:57:24','A'),(1474,'thumbnail_one','thumbnail','2014-08-06 20:55:04',NULL,'A'),(1483,'dgdfgdfgdfgd','beer','2014-08-07 13:58:09',NULL,'A'),(1484,'clone_2','form_action','2014-08-11 10:42:44',NULL,'A'),(1486,'slide_left_1','form_link','2014-08-11 10:45:48',NULL,'A'),(1487,'bridge','thumbnail','2014-08-11 16:49:54',NULL,'A'),(1488,'__blank__','category','2014-08-11 20:08:09',NULL,'A'),(1489,'koala','thumbnail','2014-08-11 20:30:03',NULL,'A'),(1490,'penguins','thumbnail','2014-08-11 20:30:33',NULL,'A'),(1493,'thumbnail_image','thumbnail','2014-08-15 10:14:18',NULL,'A'),(1496,'welcome_to_clearwater','category','2014-08-15 17:10:22','2014-09-24 22:28:44','A'),(1498,'television_1','category','2014-08-15 17:15:13','2014-09-24 22:27:22','A'),(1500,'design','category','2014-08-15 17:30:35','2014-09-24 22:20:42','A'),(1502,'motion','category','2014-08-15 18:03:05','2014-09-24 22:23:02','A'),(1504,'online_mobile','category','2014-08-15 18:05:22','2014-09-24 22:24:21','A'),(1506,'connecting_people_to_content','category','2014-08-15 18:08:40','2014-09-23 11:49:04','A'),(1508,'social_media','category','2014-08-15 18:10:51','2014-09-24 22:26:17','A'),(1510,'strategy','category','2014-08-15 18:21:41','2014-09-23 11:51:46','A'),(1512,'integrated_campaign','category','2014-08-15 18:24:20','2014-09-24 22:21:44','A'),(1514,'the_cw_experience','category','2014-08-15 18:28:34','2014-09-24 22:27:56','A'),(1516,'production_facilitation','category','2014-08-15 18:30:36','2014-09-24 22:24:52','A'),(1518,'working_with_sa','category','2014-08-15 18:33:11','2014-09-24 22:29:49','A'),(1520,'africa_is_not_a_country','category','2014-08-15 18:35:04','2014-09-24 22:17:27','A'),(1522,'credentials','category','2014-08-15 18:37:13','2014-09-24 22:18:44','A'),(1524,'art','category','2014-08-15 18:39:41','2014-09-23 11:48:51','A'),(1526,'recruitment','category','2014-08-15 18:41:37','2014-09-24 22:25:43','A'),(1528,'contact_and_map_1','category','2014-08-15 18:43:49','2014-09-25 05:47:10','A'),(1529,'video','thumbnail','2014-08-15 19:11:28',NULL,'A'),(1530,'bourne_ultimatum_movie_promo_1','project','2014-08-15 19:13:49',NULL,'A'),(1531,'video_thumb_35','thumbnail','2014-08-15 19:20:53',NULL,'A'),(1532,'californication_season_launch_preview_teaser','project','2014-08-15 19:20:55',NULL,'A'),(1533,'video_thumb_28','thumbnail','2014-08-15 19:22:06',NULL,'A'),(1534,'dexter_season_3_launch_promo','project','2014-08-15 19:22:07',NULL,'A'),(1535,'thumbnail_5','thumbnail','2014-08-15 19:25:54',NULL,'A'),(1536,'greys_anatomy_season_launch_promo','project','2014-08-15 19:25:55',NULL,'A'),(1537,'thumbnail_6','thumbnail','2014-08-15 19:27:32',NULL,'A'),(1538,'greys_anatomy_heart_recap','project','2014-08-15 19:27:34',NULL,'A'),(1539,'thumbnail_7','thumbnail','2014-08-15 19:28:36',NULL,'A'),(1540,'greys_anatomy_heart_tease','project','2014-08-15 19:28:38',NULL,'A'),(1541,'thumbnail_8','thumbnail','2014-08-15 19:29:34',NULL,'A'),(1542,'gossip_girl_hollywood','project','2014-08-15 19:29:36','2014-08-15 19:50:01','A'),(1543,'thumbnail_9','thumbnail','2014-08-15 19:30:24',NULL,'A'),(1544,'pacman','project','2014-08-15 19:30:25',NULL,'A'),(1545,'thumbnail_10','thumbnail','2014-08-15 19:31:04',NULL,'A'),(1546,'iron_man_2_movie_promo','project','2014-08-15 19:31:23',NULL,'A'),(1547,'thumbnail_11','thumbnail','2014-08-15 19:32:34',NULL,'A'),(1548,'knight_and_day_movie_promo','project','2014-08-15 19:32:36',NULL,'A'),(1549,'thumbnail_12','thumbnail','2014-08-15 19:33:37',NULL,'A'),(1550,'mnet_fully_loaded_fridays','project','2014-08-15 19:33:48',NULL,'A'),(1551,'thumbnail_13','thumbnail','2014-08-15 19:37:00',NULL,'A'),(1552,'terra_nova_show_launch_promo','project','2014-08-15 19:37:56',NULL,'A'),(1553,'thumbnail_14','thumbnail','2014-08-15 19:38:50',NULL,'A'),(1554,'the_departed_movie_promo','project','2014-08-15 19:38:55',NULL,'A'),(1555,'thumbnail_15','thumbnail','2014-08-15 19:39:36',NULL,'A'),(1556,'transformers_movie_teaser','project','2014-08-15 19:39:38',NULL,'A'),(1557,'thumbnail_16','thumbnail','2014-08-15 19:40:28',NULL,'A'),(1558,'ugly_betty_halloween','project','2014-08-15 19:40:29',NULL,'A'),(1559,'thumbnail_17','thumbnail','2014-08-15 19:41:28',NULL,'A'),(1560,'xfactor_season_launch_promo','project','2014-08-15 19:41:29',NULL,'A'),(1561,'thumbnail_18','thumbnail','2014-08-15 19:51:22',NULL,'A'),(1562,'mnet_animation_and_imagination_festival','project','2014-08-15 19:51:24',NULL,'A'),(1563,'thumbnail_19','thumbnail','2014-08-15 19:55:27',NULL,'A'),(1564,'celebrating_oscar','project','2014-08-15 19:55:29',NULL,'A'),(1565,'thumbnail_20','thumbnail','2014-08-15 19:56:21',NULL,'A'),(1566,'mnet_movies_bestofbritish_festival','project','2014-08-15 19:56:23',NULL,'A'),(1567,'thumbnail_21','thumbnail','2014-08-18 12:41:13',NULL,'A'),(1569,'thumbnail_22','thumbnail','2014-08-18 12:42:18',NULL,'A'),(1571,'thumbnail_23','thumbnail','2014-08-18 12:58:26',NULL,'A'),(1572,'mnet_valentines_themepiece','project','2014-08-18 12:58:28',NULL,'A'),(1573,'thumbnail_24','thumbnail','2014-08-18 12:59:15',NULL,'A'),(1574,'ktv_love_quest_valentines_promo','project','2014-08-18 12:59:16',NULL,'A'),(1575,'thumbnail_25','thumbnail','2014-08-18 12:59:44',NULL,'A'),(1576,'winter_sensations','project','2014-08-18 13:24:02',NULL,'A'),(1577,'thumbnail_26','thumbnail','2014-08-18 13:32:42',NULL,'A'),(1578,'mnet_love_your_couch','project','2014-08-18 13:32:43',NULL,'A'),(1579,'thumbnail_27','thumbnail','2014-08-18 13:33:31',NULL,'A'),(1580,'mnet_thunderstruck','project','2014-08-18 13:33:33',NULL,'A'),(1581,'thumbnail_28','thumbnail','2014-08-18 13:34:20',NULL,'A'),(1582,'mnet_wildboys','project','2014-08-18 13:34:30',NULL,'A'),(1583,'thumbnail_29','thumbnail','2014-08-18 13:35:36',NULL,'A'),(1584,'mnet_movies_liftoff','project','2014-08-18 13:35:51',NULL,'A'),(1585,'thumbnail_30','thumbnail','2014-08-18 13:48:33',NULL,'A'),(1586,'mnet_believe_in_2012','project','2014-08-18 13:48:38',NULL,'A'),(1587,'thumbnail_31','thumbnail','2014-08-18 13:49:33',NULL,'A'),(1588,'mnet_believe','project','2014-08-18 13:49:37',NULL,'A'),(1589,'thumbnail_32','thumbnail','2014-08-18 13:50:18',NULL,'A'),(1590,'mnet_june_movies','project','2014-08-18 13:50:19',NULL,'A'),(1591,'thumbnail_33','thumbnail','2014-08-18 13:54:34',NULL,'A'),(1592,'mnet_everyday_is_an_experience','project','2014-08-18 13:54:38',NULL,'A'),(1593,'thumbnail_34','thumbnail','2014-08-18 13:55:16',NULL,'A'),(1594,'mnet_movies_zone_uninterrupted','project','2014-08-18 13:55:18',NULL,'A'),(1596,'thumbnail_35','thumbnail','2014-08-18 13:58:51',NULL,'A'),(1597,'turnup__a_daily_music_and_lifestyle_show','project','2014-08-18 13:59:11',NULL,'A'),(1598,'thumbnail_36','thumbnail','2014-08-18 14:11:18',NULL,'A'),(1600,'poster_design_and_sneaker_competition_entry','project','2014-08-18 14:11:42',NULL,'A'),(1602,'thumbnail_37','thumbnail','2014-08-18 14:12:41',NULL,'A'),(1603,'spaz_and_fan_character_design','project','2014-08-18 14:12:43',NULL,'A'),(1605,'thumbnail_38','thumbnail','2014-08-18 14:13:38',NULL,'A'),(1606,'show_endboard_for_weeds','project','2014-08-18 14:13:40',NULL,'A'),(1608,'thumbnail_39','thumbnail','2014-08-18 14:14:54',NULL,'A'),(1609,'womens_channel_imaging_proposal','project','2014-08-18 14:14:56',NULL,'A'),(1611,'thumbnail_40','thumbnail','2014-08-18 14:15:48',NULL,'A'),(1612,'zoo_city_film_proposal','project','2014-08-18 14:15:50',NULL,'A'),(1614,'thumbnail_41','thumbnail','2014-08-18 14:16:55',NULL,'A'),(1615,'title_design_for_a_crime_show_themepiece','project','2014-08-18 14:16:57',NULL,'A'),(1617,'thumbnail_42','thumbnail','2014-08-18 14:18:21',NULL,'A'),(1618,'mnet_friends_social_media_campaign','project','2014-08-18 14:18:23',NULL,'A'),(1620,'thumbnail_43','thumbnail','2014-08-18 14:19:22',NULL,'A'),(1621,'promo_title_design_for_an_action_movie_festival','project','2014-08-18 14:19:30',NULL,'A'),(1623,'thumbnail_44','thumbnail','2014-08-18 14:20:38',NULL,'A'),(1624,'show_opener_proposal_for_a_reality_show','project','2014-08-18 14:20:39',NULL,'A'),(1625,'thumbnail_45','thumbnail','2014-08-18 14:49:24',NULL,'A'),(1626,'channel_o_mva_opening_titles','project','2014-08-18 14:49:34',NULL,'A'),(1627,'thumbnail_46','thumbnail','2014-08-18 14:50:43',NULL,'A'),(1628,'channel_o_mva_opening_titles_2','project','2014-08-18 14:50:44',NULL,'A'),(1629,'thumbnail_47','thumbnail','2014-08-18 14:52:12',NULL,'A'),(1630,'colombiana_countdown','project','2014-08-18 14:52:14',NULL,'A'),(1631,'thumbnail_48','thumbnail','2014-08-18 14:53:02',NULL,'A'),(1632,'mnet_movies_ident','project','2014-08-18 14:53:05',NULL,'A'),(1633,'thumbnail_49','thumbnail','2014-08-18 14:54:16',NULL,'A'),(1634,'mnet_movies_filler__the_matrix','project','2014-08-18 14:54:17',NULL,'A'),(1635,'thumbnail_50','thumbnail','2014-08-18 14:55:02',NULL,'A'),(1636,'mtv_africa_music_awards_opening_titles','project','2014-08-18 14:55:04',NULL,'A'),(1637,'thumbnail_51','thumbnail','2014-08-18 14:58:26',NULL,'A'),(1638,'mtv_africa_music_awards_tease','project','2014-08-18 14:58:29',NULL,'A'),(1639,'thumbnail_52','thumbnail','2014-08-18 14:59:04',NULL,'A'),(1640,'siya','project','2014-08-18 14:59:06','2014-09-24 22:33:46','A'),(1641,'thumbnail_53','thumbnail','2014-08-18 15:00:28',NULL,'A'),(1642,'mtv_ema_pitch','project','2014-08-18 15:00:30',NULL,'A'),(1643,'thumbnail_54','thumbnail','2014-08-18 15:01:02',NULL,'A'),(1644,'breathing','project','2014-08-18 15:01:04',NULL,'A'),(1645,'thumbnail_55','thumbnail','2014-08-18 15:01:43',NULL,'A'),(1646,'mnet_lucky_tuesdays','project','2014-08-18 15:01:45',NULL,'A'),(1647,'thumbnail_56','thumbnail','2014-08-18 15:02:20',NULL,'A'),(1648,'mad_men_season_launch_promo','project','2014-08-18 15:02:21',NULL,'A'),(1649,'thumbnail_57','thumbnail','2014-08-18 15:04:51',NULL,'A'),(1650,'gary_the_toothfairy_part_1','project','2014-08-18 15:04:57',NULL,'A'),(1651,'thumbnail_58','thumbnail','2014-08-18 15:05:36',NULL,'A'),(1652,'gary_the_toothfairy_part_2','project','2014-08-18 15:05:38',NULL,'A'),(1653,'thumb','thumbnail','2014-08-18 15:06:22',NULL,'A'),(1654,'gary_the_toothfairy_part_3','project','2014-08-18 15:06:24',NULL,'A'),(1655,'thumb_1','thumbnail','2014-08-18 15:07:04',NULL,'A'),(1656,'gary_the_toothfairy_part_4','project','2014-08-18 15:07:08',NULL,'A'),(1657,'thumb_2','thumbnail','2014-08-18 15:07:39',NULL,'A'),(1658,'homeland_teaser','project','2014-08-18 15:07:41',NULL,'A'),(1659,'thumb_3','thumbnail','2014-08-18 15:08:16',NULL,'A'),(1660,'homeland_launch','project','2014-08-18 15:08:18',NULL,'A'),(1661,'thumb_4','thumbnail','2014-08-18 15:09:02',NULL,'A'),(1662,'isibaya_season','project','2014-08-18 15:09:21',NULL,'A'),(1663,'thumb_5','thumbnail','2014-08-18 15:09:59',NULL,'A'),(1664,'vampire_tease','project','2014-08-18 15:10:00',NULL,'A'),(1665,'thumb_6','thumbnail','2014-08-18 15:10:36',NULL,'A'),(1666,'vampire_code','project','2014-08-18 15:10:37',NULL,'A'),(1668,'derek_test','category','2014-08-31 07:02:17','2014-09-25 06:15:39','A'),(1685,'__main__','site','2014-09-23 11:35:20',NULL,'A'),(1696,'thumbnail_59','thumbnail','2014-09-25 06:30:58',NULL,'A'),(1697,'dylan_test_project','project','2014-09-25 06:31:03',NULL,'A'),(1698,'dylan_test_category','category','2014-09-25 06:33:26','2014-09-26 14:59:00','A'),(1701,'thumbanil','thumbnail','2014-09-25 09:26:21',NULL,'A'),(1702,'thumbanil_1','thumbnail','2014-09-25 09:26:32',NULL,'A'),(1703,'project_name','project','2014-09-25 09:26:36',NULL,'A'),(1704,'reggie','category','2014-09-25 09:34:55',NULL,'A'),(1837,'dylan','site','2014-10-11 18:10:55',NULL,'A'),(1838,'welcome_to_clearwater_clone_4','category','2014-10-11 18:11:07',NULL,'A'),(1839,'contact_and_map_clone','category','2014-10-11 18:11:07',NULL,'A'),(1841,'welcome_to_clearwater_clone_5','category','2014-10-12 16:17:52',NULL,'A'),(1842,'contact_and_map_clone_1','category','2014-10-12 16:17:52',NULL,'A'),(1844,'recruitment_clone','category','2014-10-12 16:27:55',NULL,'A'),(1845,'social_media_clone','category','2014-10-12 16:27:55',NULL,'A'),(1846,'strategy_clone','category','2014-10-12 16:27:55',NULL,'A'),(1864,'welcome_to_clearwater_clone_clone','category','2014-10-13 08:18:12',NULL,'A'),(1865,'contact_and_map_clone_clone','category','2014-10-13 08:18:12',NULL,'A'),(1866,'social_media_clone_clone','category','2014-10-13 08:18:13',NULL,'A'),(1916,'lifetime_1','site','2014-10-14 08:51:43',NULL,'A'),(1917,'username','form_row','2014-10-17 14:02:28',NULL,'A');
/*!40000 ALTER TABLE `entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_link`
--

DROP TABLE IF EXISTS `entity_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_link` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent` mediumint(8) unsigned NOT NULL,
  `child` mediumint(8) unsigned NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`,`child`)
) ENGINE=InnoDB AUTO_INCREMENT=4468 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_link`
--

LOCK TABLES `entity_link` WRITE;
/*!40000 ALTER TABLE `entity_link` DISABLE KEYS */;
INSERT INTO `entity_link` VALUES (22,65,66,1),(25,71,72,1),(26,71,73,2),(28,69,71,1),(29,69,75,1),(31,70,94,2),(38,94,82,2),(39,184,84,1),(40,184,85,2),(41,94,86,3),(42,94,87,4),(43,94,88,5),(44,94,89,6),(45,94,90,7),(46,94,91,8),(47,94,92,9),(50,65,111,1),(52,65,113,2),(104,70,184,2),(105,65,185,9),(147,264,279,1),(148,264,280,2),(149,264,281,3),(150,264,282,4),(151,264,283,5),(152,264,284,6),(153,264,285,7),(154,263,264,1),(211,65,354,3),(248,395,396,1),(249,395,397,2),(250,395,398,3),(251,395,399,4),(252,395,400,5),(253,70,395,1),(254,263,401,1),(255,262,263,1),(256,68,69,1),(257,264,403,8),(258,404,64,1),(259,407,405,1),(260,407,406,2),(261,411,408,1),(262,411,409,2),(263,411,410,3),(270,442,440,1),(271,442,441,2),(272,447,443,1),(273,447,444,2),(274,447,445,3),(275,447,446,4),(276,447,442,5),(299,471,460,1),(300,472,447,1),(306,263,479,1),(307,263,485,1),(308,263,486,2),(309,263,487,3),(310,263,488,4),(314,411,490,1),(315,411,491,2),(316,411,492,3),(319,465,461,1),(321,451,461,1),(328,395,498,10),(342,493,452,1),(345,461,515,1),(397,552,545,1),(398,461,555,1),(399,475,555,2),(401,461,556,1),(402,475,556,2),(404,461,557,1),(405,475,557,2),(407,461,558,1),(408,475,558,2),(409,493,558,3),(412,567,562,1),(413,567,563,2),(414,567,564,3),(415,567,565,4),(416,567,566,5),(462,607,568,1),(469,615,568,1),(481,461,623,1),(483,461,624,1),(485,461,625,1),(487,461,627,1),(541,461,637,1),(542,475,637,2),(554,461,641,1),(557,461,642,1),(560,447,641,1),(561,447,642,2),(563,461,443,1),(565,461,444,1),(567,461,445,1),(569,461,446,1),(575,475,642,1),(577,461,643,1),(580,461,644,1),(582,461,645,1),(584,461,646,1),(586,461,647,1),(588,461,648,1),(589,69,644,1),(590,69,645,2),(591,69,646,3),(592,69,647,4),(593,69,648,5),(595,461,649,1),(596,69,649,1),(598,461,650,1),(599,263,650,1),(601,461,651,1),(603,461,652,1),(605,461,653,1),(607,461,654,1),(609,461,655,1),(611,461,656,1),(612,70,651,1),(613,70,652,2),(614,70,653,3),(615,70,654,4),(616,70,655,5),(617,70,656,6),(620,461,659,1),(622,661,660,1),(625,94,663,1),(626,667,665,1),(627,667,666,2),(632,461,674,1),(642,461,677,1),(643,475,677,2),(649,461,679,1),(650,447,679,1),(793,751,749,1),(795,753,749,1),(797,756,754,1),(842,461,479,1),(843,461,485,1),(844,461,486,1),(845,461,487,1),(846,461,488,1),(852,461,825,1),(853,828,826,1),(854,828,827,2),(856,461,829,1),(858,461,830,1),(859,833,831,1),(860,833,832,2),(1224,1159,1141,2),(1225,1159,1155,3),(1226,1159,1156,4),(1227,1159,1157,5),(1228,1159,1158,6),(1230,1165,1140,2),(1231,1165,1161,3),(1232,1165,1162,4),(1233,1165,1163,5),(1234,1165,1164,6),(1236,1171,1139,2),(1237,1171,1167,3),(1238,1171,1168,4),(1239,1171,1169,5),(1240,1171,1170,6),(1242,1174,1139,2),(1243,1174,1173,3),(1245,1180,1139,2),(1246,1180,1176,3),(1247,1180,1177,4),(1248,1180,1178,5),(1249,1180,1179,6),(1251,1184,1141,2),(1252,1184,1182,3),(1253,1184,1183,4),(1255,1187,1139,2),(1256,1187,1186,3),(1258,1190,1141,2),(1259,1190,1189,3),(1261,1193,1139,2),(1262,1193,1192,3),(1264,1196,1139,2),(1265,1196,1195,3),(1267,1200,1139,2),(1268,1200,1198,3),(1269,1200,1199,4),(1271,1205,1141,2),(1272,1205,1203,3),(1273,1205,1204,4),(1279,1209,1139,2),(1280,1209,1208,3),(1282,1214,1141,2),(1283,1214,1211,3),(1284,1214,1212,4),(1285,1214,1213,5),(1287,1227,1141,2),(1288,1227,1220,3),(1289,1227,1221,4),(1290,1227,1222,5),(1291,1227,1223,6),(1292,1227,1224,7),(1293,1227,1225,8),(1294,1227,1226,9),(1296,1235,1141,2),(1297,1235,1229,3),(1298,1235,1230,4),(1299,1235,1231,5),(1300,1235,1232,6),(1301,1235,1233,7),(1302,1235,1234,8),(1304,1240,1141,2),(1305,1240,1237,3),(1306,1240,1238,4),(1307,1240,1239,5),(1309,1247,1141,2),(1310,1247,1246,3),(1312,1251,1141,2),(1313,1251,1250,3),(1315,1256,1142,2),(1316,1256,1253,3),(1317,1256,1254,4),(1318,1256,1255,5),(1320,1259,1139,2),(1321,1259,1258,3),(1323,1265,1140,2),(1324,1265,1261,3),(1325,1265,1262,4),(1326,1265,1263,5),(1327,1265,1264,6),(1329,1273,1141,2),(1330,1273,1267,3),(1331,1273,1268,4),(1332,1273,1269,5),(1333,1273,1270,6),(1334,1273,1271,7),(1335,1273,1272,8),(1337,1278,1139,2),(1338,1278,1275,3),(1339,1278,1276,4),(1340,1278,1277,5),(1342,1284,1139,2),(1343,1284,1280,3),(1344,1284,1282,4),(1345,1284,1283,5),(1347,1289,1138,2),(1348,1289,1286,3),(1349,1289,1287,4),(1350,1289,1288,5),(1352,1299,1139,2),(1353,1299,1291,3),(1354,1299,1292,4),(1355,1299,1293,5),(1356,1299,1294,6),(1357,1299,1295,7),(1358,1299,1296,8),(1359,1299,1297,9),(1360,1299,1298,10),(1362,1306,1139,2),(1363,1306,1301,3),(1364,1306,1302,4),(1365,1306,1303,5),(1366,1306,1304,6),(1367,1306,1305,7),(1369,1311,1138,2),(1370,1311,1308,3),(1371,1311,1309,4),(1372,1311,1310,5),(1374,1314,1138,2),(1375,1314,1313,3),(1377,1329,1143,2),(1378,1329,1321,3),(1379,1329,1322,4),(1380,1329,1323,5),(1381,1329,1324,6),(1382,1329,1325,7),(1383,1329,1326,8),(1384,1329,1327,9),(1385,1329,1328,10),(1387,1351,1140,2),(1388,1351,1331,3),(1389,1351,1332,4),(1390,1351,1333,5),(1391,1351,1334,6),(1392,1351,1335,7),(1393,1351,1336,8),(1394,1351,1337,9),(1395,1351,1338,10),(1396,1351,1339,11),(1397,1351,1340,12),(1398,1351,1341,13),(1399,1351,1342,14),(1400,1351,1343,15),(1401,1351,1344,16),(1402,1351,1345,17),(1403,1351,1346,18),(1404,1351,1347,19),(1405,1351,1348,20),(1406,1351,1349,21),(1407,1351,1350,22),(1409,1369,1143,2),(1410,1369,1353,3),(1411,1369,1354,4),(1412,1369,1355,5),(1413,1369,1356,6),(1414,1369,1357,7),(1415,1369,1358,8),(1416,1369,1359,9),(1417,1369,1360,10),(1418,1369,1361,11),(1419,1369,1362,12),(1420,1369,1363,13),(1421,1369,1364,14),(1422,1369,1365,15),(1423,1369,1366,16),(1424,1369,1367,17),(1425,1369,1368,18),(1427,1380,1143,2),(1428,1380,1371,3),(1429,1380,1372,4),(1430,1380,1373,5),(1431,1380,1374,6),(1432,1380,1375,7),(1433,1380,1376,8),(1434,1380,1377,9),(1435,1380,1378,10),(1436,1380,1379,11),(1438,1387,1143,2),(1439,1387,1382,3),(1440,1387,1383,4),(1441,1387,1384,5),(1442,1387,1385,6),(1443,1387,1386,7),(1571,1453,568,1),(1716,461,1484,1),(2151,1530,1529,1),(2159,1534,1533,1),(2168,1536,1535,1),(2169,1538,1537,1),(2170,1540,1539,1),(2172,1544,1543,1),(2173,1546,1545,1),(2174,1548,1547,1),(2175,1550,1549,1),(2176,1532,1531,1),(2177,1552,1551,1),(2178,1554,1553,1),(2179,1556,1555,1),(2180,1558,1557,1),(2181,1560,1559,1),(2203,1542,1541,1),(2204,1562,1561,1),(2227,1564,1563,1),(2228,1566,1565,1),(2255,1572,1571,1),(2256,1574,1573,1),(2257,1576,1575,1),(2258,1578,1577,1),(2259,1580,1579,1),(2260,1582,1581,1),(2261,1584,1583,1),(2293,1586,1585,1),(2294,1588,1587,1),(2295,1590,1589,1),(2296,1592,1591,1),(2297,1594,1593,1),(2299,1597,1596,2),(2343,1600,1598,2),(2345,1603,1602,2),(2347,1606,1605,2),(2349,1609,1608,2),(2351,1612,1611,2),(2353,1615,1614,2),(2355,1618,1617,2),(2357,1621,1620,2),(2359,1624,1623,2),(2375,1626,1625,1),(2376,1628,1627,1),(2377,1630,1629,1),(2378,1632,1631,1),(2379,1634,1633,1),(2380,1636,1635,1),(2381,1638,1637,1),(2383,1642,1641,1),(2384,1644,1643,1),(2385,1646,1645,1),(2386,1648,1647,1),(2404,1650,1649,1),(2405,1652,1651,1),(2406,1654,1653,1),(2407,1656,1655,1),(2408,1658,1657,1),(2409,1660,1659,1),(2410,1662,1661,1),(2411,1664,1663,1),(2412,1666,1665,1),(2891,561,559,1),(2892,561,1392,2),(2893,561,560,3),(3081,1524,1522,1),(3082,1524,1526,2),(3083,1524,1522,3),(3084,1524,1526,4),(3086,1524,1685,1),(3190,1510,1508,1),(3191,1510,1512,2),(3192,1510,1508,3),(3193,1510,1512,4),(3195,1510,1685,1),(3376,1500,1498,1),(3377,1500,1502,2),(3378,1500,1498,3),(3379,1500,1502,4),(3380,1500,1597,1),(3381,1500,1600,2),(3382,1500,1603,3),(3383,1500,1606,4),(3384,1500,1609,5),(3385,1500,1612,6),(3386,1500,1615,7),(3387,1500,1618,8),(3388,1500,1621,9),(3389,1500,1624,10),(3391,1500,1685,1),(3395,1512,1510,1),(3396,1512,1514,2),(3397,1512,1510,3),(3398,1512,1514,4),(3399,1512,1650,1),(3400,1512,1652,2),(3401,1512,1654,3),(3402,1512,1656,4),(3403,1512,1658,5),(3404,1512,1660,6),(3405,1512,1662,7),(3406,1512,1664,8),(3407,1512,1666,9),(3409,1512,1685,1),(3412,1502,1500,1),(3413,1502,1504,2),(3414,1502,1500,3),(3415,1502,1504,4),(3416,1502,1626,1),(3417,1502,1628,2),(3418,1502,1630,3),(3419,1502,1632,4),(3420,1502,1634,5),(3421,1502,1636,6),(3422,1502,1638,7),(3423,1502,1640,8),(3424,1502,1642,9),(3425,1502,1644,10),(3426,1502,1646,11),(3427,1502,1648,12),(3429,1502,1685,1),(3432,1504,1502,1),(3433,1504,1506,2),(3434,1504,1502,3),(3435,1504,1506,4),(3437,1504,1685,1),(3440,1516,1514,1),(3441,1516,1518,2),(3442,1516,1514,3),(3443,1516,1518,4),(3445,1516,1685,1),(3448,1526,1524,1),(3449,1526,1528,2),(3450,1526,1524,3),(3451,1526,1528,4),(3453,1526,1685,1),(3456,1508,1506,1),(3457,1508,1510,2),(3458,1508,1506,3),(3459,1508,1510,4),(3461,1508,1685,1),(3503,1514,1512,1),(3504,1514,1516,2),(3505,1514,1512,3),(3506,1514,1516,4),(3508,1514,1685,1),(3512,1496,1528,1),(3513,1496,1498,2),(3514,1496,1528,3),(3515,1496,1498,4),(3517,1496,1685,1),(3518,1518,1516,1),(3519,1518,1520,2),(3520,1518,1516,3),(3521,1518,1520,4),(3523,1518,1685,1),(3526,1640,1639,1),(3544,1668,1498,1),(3545,1668,1500,2),(3546,1668,1502,3),(3547,1668,1504,4),(3548,1668,1542,1),(3549,1668,1544,2),(3550,1668,1546,3),(3551,1668,1548,4),(3553,1668,1685,1),(3576,1697,1696,1),(3614,1703,1701,1),(3615,1704,1520,1),(3616,1704,1524,2),(3617,1704,1506,3),(3618,1704,1528,4),(3619,1704,1532,5),(3620,1704,1534,6),(3621,1704,1536,7),(3622,1704,1538,8),(3623,1704,1540,9),(3624,1704,1542,10),(3626,1704,1685,12),(3629,1520,1518,1),(3630,1520,1522,2),(3631,1520,1518,3),(3632,1520,1522,4),(3634,1520,1685,1),(3638,1506,1504,1),(3639,1506,1508,2),(3640,1506,1504,3),(3641,1506,1508,4),(3643,1506,1685,1),(3648,1528,1526,1),(3649,1528,1496,2),(3650,1528,1526,3),(3651,1528,1496,4),(3653,1528,1685,1),(3659,1522,1520,1),(3660,1522,1524,2),(3661,1522,1520,3),(3662,1522,1524,4),(3664,1522,1685,1),(3671,1498,1496,1),(3672,1498,1500,2),(3673,1498,1496,3),(3674,1498,1500,4),(3675,1498,1530,1),(3676,1498,1532,2),(3677,1498,1534,3),(3678,1498,1536,4),(3679,1498,1538,5),(3680,1498,1540,6),(3681,1498,1542,7),(3682,1498,1544,8),(3683,1498,1546,9),(3684,1498,1548,10),(3685,1498,1550,11),(3686,1498,1552,12),(3687,1498,1554,13),(3688,1498,1556,14),(3689,1498,1558,15),(3690,1498,1560,16),(3691,1498,1562,17),(3692,1498,1564,18),(3693,1498,1566,19),(3694,1498,1572,20),(3695,1498,1574,21),(3696,1498,1576,22),(3697,1498,1578,23),(3698,1498,1580,24),(3699,1498,1582,25),(3700,1498,1584,26),(3701,1498,1586,27),(3702,1498,1588,28),(3703,1498,1590,29),(3704,1498,1592,30),(3705,1498,1594,31),(3707,1498,1685,1),(3730,1698,1520,1),(3731,1698,1524,2),(3732,1698,1506,3),(3733,1698,1528,4),(3734,1698,1697,1),(3735,1698,1530,2),(3736,1698,1532,3),(3737,1698,1534,4),(3738,1698,1536,5),(3741,1698,1685,2),(3815,461,453,1),(3816,475,453,2),(3819,461,638,1),(3820,475,638,2),(4074,454,447,1),(4177,1837,1496,1),(4178,1837,1528,2),(4180,1838,1837,1),(4182,1839,1837,1),(4233,1863,1838,1),(4234,1863,1839,2),(4235,1863,1845,3),(4237,1864,1863,1),(4239,1865,1863,1),(4241,1866,1863,1),(4245,1868,1510,1),(4246,1868,1498,2),(4247,1868,1514,3),(4332,1913,1496,1),(4333,1913,1524,2),(4352,459,455,1),(4353,459,456,2),(4354,459,457,3),(4355,459,458,4),(4356,459,1917,5),(4359,462,461,1),(4361,461,466,1),(4363,461,876,1),(4365,461,1391,1),(4367,461,467,1),(4369,461,468,1),(4371,461,469,1),(4373,461,470,1),(4388,568,555,1),(4389,568,637,2),(4390,568,556,3),(4391,568,638,4),(4392,568,557,5),(4393,568,558,6),(4395,568,561,1),(4396,568,567,2),(4397,662,659,1),(4399,662,661,1),(4414,461,448,1),(4415,475,448,2),(4417,461,1390,1),(4419,461,450,1),(4420,475,450,2),(4422,461,449,1),(4423,475,449,2),(4425,461,451,1),(4427,461,465,1),(4428,475,465,2),(4430,621,568,1),(4432,494,493,1),(4433,64,466,1),(4434,64,876,2),(4435,64,1391,3),(4436,64,467,4),(4437,64,468,5),(4438,64,469,6),(4439,64,470,7),(4440,64,1484,8),(4442,64,404,1),(4443,64,262,2),(4444,64,68,3),(4445,64,65,1),(4446,413,515,1),(4447,413,623,2),(4448,413,643,3),(4449,413,624,4),(4450,413,625,5),(4451,413,627,6),(4452,413,471,1),(4453,413,472,2),(4454,413,407,1),(4455,413,411,2),(4456,413,667,3),(4457,460,448,1),(4458,460,674,2),(4459,460,1390,3),(4460,460,450,4),(4461,460,449,5),(4462,460,451,6),(4463,460,452,7),(4464,460,453,8),(4465,460,465,9),(4466,460,454,1),(4467,460,459,1);
/*!40000 ALTER TABLE `entity_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form`
--

DROP TABLE IF EXISTS `form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form` (
  `id` mediumint(8) unsigned NOT NULL,
  `form_module` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `url_column` varchar(64) DEFAULT NULL,
  `default_status` enum('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') NOT NULL DEFAULT 'A',
  `display` tinyint(4) NOT NULL DEFAULT '1',
  KEY `id` (`id`),
  CONSTRAINT `form_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form`
--

LOCK TABLES `form` WRITE;
/*!40000 ALTER TABLE `form` DISABLE KEYS */;
INSERT INTO `form` VALUES (64,'form','Form','name','A',1),(69,'form_group','Form Group','title','A',0),(70,'form_row','Form Row','label','A',0),(263,'form_link','Form Link','title','A',0),(413,'form_action','Form Action','action','A',0),(447,'user_group','User Group','title','A',0),(460,'user','User','email_address','A',1),(568,'image','Image','title','A',0),(662,'trash','Trash','trash_row','A',1);
/*!40000 ALTER TABLE `form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_action`
--

DROP TABLE IF EXISTS `form_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_action` (
  `id` mediumint(8) unsigned NOT NULL,
  `action` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `display_main_menu` tinyint(3) unsigned DEFAULT NULL,
  `display_module_menu` tinyint(3) unsigned DEFAULT NULL,
  `display_entity` tinyint(3) unsigned DEFAULT NULL,
  `main_menu_title` varchar(32) DEFAULT NULL,
  `module_menu_title` varchar(32) DEFAULT NULL,
  `entity_title` varchar(32) DEFAULT NULL,
  `allowed_statusses` varchar(255) DEFAULT NULL,
  `disallowed_statusses` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `form_action_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_action`
--

LOCK TABLES `form_action` WRITE;
/*!40000 ALTER TABLE `form_action` DISABLE KEYS */;
INSERT INTO `form_action` VALUES (420,'Create','Create a new User Group',0,0,0,NULL,NULL,NULL,'',''),(421,'Update','kjh',0,0,0,NULL,NULL,NULL,'',''),(443,'Create','Create a New Form Group',1,1,0,'New','New',NULL,'',''),(444,'Update','Update a User Group',0,0,1,NULL,NULL,'Update','A',''),(445,'Delete','Delete a User Group',0,0,1,NULL,NULL,'Delete','',''),(446,'Restore','Restore a User Group',0,0,1,NULL,NULL,'Restore','D',''),(448,'Create','Create a new User',1,1,0,'New','New',NULL,'',''),(449,'Delete','Delete a User',0,0,1,NULL,NULL,NULL,'',''),(450,'Update','Update a User',0,0,1,NULL,NULL,'Update','A',''),(451,'Restore','Restore a deleted User',0,0,1,NULL,NULL,'Restore','D',''),(452,'Login','Login to the system',0,0,0,NULL,NULL,NULL,'',''),(453,'Logout','Logout from the system',0,0,0,NULL,NULL,NULL,'',''),(465,'View','View Users',1,1,0,'All','All','','',''),(466,'Create','Create a Form',1,1,0,'New','New',NULL,'',''),(467,'View','View a Form',1,1,0,'All','All',NULL,'',''),(468,'Update','Update a Form',0,0,1,NULL,NULL,'Update','A',''),(469,'Delete','Delete a Form',0,0,1,NULL,NULL,NULL,'',''),(470,'Restore','Restore a Form',0,0,1,NULL,NULL,'Restore','D',''),(476,'Create','Create a Form Link',0,0,0,NULL,NULL,NULL,'',''),(477,'Create','Create a Form Link',0,0,0,NULL,NULL,NULL,'',''),(478,'Update','Update a Form Link',0,0,0,NULL,NULL,NULL,'',''),(479,'View','View a Form Link',0,0,0,NULL,NULL,NULL,'',''),(480,'Delete','Delete a Form Link',0,0,0,NULL,NULL,NULL,'',''),(485,'Create','Create a Form Link',0,0,0,NULL,NULL,NULL,'',''),(486,'Update','Update a Form Action',0,0,0,NULL,NULL,NULL,'A',''),(487,'Delete','Delete a Form Action',0,0,1,NULL,NULL,'Delete','',''),(488,'Restore','Restore a Form Action',0,0,1,NULL,NULL,'Restore','D',''),(515,'Create','Create a new Form Action',0,0,0,'','','','',''),(536,'Create','sdfsdfsd',1,0,0,'Create','sdfsdfs','sdfsdf','',''),(553,'dfgdfg','dfgdfg',1,0,0,'dfgdfgdf','','','',''),(555,'Create','Create a new Image',0,0,0,'','','','',''),(556,'Delete','Delete an Image',0,0,1,'','','Delete','',''),(557,'Restore','Restore an Image',0,0,1,'','','Restore','D',''),(558,'View','View an Image',1,1,0,'All','All','','',''),(623,'View','View a Form Action',0,0,0,'','All','','',''),(624,'Update','Update a Form Action',0,0,1,'','','Update','A',''),(625,'Restore','Restore a Form Action',0,0,1,'','','Restore','D',''),(627,'Link','Link a Form Action to another Form',0,0,0,'','','','',''),(637,'Update','Update an Image',0,0,1,'','','Update','A',''),(638,'Link','Link an Image to another Form',0,0,0,'','','','',''),(641,'View','View a User Group',1,1,0,'All','All','','',''),(642,'Link','Link a User Group to another Form',0,0,0,'','','','',''),(643,'Delete','Delete a Form Action',0,0,1,'','','Delete','',''),(644,'Create','Create a Form Group',0,0,0,'','','','',''),(645,'View','View a Form Group',0,0,0,'','','','',''),(646,'Update','Update a Form Group',0,0,1,'','','Update','A',''),(647,'Delete','Delete a Form Group',0,0,1,'','','Delete','',''),(648,'Restore','Restore a Form Group',0,0,1,'','','Restore','D',''),(649,'Link','Link a Form Group to another Form',0,0,0,'','','','',''),(650,'Link','Link a Form Link to another Form',0,0,0,'','','','',''),(651,'Create','Create a Form Row',0,0,0,'','','','',''),(652,'View','View a Form Row',0,0,0,'','','','',''),(653,'Update','Update a Form Row',0,0,1,'','','Update','A',''),(654,'Delete','Delete a Form Row',0,0,0,'','','','',''),(655,'Restore','Restore a Form Row',0,0,1,'','','Restore','D',''),(656,'Link','Link a Form Row to another Form',0,0,0,'','','','',''),(659,'View','View Trash',1,1,0,'All','All','','',''),(668,'Delete Me','please delete me',0,0,0,'','','','C','Z'),(670,'Deleeeete','dddd',0,0,0,'','','','C',NULL),(671,'del','del',0,0,0,'','','','B,C','A,B'),(674,'Clone','Clone the User to create a new one.',0,0,1,'','','Clone','A',NULL),(677,'Clone','Clone a Dealer.',0,0,1,'','','Clone','A',NULL),(679,'Clone','Clone a User Group',0,0,1,'','','Clone','A',NULL),(735,'Create','Create ',0,0,0,'','','',NULL,NULL),(825,'View','View',1,1,0,'All','All','',NULL,NULL),(829,'View','View Article',1,1,0,'All','All','',NULL,NULL),(830,'Create','Create',1,1,0,'New','New','',NULL,NULL),(876,'Link','Link a Form',0,0,0,'','','',NULL,NULL),(1390,'Tree','All relationships',0,0,1,'','','Tree',NULL,NULL),(1391,'Tree','A tree view of a Form',0,0,1,'','','Tree',NULL,NULL),(1484,'Clone','Clone a Form to create a new similar one',0,0,1,'','','Clone',NULL,NULL);
/*!40000 ALTER TABLE `form_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_group`
--

DROP TABLE IF EXISTS `form_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_group` (
  `id` mediumint(8) unsigned NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` text NULL,
  KEY `id` (`id`),
  CONSTRAINT `form_group_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_group`
--

LOCK TABLES `form_group` WRITE;
/*!40000 ALTER TABLE `form_group` DISABLE KEYS */;
INSERT INTO `form_group` VALUES
(65,'General Form Information',''),
(71,'General Form Group Information',''),
(94,'Values','Details about the values of this field.'),
(184,'Properties',''),
(264,'Form Link information',''),
(395,'Field','General Information about the Form Row being created.'),
(407,'General Form Action Information','Details about the Action'),
(411,'Display','Details about where the Form Action will be displayed.'),
(442,'User Group Information','General information about the user group.'),
(459,'Personal Information','Personal Information about the user'),
(561,'General Image Information','General information about the Image.'),
(567,'Technical Information','Technical information about the Image'),
(661,'Trash Group','Group for the trash'),
(667,'Statusses','Which statusses are allowed and which not.'),
(828,'One','One'),
(833,'Uno','dasd');
/*!40000 ALTER TABLE `form_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_link`
--

DROP TABLE IF EXISTS `form_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_link` (
  `id` mediumint(8) unsigned NOT NULL,
  `include` varchar(32) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` text,
  `relationship` enum('parent','child') DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '1',
  `create` tinyint(1) NOT NULL DEFAULT '1',
  `select` tinyint(1) NOT NULL DEFAULT '1',
  KEY `id` (`id`),
  CONSTRAINT `form_link_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_link`
--

LOCK TABLES `form_link` WRITE;
/*!40000 ALTER TABLE `form_link` DISABLE KEYS */;
INSERT INTO `form_link` VALUES (68,'form_group','Form Group',NULL,'child',1,1,1,0),(75,'form_row','Form Row',NULL,'child',1,1,1,0),(262,'form_link','Form Link','Create links to other Forms you have already created.','child',0,1,1,0),(401,'form','Form',NULL,'child',1,0,0,1),(404,'form_action','Form Action','A Form Action is an Action that can be performed on a Form: Create, Delete. Restore, Update or any other action','child',1,1,1,0),(454,'user_group','User Group','Select a User Group for this User.','child',1,0,0,1),(471,'user','Allowed Users','A list of Users that are allowed to perform this Form Action','parent',0,1,0,1),(472,'user_group','Allowed User Groups','A list of User Groups that are allowed to perform this Form Action','parent',0,1,0,1),(607,'image','Thumbnail','The thumbnail Image representing the Form','child',0,0,1,1),(615,'image','Thumbnail','The thumbnail icon representing the Form Action','child',0,0,1,1),(621,'image','Thumbnail','','child',0,0,1,1),(1453,'image','Image(s)','All Image assets related to this Project. Please note that the first Image loaded will be the thumbnail and NOT an asset.','child',0,1,1,1),(1473,'thumbnail','Thumbnail','The thumbnail image for this Project','child',1,0,1,0),(1486,'slide_left','Slide Left','The slide on the left hand side of the screen','child',0,0,0,1);
/*!40000 ALTER TABLE `form_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_row`
--

DROP TABLE IF EXISTS `form_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_row` (
  `id` mediumint(8) unsigned NOT NULL,
  `field` varchar(32) NOT NULL,
  `label` varchar(32) NOT NULL,
  `description` text,
  `placeholder` varchar(32) DEFAULT NULL,
  `type_parsed` varchar(32) NOT NULL,
  `type` enum('color','date','datetime','email','html','month','number','range','decimal','tel','time','url','week','checkbox','hidden','password','radio','text','textarea','dropdown','multiple','html') NOT NULL DEFAULT 'text',
  `signed` tinyint(1) DEFAULT NULL,
  `null` tinyint(1) NOT NULL DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `readonly` tinyint(1) DEFAULT NULL,
  `value_min` varchar(32) DEFAULT NULL,
  `value_max` varchar(32) DEFAULT NULL,
  `allowed_values` varchar(255) DEFAULT NULL,
  `step` tinyint(4) DEFAULT NULL,
  `length_min` mediumint(9) DEFAULT NULL,
  `length_max` mediumint(9) DEFAULT NULL,
  `functions_prepare` varchar(255) DEFAULT NULL,
  `functions_validate` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `form_row_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Dumping data for table `form_row`
--

LOCK TABLES `form_row` WRITE;
/*!40000 ALTER TABLE `form_row` DISABLE KEYS */;
INSERT INTO `form_row` VALUES
(66,'form_module','Module Name','All lowercase. Spaces replaced with underscores. this_is_an_example','Module Name','VARCHAR','hidden',0,0,NULL,NULL,NULL,NULL,NULL,'',NULL,0,32,NULL,'module_name'),
(72,'title','Title',NULL,'Title','VARCHAR','text',0,0,NULL,NULL,NULL,NULL,NULL,'',NULL,0,32,NULL,NULL),
(73,'description','Description',NULL,'Description','TEXT','textarea',0,0,NULL,NULL,NULL,NULL,NULL,'',NULL,0,65535,NULL,NULL),
(82,'null','Null',NULL,NULL,'TINYINT','checkbox',1,0,'0',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(84,'disabled','Disbaled',NULL,'Disabled','TINYINT','checkbox',1,0,NULL,NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(85,'readonly','Readonly',NULL,NULL,'TINYINT','checkbox',1,0,NULL,NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(86,'value_min','Value Minimum',NULL,NULL,'BIGINT','number',1,1,NULL,NULL,NULL,'-9223372036854775808','9223372036854775807','',1,NULL,NULL,NULL,NULL),
(87,'value_max','Value Maximum',NULL,NULL,'BIGINT','number',1,1,NULL,NULL,NULL,'-9223372036854775808','9223372036854775807','',1,NULL,NULL,NULL,NULL),
(88,'step','Step',NULL,'Step','DECIMAL','number',0,1,NULL,NULL,NULL,'0.001','1000','',1,1,2,NULL,NULL),
(89,'length_min','Length Minimum',NULL,'Length Minimum','SMALLINT','number',0,1,NULL,NULL,NULL,'0','65535','',1,NULL,NULL,NULL,NULL),
(90,'length_max','Length Maximum',NULL,'Length Maximum','SMALLINT','number',0,1,NULL,NULL,NULL,'0','65535','',1,NULL,NULL,NULL,NULL),
(91,'functions_prepare','Functions Prepare',NULL,'Functions Prepare','VARCHAR','text',0,1,NULL,NULL,NULL,NULL,NULL,'',NULL,0,255,NULL,NULL),
(92,'functions_validate','Functions Validate',NULL,'Functions Validate','VARCHAR','text',0,1,NULL,NULL,NULL,NULL,NULL,'',NULL,0,255,NULL,NULL),
(111,'name','Name','The name of the form being created. For example: User or Product Category.','Name','VARCHAR','text',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,32,NULL,NULL),
(113,'default_status','Default Status','The default status when a new Entity is being created. The system default is \'A\' for Active.',NULL,'ENUM','dropdown',NULL,0,'A',NULL,NULL,NULL,NULL,'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z',NULL,NULL,NULL,NULL,NULL),
(185,'url_column','URL Column','This is the unique column the Entity will be identified by.','URL Column','VARCHAR','hidden',NULL,1,NULL,0,0,NULL,NULL,NULL,NULL,0,64,NULL,NULL),
(279,'include','Include','This is the Form that will be included. This needs to be replaced as a Form Link.','Include','VARCHAR','hidden',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,32,NULL,NULL),
(280,'title','Title',NULL,'Title','VARCHAR','text',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,32,NULL,NULL),
(281,'relationship','Relationship',NULL,'Relationship','ENUM','dropdown',NULL,0,'child',NULL,NULL,NULL,NULL,'child,parent',NULL,NULL,NULL,NULL,NULL),
(282,'required','Required',NULL,NULL,'TINYINT','checkbox',0,0,'0',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(283,'multiple','Multiple',NULL,NULL,'TINYINT','checkbox',0,0,'1',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(284,'create','Create Allowed',NULL,NULL,'TINYINT','checkbox',0,0,'1',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(285,'select','Select Allowed',NULL,NULL,'TINYINT','checkbox',0,0,'1',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(354,'display','Display','Should this Form be displayed in the main menu?',NULL,'TINYINT','checkbox',0,0,'1',NULL,NULL,'0','1','0,1',1,1,1,NULL,NULL),
(396,'field','Field',NULL,NULL,'VARCHAR','hidden',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,32,NULL,NULL),
(397,'label','Label',NULL,'Label','VARCHAR','text',NULL,0,'',NULL,NULL,NULL,NULL,'',NULL,1,32,NULL,NULL),
(398,'description','Description',NULL,'Description','TEXT','textarea',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,65535,NULL,NULL),
(399,'placeholder','Placeholder',NULL,'Placeholder','VARCHAR','text',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,32,NULL,NULL),
(400,'type','Type',NULL,'Type','ENUM','dropdown',NULL,0,'text',NULL,NULL,NULL,NULL,'color,date,datetime,email,month,number,range,decimal,tel,time,url,week,checkbox,hidden,password,radio,text,textarea,dropdown,multiple,html',NULL,NULL,NULL,NULL,NULL),
(403,'description','Description',NULL,'Description','TEXT','textarea',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,65535,NULL,NULL),
(405,'action','Action','An action that can be performed on this Form','Action','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,32,'',''),
(406,'description','Description','','Description','TEXT','textarea',NULL,0,NULL,0,0,'','',NULL,0,0,65535,'',''),
(408,'display_main_menu','Display in Main Menu','Should this Form Action be shown in the main menu under this module','Main Menu','TINYINT','checkbox',NULL,1,NULL,0,0,'0','1',NULL,0,0,1,'',''),
(409,'display_module_menu','Display in Module Menu','Must this action be shown in the top right corner when another Form Action is performed on this module?','Module Menu','TINYINT','checkbox',NULL,1,NULL,0,0,'0','1',NULL,0,0,1,'',''),
(410,'display_entity','Display per Entity','Must this Form Action be shown on a per-row basis?','Entity','TINYINT','checkbox',NULL,1,NULL,0,0,'0','1',NULL,0,0,1,'',''),
(422,'delete_me','delete me','dasd','asdasd','','text',NULL,0,NULL,0,0,'','',NULL,0,0,64,'',''),
(440,'title','Title','','Title','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,32,'',''),
(441,'description','Description','','Description','TEXT','textarea',NULL,0,NULL,0,0,'','',NULL,0,0,65535,'',''),
(455,'email_address','Email Address','The user\'s Email Address','Email Address','VARCHAR','email',NULL,0,NULL,0,0,'','',NULL,0,0,255,'',''),
(456,'first_name','First Name','The User\'s first name.','First Name','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,64,'',''),
(457,'last_name','Last Name','The User\'s last name','Last Name','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,64,'',''),
(458,'password','Password','The User\'s password.','Password','VARCHAR','password',NULL,0,NULL,0,0,'','',NULL,0,0,32,'',''),
(490,'main_menu_title','Main Menu Title','The title of this Form Action in the main menu','Main Menu Title','','text',NULL,1,NULL,0,0,'','',NULL,0,0,32,'',''),
(491,'module_menu_title','Module Menu Title','The title in the menu at the top of the Form','Module Menu Title','','text',NULL,1,NULL,0,0,'','',NULL,0,0,32,'',''),
(492,'entity_title','Entity Title','The title of the Form Action on a per Entity basis','Entity Title','','text',NULL,1,NULL,0,0,'','',NULL,0,0,64,'',''),
(498,'type_parsed','Type Parsed',NULL,NULL,'VARCHAR','hidden',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,32,NULL,NULL),
(559,'title','Title','The title/name of the Image.','Title','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,32,'',''),
(560,'description','Description','A description of the Image.','','TEXT','hidden',NULL,1,NULL,0,0,'','',NULL,0,0,65535,'',''),
(562,'filename','Filename','The original name of the Image','Filename','VARCHAR','text',NULL,0,NULL,0,1,'','',NULL,0,0,255,'',''),
(563,'path','Path','The path of where the Image is stored on the server.','Path','VARCHAR','text',NULL,0,NULL,0,1,'','',NULL,0,0,255,'',''),
(564,'filesize','Filesize','The size of the Image in Bytes','Filesize','MEDIUMINT','number',NULL,0,NULL,0,1,'0','10485760',NULL,1,0,64,'',''),
(565,'width','Width','The width of the Image in pixels','Width','SMALLINT','number',NULL,0,NULL,0,1,'0','10000',NULL,1,0,64,'',''),
(566,'height','Height','The height of the Image in pixels','Height','SMALLINT','number',NULL,0,NULL,0,1,'0','10000',NULL,1,0,64,'',''),
(660,'trash_row','Trash Row','Row for the trash','Trash Row','VARCHAR','text',NULL,0,NULL,0,0,'','',NULL,0,0,64,'',''),
(663,'allowed_values','Allowed Values','Enter the values that are allowed in this field, separated by commas.','Allowed Values','VARCHAR','text',NULL,1,NULL,0,0,'','',NULL,0,0,255,'',''),
(665,'allowed_statusses','Allowed Statusses','When an Entity is this status then this Form Action can be applied to it.','','VARCHAR','multiple',NULL,1,NULL,0,0,'','','A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z',0,0,64,'',''),
(666,'disallowed_statusses','Disallowed Statusses','When an Entity is this status this Form Action is not allowed.','','VARCHAR','multiple',NULL,1,NULL,0,0,'','','A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z',0,0,64,'',''),
(1392,'tag','Tag','Add a tag to this Image to easily distinguish between similar images.','Tag','VARCHAR','hidden',NULL,1,NULL,0,0,'','','',0,0,32,'',''),
(1917,'username','Username','A unique username with which you will be identified throughout the site','Username','VARCHAR','text',NULL,0,NULL,0,0,'','','',0,0,32,'','');
/*!40000 ALTER TABLE `form_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` mediumint(8) unsigned NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` text,
  `filename` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `filesize` mediumint(8) unsigned NOT NULL,
  `width` smallint(5) unsigned NOT NULL,
  `height` smallint(5) unsigned NOT NULL,
  `tag` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trash`
--

DROP TABLE IF EXISTS `trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trash` (
  `id` mediumint(8) unsigned NOT NULL,
  `trash_row` varchar(64) NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `trash_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trash`
--

LOCK TABLES `trash` WRITE;
/*!40000 ALTER TABLE `trash` DISABLE KEYS */;
/*!40000 ALTER TABLE `trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` mediumint(8) unsigned NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `password` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (462,'righteous.trespasser@gmail.com','Hendrik','Volschenk','Steers','righteous.trespasser'),(494,'visitor@twentytwo.co.za','Visitor','User','Visitor','Visitor');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` mediumint(8) unsigned NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` text NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (461,'Administrators','Website Administrators are added into this User Group.'),(475,'Users','All users go in here'),(493,'Visitors','Users that are not logged in to the system.');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-01  1:02:30